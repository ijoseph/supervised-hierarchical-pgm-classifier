# -*- coding: utf-8 -*-
'''
@author: Isaac C. Joseph
'''
import argparse, multiprocessing
import pandas as pd
import logging
import sys
import functools
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

import Performance.classification.CrossValidation
import numpy as np
import feather

class ParserBase(object):

    def _handle_low_variance(self, merged_plus_outcome, boundary_index, thresh=0):
        orig_n_col = merged_plus_outcome.shape[1]

        num_within_boundary = (merged_plus_outcome.var()[0:boundary_index] <= thresh).sum()

        merged_plus_outcome = merged_plus_outcome.loc[:, merged_plus_outcome.var() > thresh]

        boundary_index -= num_within_boundary

        logging.warning("Removed {0} of {1} columns due to low variance (variance ≤ {2}), resulting in {3}.".
                        format(orig_n_col - merged_plus_outcome.shape[1], orig_n_col, thresh,
                               merged_plus_outcome.shape[1]))

        return (merged_plus_outcome, boundary_index)

    @staticmethod
    def _keep_only_top_n_most_variable_columns(data_frame, n=50):
        if n >= data_frame.shape[1]:
            return data_frame


        orig_n_col = data_frame.shape[1]
        var_df = data_frame.var()
        var_df.sort_values(ascending=False, inplace=True)
        data_frame = data_frame[var_df[0:n].index]

        logging.warning("Keeping top {0} columns (reduced by {2} from {1})".format(n, orig_n_col, orig_n_col-n))

        return data_frame


class CGPData(ParserBase):
    """
    Parse Cancer Cell Line Encyclopedia (CCLE) data
    """

    def __init__(self, sensitivity, cnv=None, expression=None, mutation=None):
        """
        Constructor
        """
        self.sensitivity_df = None
        self.cnv_df = None
        self.expression_df = None
        self.mutation_df = None
        self.sample_name__to__sample_id = None

        self.read_files(cnv, expression, mutation, sensitivity)

        # Split into separate data.frames again
        self.name_to_df_dict = {"cnv": self.cnv_df, "sensitivity": self.sensitivity_df,
                                "expression": self.expression_df, "mutation": self.mutation_df}

    def read_files(self, cnv, expression, mutation, sensitivity):

        if cnv:
            logging.info(".done. Reading CNV data...")
            self.parse_cnv(cnv)

        if expression:
            logging.info(".done. Reading expression data...")
            self.parse_expression(expression)

        if mutation:
            logging.info(".done. Reading mutation data...")
            self.parse_mutations(mutation)

        logging.info("Reading sensitivity data...")
        self.parse_sensitivity(sensitivity)

    def parse_sensitivity(self, sensitivity):
        """
        Parse sensitivity compound data, substituting sample name for ID
        :param sensitivity:
        :return:
        """
        sensitivity_raw = pd.read_csv(sensitivity.name, low_memory=False, sep="\t")

        self.sensitivity_df = sensitivity_raw.merge(self.sample_name__to__sample_id,
                                                    left_index=True, right_index=True).set_index("ID_SAMPLE")\
            .drop(["Screened Compounds:", "Sample name"], axis=1)

    def parse_cnv(self, cnv):
        cnv_raw = pd.read_csv(cnv.name, low_memory=False, sep="\t")
        self.cnv_df = cnv_raw.pivot_table(index="ID_SAMPLE", columns="gene_name", values="TOTAL_CN", fill_value=2)
        self.sample_name__to__sample_id = cnv_raw[["ID_SAMPLE", "SAMPLE_NAME"]]

        # if no call at a particular locus, assume normal copy number

    def parse_expression(self, expression):
        expression_raw = pd.read_csv(expression.name, low_memory=False, sep="\t")
        self.expression_df = expression_raw.pivot_table(index="ID_SAMPLE", columns="GENE_NAME", values="Z_SCORE")
        self.sample_name__to__sample_id = expression_raw[["ID_SAMPLE", "SAMPLE_NAME"]]

    def parse_mutations(self, mutation):
        mutation_raw = pd.read_csv(mutation.name, low_memory=False, sep="\t")

        # Aggregate on the gene level
        self.mutation_df = mutation_raw.pivot_table(index="ID_sample", columns="Accession Number", values= [],
                                                    aggfunc=lambda x: True, fill_value=False)
        self.mutation_df.rename(columns={"ID_sample": "ID_SAMPLE"}, inplace=True)

        self.sample_name__to__sample_id = mutation_raw[["ID_sample", "Sample name"]]\
            .rename(columns={"Sample name:": 'SAMPLE_NAME', "ID_sample": "ID_SAMPLE"})

        # change mutations from true/false to numeric
        self.mutation_df = self.mutation_df.astype(float)

    def get_two_platform_data_frame_collection(self, platform_one_name, platform_two_name,
                                               compound_name, impute_type="drop_rows"):
        """
        Gets DataFrameCollection object with compound_name compound as observed variable
        :param compound_name:
        :return:
        """

        logging.info("Merging data types...")
        (merged_plus_outcome , boundary_index)= \
            self._merge_to_data_frame(compound_name, platform_one_name, platform_two_name)

        boundary_index = self._handle_missing_data(merged_plus_outcome, boundary_index, impute_type=impute_type)

        (merged_plus_outcome , boundary_index) = self._handle_low_variance(merged_plus_outcome, boundary_index,  thresh=0)

        # Drop duplicates based on index
        merged_plus_outcome = self.handle_duplicates(merged_plus_outcome)

        logging.warning("Final size: {0} × {1}".format(merged_plus_outcome.shape[0], merged_plus_outcome.shape[1]))

        if merged_plus_outcome.size > 0:
            dfc = Performance.classification.CrossValidation.DataFrameCollection(
                independent_data=merged_plus_outcome.drop(compound_name, axis=1),
                dependent_data=merged_plus_outcome[[compound_name]].set_index(merged_plus_outcome.index).astype(bool),
                boundary_indices=[boundary_index])
        else:
            dfc = Performance.classification.CrossValidation.DataFrameCollection()

        return dfc

    def handle_duplicates(self, merged_plus_outcome):

        orig_n_row = merged_plus_outcome.shape[0]

        merged_plus_outcome = merged_plus_outcome[~merged_plus_outcome.index.duplicated(keep='first')]
        logging.warning("Removed {0} of {1} rows due to duplication, resulting in {2}.".
                        format(orig_n_row - merged_plus_outcome.shape[0], orig_n_row, merged_plus_outcome.shape[0]))

        return merged_plus_outcome


    def _merge_to_data_frame(self, compound_name, platform_one_name, platform_two_name):
        try:
            df_one = self.name_to_df_dict[platform_one_name]
            df_two = self.name_to_df_dict[platform_two_name]
        except KeyError:
            raise ValueError("Platform not found: {0} or {1}\n must be one of {2}"
                             .format(platform_one_name, platform_one_name, self.name_to_df_dict.keys()))


        # Merge feature datatypes
        df_one.columns = df_one.columns.map(lambda x: str(x) + '_' + platform_one_name)
        df_two.columns = df_two.columns.map(lambda x: str(x) + '_' + platform_two_name)
        boundary_index = df_one.shape[1] -1
        merged = df_one.merge(df_two, how="outer", left_index=True, right_index=True)


        # Get just the relevant compound
        try:
            sensitivity = self.sensitivity_df[[compound_name]]
            # translate "S"[ensitive] to True, "R"[esistant] to False
            sensitivity = sensitivity.applymap(lambda x: True if x == "S" else False if x == "R" else x)
            sensitivity = sensitivity.dropna() # remove missing data from outcome
        except KeyError:
            raise ValueError("Cannot find compound name {0}".format(compound_name))

        merged_plus_outcome = merged.merge(sensitivity, left_index=True, right_index=True, how="inner")


        return (merged_plus_outcome, boundary_index)

    def _handle_missing_data(self, merged_plus_outcome, boundary_index, impute_type="drop_rows"):
        orig_n_col = merged_plus_outcome.shape[1]
        orig_n_row = merged_plus_outcome.shape[0]

        num_missing = merged_plus_outcome.isnull().sum().sum()
        num_cols_with_missing = merged_plus_outcome.isnull().any().sum()
        num_rows_with_missing = merged_plus_outcome.isnull().any(axis=1).sum()

        logging.warning("{0} data points missing, of {1} total\n	{2} out of {3} rows (samples) have missing values\n"
                        "	{4} out of {5} columns (features) have missing values\n".format(num_missing,
                                                                                        merged_plus_outcome.size,
                                                                                        num_rows_with_missing,
                                                                                        merged_plus_outcome.shape[0],
                                                                                        num_cols_with_missing,
                                                                                        merged_plus_outcome.shape[1]))

        if impute_type == "mean_column": # Impute columns based on average of column
            logging.warning("Column-imputing to fill missing.")
            merged_plus_outcome.fillna(merged_plus_outcome.mean(), inplace=True)

            # Remove columns that are all NaN
            num_within_boundary = merged_plus_outcome.isnull().all()[0:boundary_index].sum()
            boundary_index -= num_within_boundary

            merged_plus_outcome.dropna(how='all', axis=1, inplace=True)

        elif impute_type == "drop_columns":
            logging.warning("Removing features missing in any sample.")
            num_within_boundary = merged_plus_outcome.isnull().any()[0:boundary_index].sum()
            merged_plus_outcome.dropna(how='any', axis=1, inplace=True)
            boundary_index -= num_within_boundary

        elif impute_type == "drop_rows":
            logging.warning("Removing any samples with missing features.")
            merged_plus_outcome.dropna(how='any', axis=0, inplace=True)
        else:
            raise ValueError("Unknown impute type {0}".format(impute_type))




        logging.warning("Removed {0} of {1} columns due to missing data, resulting in {2}.".
                        format(orig_n_col - merged_plus_outcome.shape[1], orig_n_col, merged_plus_outcome.shape[1]))
        logging.warning("Removed {0} of {1} rows due to missing data, resulting in {2}.".
                        format(orig_n_row - merged_plus_outcome.shape[0], orig_n_row, merged_plus_outcome.shape[0]))

        return boundary_index



    def remove_low_variance_columns(self, merged_plus_outcome):
        pass


class CGPDataFromSupplement(CGPData):
    """
    Load CGP data from the supplement excel files
    """
    def __init__(self, sensitivity, mutations, cnv):
        super(CGPDataFromSupplement, self).__init__(sensitivity=sensitivity, mutation=mutations, cnv=cnv)


    def parse_sensitivity(self, sensitivity):
        """
        Parse sensitivity compound data, substituting sample name for ID
        :param sensitivity:
        :return:
        """
        sensitivity_raw = pd.read_csv(sensitivity.name, low_memory=False, sep="\t")

        self.sensitivity_df = sensitivity_raw.set_index("Screened Compounds:")


    def parse_mutations(self, mutations):
        """
        Parse mutations from excel
        :param mutation:
        :return:
        """
        logging.info("Reading mutation data...")

        if isinstance(mutations, file):
            mut_raw = pd.read_csv(file.name, low_memory=False)

        elif isinstance(mutations, str):
            mut_raw = pd.read_csv(mutations, low_memory=False)

        else:
            raise ValueError("Could not read mutation file as it is neither string nor file")


        def indicate_mutations(cell_string):
            if not isinstance(cell_string, str):
                return cell_string

            if not "::" in cell_string:# this isn't a mutation
                return cell_string

            (var_info, cn_info) = cell_string.split("::")

            if var_info == 'na':
                return np.nan

            elif var_info == 'wt':
                return 0.0

            elif var_info.startswith('p'):# some kind of mutation
                return 1.0


        mut_binarized = mut_raw.applymap(indicate_mutations).set_index("Cell Line").iloc[:, range(5, 81)] # just the mutations

        mut_binarized.dropna(axis=0 ,how='all', inplace=True) # this is just to get rid of the extra row that does nothing

        self.mutation_df = mut_binarized


    def parse_cnv(self, cnv):
        cnv_raw = pd.read_csv(cnv).rename(columns={"Unnamed: 0": "Sample name"})
        cnv_proc = cnv_raw.set_index("Sample name").T

        self.cnv_df = cnv_proc


class CostelloData(ParserBase):
    """
    Parse mtuation, methylation
    """
    def __init__(self):
        self.prepare_for_parsing_mutations()
        self.mutation_df = None
        self.hm_info = None
        self.clinical_df = None

    """  Mutations """
    def prepare_for_parsing_mutations(self):
        def any_damaging_aggfn(pandas_series, damaging_labels):
            if pandas_series.isin(damaging_labels).any():
                return 2.0
            elif pandas_series.isnull().all():
                return np.nan
            else:
                return 1.0

        self.annotation_categories = (
            "MutationAssessor_Func..Impact", "FATHMM_prediction", "PROVEAN_verdict", "SIFT_verdict")
        pathogenic_indicators = dict(
            zip(self.annotation_categories, [("medium", "high"), ("PATHOGENIC",), ("Deleterious",), ("Damaging",)]))
        self.data_type_dict = dict(zip(self.annotation_categories, [pd.Categorical] * len(self.annotation_categories)))
        self.data_type_dict.update({"patient": int})
        self.pathogenic_agg_fns = dict(zip(self.annotation_categories, [
            functools.partial(any_damaging_aggfn, damaging_labels=pathogenic_indicators[tool]) for tool in
            self.annotation_categories]))

    def parse_mutations(self, mutation_file, keep_only_astro=True, merge_idh1_and_2=True, keep_top_n=50):
        mut_raw = pd.read_csv(
            mutation_file,
            sep="\t", low_memory=False, dtype=self.data_type_dict)

        mut_raw = self.__filter_mutations(mut_raw, keep_only_astro=keep_only_astro, merge_idh1_and_2=merge_idh1_and_2)


        mut_all_raw = mut_raw.pivot_table(index="patient", columns="gene", values=self.annotation_categories,
                                          aggfunc=self.pathogenic_agg_fns,
                                          fill_value=np.nan)

        mut_all_raw_swapped = mut_all_raw.swaplevel(0, 1, axis=1)
        merged_plus_outcome = mut_all_raw_swapped.mean(axis=1, level=0)

        merged_plus_outcome.fillna(value=0, inplace=True)

        merged_plus_outcome = self._keep_only_top_n_most_variable_columns(merged_plus_outcome, n=keep_top_n)

        self.mutation_df = merged_plus_outcome

    def parse_hypermutation_on_recurrence_info(self, info_file):
        hm_raw = pd.read_csv(
            info_file,
            sep="\t", index_col="patient")

        hm = (hm_raw == "yes")

        self.hm_info = hm

        self.hm_info = self.hm_info.astype(bool)

    def __filter_mutations(self, mut_raw, merge_idh1_and_2, keep_only_astro):
        mut_raw = mut_raw[mut_raw.Pri_called == True]

        mut_raw = mut_raw[~mut_raw.type.isin(["Silent"])]
        mut_raw = mut_raw[~mut_raw.type.isnull()]

        mut_raw = mut_raw[mut_raw.TMZ_treatment == "yes"]

        # Only oligo
        if keep_only_astro:
            mut_raw = mut_raw[mut_raw['disease'].str.startswith("Astro")]

        # Combine IDH1 and 2
        if merge_idh1_and_2:
            mut_raw['gene'] = mut_raw['gene'].replace(to_replace={"IDH2": "IDH1/2", "IDH1": "IDH1/2"})

        # Get rid of patient 211

        mut_raw = mut_raw[mut_raw['patient'] != 211]

        return mut_raw

    def get_mutation_dfc(self):
        both = self.mutation_df.join(self.hm_info)

        (both, boundary_indices) = self._handle_low_variance(merged_plus_outcome=both, boundary_index=both.shape[1]-1)


        dfc = Performance.classification.CrossValidation.DataFrameCollection(
                independent_data=both.drop(labels="HM", axis=1),
                dependent_data=both[["HM"]],
                boundary_indices=boundary_indices)
        return dfc

    """  Methylation """

    def parse_methylation(self, methylation_feather_file, spec_file, keep_only_astro=True, keep_top_n=50):
        all = feather.read_dataframe(
            "/Volumes/Costello_reworked/TMZ/Molecular/Methylation/Final-Patient-Set/processed/all.feather")
        sample_names = all.columns[15:]
        all.set_index("cg", inplace=True)
        all = all.loc[:, sample_names]
        all = all.T

        all_top_n = self._keep_only_top_n_most_variable_columns(all, n=keep_top_n)

        spec = self.read_meth_spec(spec_file)

        all_top_n_annot = self.incorporate_meth_spec(all_top_n, spec, keep_only_astro)

        all_top_n_annot.fillna(all_top_n_annot.mean(), inplace=True)

        self.methylation_df = all_top_n_annot

    def incorporate_meth_spec(self, all_top_n, spec, keep_only_astro):
        all_top_n_annot = all_top_n.join(spec)
        all_top_n_annot = all_top_n_annot[all_top_n_annot["Tumor"] == "Primary"]
        if keep_only_astro:
            all_top_n_annot = all_top_n_annot[all_top_n_annot["Histology"] == "Astro"]

        all_top_n_annot.drop(
            ["Grimmer", "Tumor exomes", "Order", "SF#", "SF#unique", "Exome Kit", "Exome", "Histology", "Grade",
             "RNA-seq", "RNA-seq alt", "Infinium beadchip", "Infinium pos", "Tissue", "Tumor"], axis=1, inplace=True)
        all_top_n_annot["Patient"] = all_top_n_annot.Patient.map(lambda x: x.strip("P")).astype(int)


        all_top_n_annot.set_index("Patient", inplace=True)
        return all_top_n_annot

    def read_meth_spec(self, spec_file):
        spec = pd.read_csv(spec_file, sep="\t")
        spec["index"] = spec["Infinium beadchip"].map(str) + "_" + spec["Infinium pos"]
        spec = spec.set_index("index")
        return spec

    def get_methylation_dfc(self):
        both = self.methylation_df.join(self.hm_info)

        (both, boundary_indices) = self._handle_low_variance(merged_plus_outcome=both, boundary_index=both.shape[1] - 1)

        dfc = Performance.classification.CrossValidation.DataFrameCollection(
            independent_data=both.drop(labels="HM", axis=1),
            dependent_data=both[["HM"]],
            boundary_indices=boundary_indices)
        return dfc

    """ Methylation + Mutation """

    def get_methylation_and_mutation_dfc(self):
        mut_and_outcome = self.mutation_df.join(self.hm_info)
        (mut_and_outcome, boundary_indices) = \
            self._handle_low_variance(merged_plus_outcome=mut_and_outcome, boundary_index=mut_and_outcome.shape[1]-1)

        mut_and_meth_and_outcome = mut_and_outcome.join(self.methylation_df)

        self.handle_missing_meth_or_mut(mut_and_meth_and_outcome)


        dfc = Performance.classification.CrossValidation.DataFrameCollection(
            independent_data=mut_and_meth_and_outcome.drop(labels="HM", axis=1),
            dependent_data=mut_and_meth_and_outcome[["HM"]],
            boundary_indices=boundary_indices)


        return dfc

    def handle_missing_meth_or_mut(self, mut_and_meth_and_outcome):
        mut_and_meth_and_outcome.dropna(inplace=True)

    """ Clinical """

    def parse_clinical(self, complete_lgg_overview_xlsx, scale=True, keep_only_astro=False, ignore_resection=False):
        all_lgg_clin = pd.read_excel(complete_lgg_overview_xlsx)
        all_lgg_clin_just_tmz = all_lgg_clin[(all_lgg_clin["TMZ_treatment"] == "yes")]
        all_lgg_clin_just_tmz_just_relevant = all_lgg_clin_just_tmz[
            ["Patient_number", "Age_at_diagnosis", "PRIMARY", "resection", "TMZ_cycles",
             "Surgery_Pri to TMZ_start_days.1", "TMZ_start to Radiographic_Progression_days", "Sex", "LGG_subtype"]]
        all_lgg_clin_just_tmz_just_relevant.set_index("Patient_number", inplace=True)


        #  age
        final_df = self.parse_age(all_lgg_clin_just_tmz_just_relevant)

        #  location
        final_df = self.parse_location(all_lgg_clin_just_tmz_just_relevant, final_df)

        if not ignore_resection:
            #  extent of resection
            final_df = final_df.join(pd.get_dummies(all_lgg_clin_just_tmz_just_relevant.resection, prefix="resection_"))

        #  number of TMZ cycles
        final_df = self.parse_tmz_cycles(all_lgg_clin_just_tmz_just_relevant, final_df)

        # Time from surgery to TMZ start
        final_df = self.parse_primary_surgery_to_tmz_start(all_lgg_clin, final_df)

         # TMZ start time to progression time
        final_df = self.parse_tmz_start_to_progression(all_lgg_clin_just_tmz_just_relevant, final_df)

        # Gender
        final_df = final_df.join(
            pd.get_dummies(all_lgg_clin_just_tmz_just_relevant.Sex.apply(lambda x: np.nan if x == "-" else x),
                           drop_first=True))


        # histology
        final_df = final_df.join(pd.get_dummies(all_lgg_clin_just_tmz_just_relevant.LGG_subtype,
                                                        prefix="histology"))

        if keep_only_astro:
            final_df = final_df[final_df.histology_A == 1]\
                .drop(["histology_A", "histology_O", "histology_OA"], axis=1)

        if scale:
            final_df = final_df.apply(lambda x: (x - np.mean(x)) / (np.max(x) - np.min(x)))



        self.clinical_df = final_df

    def get_clinical_dfc(self):

        both = self.clinical_df.join(self.hm_info)


        (both, boundary_indices) = self._handle_low_variance(merged_plus_outcome=both, boundary_index=both.shape[1]-1)


        dfc = Performance.classification.CrossValidation.DataFrameCollection(
                independent_data=both.drop(labels="HM", axis=1),
                dependent_data=both[["HM"]].astype(bool),
                boundary_indices=boundary_indices)
        return dfc


    """ Methylation + Mutation + Clinical """

    def get_methylation_and_mutation_and_clinical_dfc(self, scale_all=False):
        meth_and_mut_dfc = self.get_methylation_and_mutation_dfc()
        clin_dfc = self.get_clinical_dfc()

        all_ind_df = meth_and_mut_dfc.independentDF.join(clin_dfc.independentDF).join(self.hm_info)

        full_dfc = Performance.classification.CrossValidation.DataFrameCollection(
            independent_data=all_ind_df.drop(
            labels="HM", axis=1), dependent_data=all_ind_df[["HM"]],
            boundary_indices=meth_and_mut_dfc.indIndices + meth_and_mut_dfc.independentDF.shape[1])

        if scale_all:
            full_dfc.independentDF = full_dfc.independentDF.apply(lambda x: (x - np.mean(x)) / (np.max(x) - np.min(x)))
            # drop infs and nans
            full_dfc.independentDF.replace([np.inf, -np.inf], np.nan, inplace=True)
            full_dfc.independentDF.dropna(axis=1, inplace=True)




        return full_dfc






    def parse_tmz_start_to_progression(self, all_lgg_clin_just_tmz_just_relevant, final_df):
        final_df = final_df.join(
            pd.to_numeric(all_lgg_clin_just_tmz_just_relevant["TMZ_start to Radiographic_Progression_days"],
                          errors="coerce"))
        final_df["TMZ_start to Radiographic_Progression_days"].fillna(
            final_df["TMZ_start to Radiographic_Progression_days"].mean(), inplace=True)

        return final_df

    def parse_primary_surgery_to_tmz_start(self, all_lgg_clin, final_df):
        final_df = final_df.join(pd.to_numeric(all_lgg_clin["Surgery_Pri to TMZ_start_days.1"], errors="coerce"))
        final_df["Surgery_Pri to TMZ_start_days.1"].fillna(final_df["Surgery_Pri to TMZ_start_days.1"].mean(),
                                                           inplace=True)
        final_df["log_Surgery_Pri to TMZ_start_days.1"] = final_df["Surgery_Pri to TMZ_start_days.1"].apply(np.log)
        return final_df

    def parse_tmz_cycles(self, all_lgg_clin_just_tmz_just_relevant, final_df):
        final_df = final_df.join(pd.to_numeric(all_lgg_clin_just_tmz_just_relevant.TMZ_cycles, errors="coerce"))
        final_df.TMZ_cycles.fillna(final_df.TMZ_cycles.mean(), inplace=True)
        return final_df

    def parse_location(self, all_lgg_clin_just_tmz_just_relevant, final_df):
        possibilities = ["temporal", "frontal", "occipital"]
        for possibility in possibilities:
            final_df[possibility] = all_lgg_clin_just_tmz_just_relevant.PRIMARY.str.contains(possibility,
                                                                                             case=False).astype(float)
        final_df.fillna(final_df.mean(), inplace=True)
        return final_df

    def parse_age(self, all_lgg_clin_just_tmz_just_relevant):
        all_lgg_clin_just_tmz_just_relevant[all_lgg_clin_just_tmz_just_relevant.Age_at_diagnosis == "-"] = np.nan
        final_df = pd.DataFrame(all_lgg_clin_just_tmz_just_relevant.Age_at_diagnosis.fillna(
            all_lgg_clin_just_tmz_just_relevant.Age_at_diagnosis.mean()))
        return final_df


def main():
    parser = argparse.ArgumentParser(description="Parse CCLE data into DataFrameCollection")
    parser.add_argument("--sensitivity", type=file, required= True,
                        help="sensitivity .tsv file, e.g. `binarized.tsv`")


    parser.add_argument("--cnv", type=file, default = None,
                        help="cnv .csv.gz file, e.g. `CosmicCLP_CompleteCNA.tsv.gz`")

    parser.add_argument("--expression", type=file, default = None,
                        help="expression .csv.gz file, e.g. `CosmicCLP_CompleteGeneExpression.tsv.gz`")

    parser.add_argument("--mutation", type=file, default = None,
                        help="somatic mutation .tsv.gz file, e.g. `CosmicCLP_MutantExport.tsv.gz`")

    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default=sys.stdout,
                        help="File to which to output [sys.stdout]")

    parser.add_argument('-p', '--numThreads', type=int, default=multiprocessing.cpu_count() / 2,
                        help="Number of threads to run [(# of CPUs)/2]")

    parser.add_argument('--tempFolder', default="temp/", help="temporary folder for files [./temp]")
    namespace = parser.parse_args()

    if not (namespace.cnv or namespace.expression or namespace.mutation):
        parser.error("At least one of CNV, expression, or mutation needs to be entered")

    cd = CGPData(sensitivity=namespace.sensitivity, cnv=namespace.cnv,
                 expression=namespace.expression, mutation = namespace.mutation)


    print(cd.get_two_platform_data_frame_collection("mutation", "expression", "Erlotinib"))


if __name__ == '__main__':
    main()