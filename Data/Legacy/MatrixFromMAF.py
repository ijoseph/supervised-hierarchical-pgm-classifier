'''
Created on May 18, 2014

@author: ijoseph
'''
import argparse
import sys
import csv
import collections
from psutil._compat import defaultdict

class Gene:
    def __init__(self, geneName, entrezID):
        self.geneName = geneName            
        self.entrezID = entrezID
        
    def __hash__(self):
        return hash((self.geneName, self.entrezID))
    
    def __eq__(self, other):
        return (self.geneName, self.entrezID) ==(other.geneName, other.entrezID)
    
    def __str__(self):
        return "{0} ({1})".format(self.geneName, self.entrezID)
    
    def __repr__(self):
        return self.__str__()


# Next step: modify so that I can work with an input clinical data file with sample list, and then output the matrix in that sample order of columns.  

class MatrixFromMAF(object):
    '''
    Takes in a TCGA MAF (https://wiki.nci.nih.gov/display/TCGA/TCGA+MAF+Files) containing the tumor barcodes (https://wiki.nci.nih.gov/display/TCGA/TCGA+Barcode)  and hugo gene symbols/ entrez ID's, and 
    description of somatic mutations occuring in a range of samples, 
    and outputs a matrix whose first row is a header wtih gene names, and whose first column describes sample barcodes, and whose entries contain a 1 
    upon tumors having a certain mutation.
    '''
    

    def __init__(self, inputMAF, output, outcomeFile):
        '''
        Constructor. 
        '''        
        # I need a full list of genes, full list of samples, and a sample -> gene -> mutation type dict. 
        self.output = output        
        self.outcomeFile = outcomeFile
        self.sampleToGeneToMutType = defaultdict(dict)
        if outcomeFile is not None:
            (self.outcomeSampleList, self.barcodeLen) = self._parseOutcome(outcomeFile)            
        (self.geneList, self.sampleList, self.sampleToGeneToMutType) = self._parseMAF(inputMAF)
        self.geneList.sort()
        self.sampleList.sort()
        
        
    def _parseMAF(self, maf):
        
        geneSet = set([])
        barcodeSet = set([])
        sampleGeneMutType = collections.defaultdict(dict)
        reader = csv.DictReader(maf, delimiter = '\t')
        for row in reader:
            if self.outcomeFile is not None:
                # only take the primary solid tumor
                if row['Tumor_Sample_Barcode'][13:15] != '01':
                    continue
                barcode = row['Tumor_Sample_Barcode'][0:self.barcodeLen] # only take the length of barcode used in the outcome file
                # ignore this row if it's not in the sample list in the outcome file
                if barcode not in self.outcomeSampleList:
                    continue  
            else:
                barcode = row['Tumor_Sample_Barcode']
                
            gene = Gene(row['Hugo_Symbol'], int(row['Entrez_Gene_Id']))
            geneSet = geneSet.union([gene])                
            barcodeSet = barcodeSet.union([barcode])            
            sampleGeneMutType[barcode][gene] =  row['Variant_Classification']
    
        return (list(geneSet), list(barcodeSet) ,sampleGeneMutType)
            
    
    def _parseOutcome(self, outcomeFile):
        '''
        Loops through outcome file and sees which samples are there. Also look to see the detail of the TCGA barcode.  
        '''
        reader = csv.reader(outcomeFile, delimiter = '\t')
        barcodeList = []        
        for row in reader:
            if not row[0].startswith("TCGA"):
                sys.stderr.write("Skipping row, assuming is header since doesn't start with 'TCGA':" + "\t".join(row) )
                continue
            barcodeList += [row[0]]            
        barcodeLen = len(barcodeList[-1])
        
        return (barcodeList, barcodeLen)
            
            
    def writeMatrix(self):
        '''
        Writes the matrix
        '''
        # If there's an outcome file, produce rows in that order; 
        if self.outcomeFile is not None:
            sampleList = self.outcomeSampleList
        else:
            sampleList = self.sampleList
        
        #Write the header        
        self.output.write("variants\t" + "\t".join([str(gene) for gene in self.geneList]) + "\n")        
        for sample in sampleList:
            self.output.write(sample +"\t")
            geneVector = []                        
            for gene in self.geneList:                
                geneVector +=[str(1) if self._hasMutationInGene(self.sampleToGeneToMutType[sample], gene) else str(0)]            
            self.output.write("\t".join(geneVector)+ "\n")
            


    def _hasMutationInGene(self, sampleDict, gene):
        '''
        Determines if is a mutation for this particular sample and Gene         
        '''
        if len(sampleDict) == 0:
            return False
         
        if sampleDict.has_key(gene): # first pass: ignore silent mutations ['Silent']
            if sampleDict[gene] != 'Silent':
                return True
            else: 
                return False

        return False
            
        

def main():    
    parser = argparse.ArgumentParser(description = """  Takes in a TCGA MAF containing the tumor barcodes and hugo gene symbols/ entrez ID's, and description of somatic mutations occuring in a range of samples, 
    and outputs a matrix whose first row is a header with gene names, and whose first column describes sample barcodes, and whose entries contain a 1 
    upon tumors having a certain mutation.""") 
    parser.add_argument("--maf", type=file, required=True, help=".maf file from TCGA")    
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "File to which to output the matrix" ,required=True)
    parser.add_argument("--outcome", type=file, help="optional outcome file, containing barcodes as the first column. ")    
#     mut_parser.add_argument('-p', '--numThreads', type=int, default= multiprocessing.cpu_count()/2, help = "Number of threads to run [(# of CPUs)/2]")
#     mut_parser.add_argument('--tempFolder', default="temp/", help= "temporary folder for files [./temp]")
    namespace = parser.parse_args()
    
    maf = MatrixFromMAF(namespace.maf, namespace.output, namespace.outcome)
    maf.writeMatrix()
        
if __name__ == '__main__':
    main()
    
    