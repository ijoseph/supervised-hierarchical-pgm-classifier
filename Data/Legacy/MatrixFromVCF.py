'''
Created on Jul 1, 2015

@author: ijoseph
'''

import argparse, sys
import csv
import os
from subprocess import call


class MatrixFromVCF(object):
    '''
    Given a directory with multiple VCF files (one per patient), and also a map describing which VCF names are the normal files, 
    creates matrices using VCF tools for these for the normal samples 
    '''

    def __init__(self, vcfDir, sampleNameMap, output, maxMissing):
        '''
        Constructor. 
        '''
        self.output=output
        self.patientToNormalVCFSampleID = self.parseSampleNameMap(sampleNameMap)
        self.maxMissing = maxMissing
        self.normalVCFSampleIDToPatientID = {v: k for (k,v) in self.patientToNormalVCFSampleID.items()}        
        filePaths = [os.path.join(vcfDir,filename) for filename in next(os.walk(vcfDir))[2] if (filename.endswith(".vcf") \
                                                                                                and not (filename.startswith("normal")) and not (filename.startswith("all")))]
        sys.stderr.write("1. Selecting normal samples from VCFs\n")
        #normalVCFFilepaths = self.outputNormalVCFs(filePaths, vcfDir)
        sys.stderr.write("2. Combining VCFs into one large one for matrix writing using vcf-merge...")
        #fullVCFPath = self.combineVCFs(normalVCFFilepaths, vcfDir)
        fullVCFPath = "/Volumes/Costello/TMZ/Molecular/Germline-Variants/allPatientsVCF.vcf.gz"
        sys.stderr.write("3. Writing matrix using vcf-tools...")
        self.makeMatrix(fullVCFPath, vcfDir, output)
        sys.stderr.write("4. Adding patient and variant info to matrix...")
        self.addPatientAndVariantInfo(output, vcfDir)
        
    def parseSampleNameMap(self, sampleNameMap):
        '''
        Parse VCF map to <Patient> -> <libID>
        '''
        patientToNormalLibID = {}
        reader = csv.DictReader(sampleNameMap, delimiter = '\t')
        for row in reader:
            if row['sample_type'] == "Normal":
                patientToNormalLibID[row['patient_ID']] =row['lib_ID']
        return patientToNormalLibID
            
            
    def outputNormalVCFs(self, vcfFilePaths, vcfDir):
        '''
        Takes a list of VCF file paths, and calls vcftools' vcf-subset to select the normal patient from each and output a VCF file for each
        '''        
        
        writtenVCFFilenames = []
        for filePath in vcfFilePaths:
            patientName = filePath.split(os.sep)[-1].split(".")[0]
            sys.stderr.write("Patient {0}...".format(patientName))
            
            # Make a VCF that's just the SNPs called in the normals  
            vcfNormalfilepath =  vcfDir  + os.sep +  "normal--" + patientName + ".vcf"
            writtenVCFFilenames += [vcfNormalfilepath]
            if (os.path.exists(vcfNormalfilepath) and os.stat(vcfNormalfilepath).st_size > 0) or \
            (os.path.exists(vcfNormalfilepath + ".gz") and os.stat(vcfNormalfilepath + ".gz").st_size > 0):

                sys.stderr.write("Skipping {0}; already exists ({1})\n".format(patientName, vcfNormalfilepath))
                continue
            vcfNormalFile = open(vcfNormalfilepath, 'wb')
            call(["/usr/local/bin/vcf-subset", "--columns", self.patientToNormalLibID[patientName], filePath], \
                 env={"PERL5LIB": "/Users/ijoseph/Code/SoftwareSource/vcftools_0.1.12b/perl:$PERL5LIB"}, 
                 stdout=vcfNormalFile)
            
            
        
        return writtenVCFFilenames
    
    def combineVCFs(self, normalVCFFilepaths, vcfDir):        
        '''
        bgzip and tabix (based on samtools) each normal vcf, then combine using vcf-merge        
        '''
        
        fullVCFPath = vcfDir + os.sep + "allPatientsVCF.vcf"
        if (os.path.exists(fullVCFPath))  and (os.stat(fullVCFPath).st_size > 0):
            sys.stderr.write("Merged file to already exist ({0}); done.\n".format(fullVCFPath))
            return fullVCFPath

        
        # bgzip and tabix
        
        for filePath in normalVCFFilepaths:            
            sys.stderr.write("bgzipping and tabixing {0}\n".format(filePath))
            call(["/usr/local/bin/bgzip", filePath])            
            call(["/usr/local/bin/tabix","-p", "vcf", filePath + ".gz"])
            
        
        normalVCFFilepaths = [fileName + ".gz" for fileName in normalVCFFilepaths]
        
        fullVCFFile = open(fullVCFPath, 'wb') 
        call(["/usr/local/bin/vcf-merge"] +  \
             normalVCFFilepaths, \
             env={"PERL5LIB": "/Users/ijoseph/Code/SoftwareSource/vcftools_0.1.12b/perl:$PERL5LIB"},
             stdout=fullVCFFile)
        
        sys.stderr.write("done.")
        return fullVCFPath
    
    def makeMatrix(self, fullVCFPath, vcfDir, output):
        '''
        Makes the full matrix
        '''
        
        fullMatrixBase = output
        if os.path.exists(fullMatrixBase + ".012") and (os.stat(fullMatrixBase + ".012").st_size > 0 ):
            sys.stderr.write("Looks like it's already made ({0})\n".format(fullMatrixBase + ".012"))
            return fullMatrixBase         
        # Gets rid of germline variants that are missing in any
        call(["/usr/local/bin/vcftools", "--gzvcf", fullVCFPath,  "--out", fullMatrixBase, "--012", "--max-missing", str(self.maxMissing)])
    
    def addPatientAndVariantInfo(self,fullMatrixBase, vcfDir ):
        '''
        Takes the matrix and add column names and row names according to .pos column name file and .indv 
        '''
        
        with open(fullMatrixBase + ".012.pos", 'r') as headerFile:            
            headerList = [pos.replace("\t","_") for pos in headerFile.read().split("\n")]
        
        with open(fullMatrixBase + ".with-names.txt", 'w') as outputFile:
            # Write header
            outputFile.write("patient_name\t" + "\t".join(headerList) + "\n")
            with open(fullMatrixBase + ".012", 'r') as matrixFile:                
                with open(fullMatrixBase + ".012.indv", 'r') as personFile:                
                    for line in matrixFile:
                        outputFile.write(self.normalVCFSampleIDToPatientID[personFile.readline().strip()] + "\t")
                        # Remove first column
                        outputFile.write("\t".join(line.strip().split()[1:len(line.strip().split())]) + "\n")
        sys.stderr.write("names added to file {0}\n".format(outputFile.name))            
                
                

            
        
    


def main():
    class readable_dir(argparse.Action):
        '''
        Class to take a direcotry as an input argument
        '''
        def __call__(self,parser, namespace, values, option_string=None):
            prospective_dir=values
            if not os.path.isdir(prospective_dir):
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
            if os.access(prospective_dir, os.R_OK):
                setattr(namespace,self.dest,prospective_dir)
            else:
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))
            
    parser = argparse.ArgumentParser(description = "description")    
    parser.add_argument('-o', '--output', type=str, help= "File to which to output outputdetails" ,required=True)
    parser.add_argument("--vcfFileDir", action=readable_dir, required=True, help="Directory containing VCF files")
    parser.add_argument("--sampleNameMap", type=file, required=True, help="Map from patient name to name of VCF file")
    parser.add_argument("--maxMissing", type=float, default = 1.0, help="max-missing")
    
    namespace = parser.parse_args()
    
    MatrixFromVCF(namespace.vcfFileDir, namespace.sampleNameMap, namespace.output, namespace.maxMissing)
        
if __name__ == '__main__':
    main()