'''
Created on Oct 15, 2014

@author: ijoseph

Takes in Costello several somatic mutation format file and outputs a matrix, where each column is a particular somatic mutation 
in at least one of the files, and each row is one particular patient. Coding is 0,1,2 based on zero, one, or two copies of a particualr 
mutation present in a patient.
    
'''
import argparse
import sys
import csv
from collections import defaultdict
from collections import Counter

class Mutation:
    def __init__(self, gene, contig, position, ref_allele, alt_allele):
        self.gene = gene
        self.contig = contig
        self.position = position
        self.ref_allele = ref_allele
        self.alt_allele = alt_allele
        
        
        
    def __hash__(self):
        return hash((self.gene, self.contig, self.position, self.ref_allele, self.alt_allele))
    
    def __eq__(self, other):
        return (self.gene, self.contig, self.position, self.ref_allele, self.alt_allele) == (other.gene, other.contig, other.position, other.ref_allele, other.alt_allele)
    
    def __str__(self):
        return "{0}_{1}:{2};{3}->{4}".format(self.gene, self.contig, self.position, self.ref_allele, self.alt_allele)
    
    def __repr__(self):
        return self.__str__()


class MatrixFromCostelloSomaticMutations(object):
    '''
    '''

    def __init__(self, mutationFile, output, resolution, multiple, selectPatients, primaryOnly):
        '''
        Constructor: takes in the mutation file directory and gets a list of files. 
        '''        
        self.output = output
        self.multiple = multiple
        self.selectPatients = self._parseSelectPatients(selectPatients)        
        self.primaryOnly = primaryOnly # only keep primary samples
        self.patients = []     
        
        (self.sampleTypeToNucleotideMutation, self.nucleotideMutationList, self.sampleTypeToGeneMutation, self.geneMutationList) = self._parseMutationFiles(mutationFile)
        # sampleTypeToNucleotideMutation: <sample ID> -> <list Mutation>  
        if resolution == "nucleotide":
            self.outputMatrix(self.sampleTypeToNucleotideMutation, self.nucleotideMutationList)        
        elif resolution == "gene":
            self.outputMatrix(self.sampleTypeToGeneMutation, self.geneMutationList)            
        else:
            sys.stderr.write("Unknown resolution; exiting")
            sys.exit(1)
        
        sys.stderr.write("Output {0} samples from {1} different patients".format(len(self.sampleTypeToNucleotideMutation.keys()), len(set(self.patients))))
        
    def _parseSelectPatients(self, selectPatients):
        '''
        Parse list of selected patients into either string vector with patient IDs, or 'all'
        '''
        if selectPatients == "all":
            return selectPatients
        else:
            return ["Patient" + i for i in selectPatients.split(",")]
        
    def _parseMutationFiles(self, mutationFile):
        '''
        Get hash: 
        mutation (#gene, contig, position, ref_allele, alt_allele) -> (patient_ID, sample_type) (this is the sub-patient identifier)
        
        then convert to 
        sample_type -> mutation -> number of occurrences  
        '''
        
        reader = csv.DictReader(mutationFile, delimiter = '\t')
        sampleTypeToNucleotideMutation = defaultdict(list)
        sampleTypeToGeneMutation = defaultdict(list)
        nucleotideMutationSet = set()
        geneMutationSet = set()
         
        # problem: in this scheme a mutation can only occur once; if it's called in multiple patients it will be ignored. Perhaps reverse it from the beginning, and add to the list?  
        
        for row in reader: # in this scheme
            # Ignore patients that aren't selected
            if not self.selectPatients == "all":
                if row['patient_ID'] not in self.selectPatients:
                    continue
            
            # Ignore non-primary samples if applicable
            if self.primaryOnly:
                if  ("Primary" not in row['sample_type']) and ("Initial" not in row['sample_type']):
                    continue
            
            # Ignore header rows
            if row['#gene'] == '#gene':
                continue
            
            
            mutation = Mutation(row['#gene'], row['contig'], row['position'], row['ref_allele'], row['alt_allele'])
            sampleType = row['patient_ID'] +"_" +  self._normalizeSampleType(row['sample_type'])
            
            self.patients += [row['patient_ID']] 
                        
            nucleotideMutationSet = nucleotideMutationSet.union([mutation])
            geneMutationSet =geneMutationSet.union([row['#gene']])
            
            sampleTypeToNucleotideMutation[sampleType] += [mutation]
            sampleTypeToGeneMutation[sampleType] +=[row['#gene']]
        
        
        
        # Postprocess: change the mutations into (mutation, occurrences)
        sampleTypeToNucleotideMutationDict = {}
        for (sampleID, mutationList) in sampleTypeToNucleotideMutation.items():
            sampleTypeToNucleotideMutationDict[sampleID] = Counter(mutationList)
        
        # Same for gene level mutations
        sampleTypeToGeneMutationDict = {}
        for (sampleID, mutationList) in sampleTypeToGeneMutation.items():
            sampleTypeToGeneMutationDict[sampleID] = Counter(mutationList)
        
        
        
        return (sampleTypeToNucleotideMutationDict, list(nucleotideMutationSet), sampleTypeToGeneMutationDict, list(geneMutationSet))
    
    def _normalizeSampleType(self, sampleType):
        '''
        Converts from 'science' paper notation to 
        MGS notation: 
        'Initial' -> 'Primary' (substring replace)
        Remove spaces
        Trailing A to 1, etc.
        '''
        sampleType = sampleType.replace("Initial", "Primary")
        sampleType = sampleType.replace(" ", "")
        
        letterToIntDict = {"A":"1", "B":"2", "C":"3", "D":"4"}        
        try:
            sampleType = sampleType[0:len(sampleType)-1] + letterToIntDict[sampleType[-1]]
        except:
            pass
        
        # Trim the '1' off for consistency
        if (sampleType.endswith("Recurrence1") or sampleType.endswith("Primary1")):
            sampleType = sampleType[0: len(sampleType)-1]
        
        # Remove the 'v'     
        if (sampleType[0:len(sampleType)-1].endswith("Recurrencev") or sampleType[0:len(sampleType)-1].endswith("Primaryv")):
            sampleType = sampleType[0: len(sampleType)-2] + sampleType[-1]
        return sampleType
        
    def outputMatrix(self, sampleTypeToMutation, mutationList):
        '''
        For each mutation in each sample, output a 1 if present, else zero. 
        '''
        # write header
        
        separator = "\t"
        
        self.output.write("Patient_ID_sample_type" +separator + separator.join([str(mutation) for mutation in mutationList]) + "\n")
        
        for (sampleID, mutationsInSampleList) in sampleTypeToMutation.items(): # Loop through all mutations found in this sample
            self.output.write(sampleID +separator) # write the patient name            
            for possibleMutation in mutationList: # For each possible mutation, see if it's the sample and output 0,1,or 2 for number of times found in the patient
                if possibleMutation in mutationsInSampleList.keys():
                    if self.multiple:
                        self.output.write(str(mutationsInSampleList[possibleMutation]) + separator)
                    else:
                        self.output.write('0' if mutationsInSampleList[possibleMutation] == 0 else  '1'  + separator)
                        
                else:
                    self.output.write("0" + separator)
            self.output.seek(-1,1) # get rid of the last separator by seeking back to it           
            self.output.write("\n")





def main():    
    parser = argparse.ArgumentParser(description = """Takes in Costello several somatic mutation format file and outputs a matrix, where each column is a particular somatic mutation 
in at least one of the files, and each row is one particular patient. Coding is 0,1,2 based on zero, one, or two copies of a particualr 
mutation present in a patient.""") 
    parser.add_argument("somaticMutationFile", type=file, help="somatic mutation file with headers\n #gene    contig    position    ref_allele    alt_allele    nucleotide    protein    context    type    patient_ID    sample_type")    
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "File to which to output output")
    parser.add_argument('-mr', '--mutationResolution', type=str, default ="gene", help= "resolution on which to output mutations (\"gene\" or (single) \"nucleotide\" [gene]")
    parser.add_argument('-mt', '--multiple', default =False, action = "store_true", help= "Count multiple occurrences of a mutation as a 2 in the matrix? [False]")
    parser.add_argument('-sp', '--selectPatients', default ="all", type=str, help= "Patient ID numbers to keep. Default keeps all.")
    parser.add_argument('-po', '--primaryOnly', default = False, action = "store_true", help= "Whether or not to keep only primary samples [False]")
#    mut_parser.add_argument('-p', '--numThreads', type=int, default= multiprocessing.cpu_count()/2, help = "Number of threads to run [(# of CPUs)/2]")
#   mut_parser.add_argument('--tempFolder', default="temp/", help= "temporary folder for files [./temp]")
    namespace = parser.parse_args()
    MatrixFromCostelloSomaticMutations(namespace.somaticMutationFile, namespace.output, namespace.mutationResolution, namespace.multiple, namespace.selectPatients, namespace.primaryOnly)    
        
if __name__ == '__main__':
    main()