# -*- coding: utf-8 -*-
'''
Created on Apr 22, 2016

@author: ijoseph
'''
import argparse, sys, multiprocessing
import csv


class RecodeVCF(object):
    '''
    classdocs
    '''


    def __init__(self, nameFile, vcf, output):
        '''
        Constructor
        '''
        libToPatientID = self.parseSampleNameMap(nameFile)        
        for vcfStr in vcf:                
            for (libName, patientName) in libToPatientID.iteritems():
                vcfStr = vcfStr.replace(libName,patientName)
            output.write(vcfStr)

        
        
    
    def parseSampleNameMap(self, sampleNameMap):
        '''
        Parse VCF map to <Patient> -> <libID>
        '''
        libToPatientID = {}
        reader = csv.DictReader(sampleNameMap, delimiter = '\t')
        for row in reader:
            if row['sample_type'] == "Normal":
                libToPatientID[row['lib_ID']] =row['patient_ID']
        return libToPatientID
        
        
        
        
        


def main():    
    parser = argparse.ArgumentParser(description = "description") 
    parser.add_argument("--vcf", type=file, help="input inputhelp")    
    parser.add_argument("--nameFile", type=file, help="input inputhelp")    
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "File to which to output outputdetails")
    parser.add_argument('-p', '--numThreads', type=int, default= multiprocessing.cpu_count()/2, help = "Number of threads to run [(# of CPUs)/2]")
    parser.add_argument('--tempFolder', default="temp/", help= "temporary folder for files [./temp]")
    namespace = parser.parse_args()
    
    RecodeVCF(namespace.nameFile, namespace.vcf, namespace.output)
        
if __name__ == '__main__':
    main()