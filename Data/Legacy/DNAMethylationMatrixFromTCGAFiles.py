'''
Created on May 14, 2015

@author: ijoseph
'''
import argparse, sys
import csv
import os


class MethMat(object):
    '''
    Holds data for converting a directory with DNA methylation files from TCGA into a matrix
    '''


    def __init__(self, methylationFileDirs, output):
        '''
        Reads all the files in this directory and creates relevant hashes to make a matrix
        '''
        self.output= output
        filePaths = [os.path.join(methylationFileDirs,fn) for fn in next(os.walk(methylationFileDirs))[2]]
        (self.barcodeToCGidToValue, self.cgIDs) = self.methylationFilesToHash(filePaths)
        self.outputMatrix()
        
                
    def methylationFilesToHash(self, methylationFileList):
        '''
        Makes a <barcode> -> <cg_id> -> < value> map 
        '''
        barcodeToCGidToValue = dict()
        cgIDs = []
        
        for methFileName in methylationFileList:
            sys.stderr.write("Reading file {0} of {1}:".format(methylationFileList.index(methFileName) +1, len(methylationFileList)) + "\n" + methFileName + "\n")
            
            methFile = open(methFileName)            
            firstLine = next(methFile) # skip first row            
            barcode = firstLine.split()[-1]
            
            reader = csv.DictReader(methFile, delimiter = '\t')            
            for row in reader:
                try:
                    barcodeToCGidToValue[barcode][row['Composite Element REF']] = row['Beta_value']
                except KeyError: # maybe barcode dict doesn't exist yet                    
                    barcodeToCGidToValue[barcode] = dict()
                    barcodeToCGidToValue[barcode][row['Composite Element REF']] = row['Beta_value']
                cgIDs += [row['Composite Element REF']]
            
        
        return (barcodeToCGidToValue, list(set(cgIDs)))
                
    
    def outputMatrix(self):
        '''
        For each mutation in each sample, output a 1 if present, else zero. 
        '''
        # write header
        
        self.output.write("Barcode" +"\t" + "\t".join([str(cgid) for cgid in self.cgIDs]) + "\n")                
        
        for (barcode, cgToBeta) in self.barcodeToCGidToValue.items():
            self.output.write(barcode + "\t")
            for cgID in self.cgIDs:
                try:
                    self.output.write(cgToBeta[cgID])
                except KeyError:
                    self.output.write("NA")
                self.output.write("\t")
            self.output.write("\n")
                

def main():
    
    class readable_dir(argparse.Action):
        '''
        Class to take a direcotry as an input argument
        '''
        def __call__(self,parser, namespace, values, option_string=None):
            prospective_dir=values
            if not os.path.isdir(prospective_dir):
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
            if os.access(prospective_dir, os.R_OK):
                setattr(namespace,self.dest,prospective_dir)
            else:
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))
        
    parser = argparse.ArgumentParser(description = "Converts a directory with DNA methylation files from TCGA into a matrix") 
    parser.add_argument("--methylationFileDir", action=readable_dir, required=True, help="Directory containing methylation files")    
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "matrix output file" ,required=False)
#     mut_parser.add_argument('-p', '--numThreads', type=int, default= multiprocessing.cpu_count()/2, help = "Number of threads to run [(# of CPUs)/2]")
#     mut_parser.add_argument('--tempFolder', default="temp/", help= "temporary folder for files [./temp]")
    namespace = parser.parse_args()
    
    MethMat(namespace.methylationFileDir, namespace.output)
    
        
if __name__ == '__main__':
    main()