'''
Created on Jan 14, 2016

@author: ijoseph
'''
class Tree:
    def __init__(self, paramData, left=None, right=None, parent=None):
        self.paramData = paramData
        self.left  = left
        self.right = right
        self.parent = parent
        # Set children
        self.children = []
        if self.left is not None: 
            self.children += self.left
        if self.right is not None:
            self.children += self.right
        
    
    
    def insertRight(self,newNode):
        if self.right == None:
            self.right = Tree(newNode)
            self.right.parent = self
            self.children += [self.right]
     
    def insertLeft(self,newNode):
        if self.left == None:
            self.left = Tree(newNode)
            self.left.parent = self
            self.children += [self.left]
            
    def __str__(self):        
        return ("TreeNode: {0} ; with children: {1} and parent: {2}".format(str(self.paramData), [str(child.paramData) for child in self.children if child is not None], str(self.parent.paramData) if self.parent is not None else ""))
    
    

