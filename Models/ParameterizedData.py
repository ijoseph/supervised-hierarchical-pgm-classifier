# -*- coding: utf-8 -*-
"""
Created on Jan 14, 2016

@author: ijoseph
"""
from __future__ import division

import sys

import numpy as np
import pandas as pd
import sklearn.decomposition
import copy
import scipy
import logging
import IPython.core.debugger



class ParameterizedData(object):
    """
    Holds data and parameters for a Gaussian node in a tree structure;
    knows how to simulate based on its parameters; also knows how
    to initialize random values for its parameters
    """
    
    def __init__(self, dimension, parent_dimension, data=None, var_name=""):
        self.W = None
        self.var_name = var_name
        self.dimension = dimension   
        self.parent_dimension = parent_dimension
        self.data = data
        self.jointPrecisionIndices = (None, None)  # indices of this data in joint precision matrix
        self.sumSecondMoment_E_z_z_T = None  # E[zᵢzᵢ]
        self.invSecondMoment_E_z_z_T = None  # E[zᵢzᵢ]⁻¹
        self.sumParentSelfCrossExpectation_E_z_z_T = None  # E[zⱼzᵢ] for j = parent
        self.pPCA_estimate_list = []

        self.set_data(data)


        
    def create_random_w(self, w_mean=0):
        """
        Creates a random W based on this node's dimension; based off of Shannon's code
        """
        if self.parent_dimension is not None:
            self.W = np.random.multivariate_normal(
                np.ones(self.parent_dimension) * w_mean, np.diag(np.ones(self.parent_dimension)), self.dimension)

            return self.W
    
    def set_data(self, data):
        """
        :param data:
        :return:
        """
        if data is None:
            data = pd.DataFrame(columns=[self.var_name + "_" + str(idx) for idx in range(self.dimension)])

        assert data.shape[1] == self.dimension, "Function parameters [dimension] or [data]'s dimension don't match"
        self.N = float(data.shape[0]) # set N
        self.data = pd.DataFrame(data)

        check_for_missing_data(self.data, "self.data after setting from set_data")


class GaussianData(ParameterizedData):
    """
    Holds data and parameters for a Gaussian node in a tree structure;
    knows how to simulate based on its parameters; also knows how
    to initialize random values for its parameters 
    """

    
    def __init__(self, dimension, parent_dimension, data = None, var_name ="", diagonalConditionalVariance = False):
        """
        Initialize paramData; sets a dimension, and initilaizes the sizes of Psi and W, 
        which govern the conditional distribution of this variable based on its 
        parent in the tree. data should be a pandas data.frame
        """

        
        super(GaussianData, self).__init__(dimension, parent_dimension, data, var_name)
        self.diagonalConditionalVariance = diagonalConditionalVariance
                        
        # Initialize parameters  as empty DataFrames
        if self.parent_dimension is not None:
            # Psi
            if not diagonalConditionalVariance:            
                self.Psi = pd.DataFrame(columns=range(self.dimension), index=range(self.dimension))
            else:
                # self.Psi = pd.DataFrame(index=[0], columns=range(self.dimension))
                self.Psi = pd.Series(index=range(self.dimension))  # changed. (first)
                            
            # W
            self.W = pd.DataFrame(index=range(self.dimension), columns= range(self.parent_dimension))

    def simulate(self, parent): # takes all the time
        """
        Simulates one data point given the parameters of this and parents' point; appends the result to self.data
        """
        if parent is None: # root; assume prior ~ N(0,I)            
            self.data.loc[self.data.shape[0]] = np.random.normal(loc=0.0, scale=1.0, size=self.dimension)
            return

        mu = np.dot(self.W, parent.paramData.data.iloc[parent.paramData.data.shape[0] - 1])
        # non-root: generate data as a function of parent        
        if self.diagonalConditionalVariance:
            psi = self.Psi  #changed
            self.data.loc[self.data.shape[0]] = np.array([np.random.normal(loc=mu_i, scale=np.sqrt(psi_i))
                                                          for (mu_i, psi_i) in zip(mu, psi)]).T
        else:
            self.data.loc[self.data.shape[0]] = np.random.multivariate_normal(mean=mu, cov=self.Psi)


    def create_warm_start_parameters(self, parent_node_paramData, debug_counts=False):
        """
        Sets self.W and self.Psi based on 'warm start,' based, in turn, on pPCA based on the data contained
        by this node
        :return:
        """
        if (self.data.size == 0):
            if (not len(self.pPCA_estimate_list)):
                raise ValueError("No data to perform warm start with for {0}".format(self))
            else:
                data_estimate = np.mean([estimate for estimate in self.pPCA_estimate_list], axis=0)

            # logging.info("Data size zero, now is\n{0}".format(data_estimate))
        else:
            # logging.warning("Data size NON-zero\n{0}".format(self.data))
            data_estimate = self.data

        if self.parent_dimension is not None:
            if self.parent_dimension <= self.dimension:
                if debug_counts:

                    # if not np.isfinite(data_estimate.sum().all()) and not np.isfinite(data_estimate).all():

                    if np.count_nonzero(np.isnan(data_estimate)) > 0:
                        logging.warning("{0} NaNs in data estimate:\n{1}".format
                                        (np.count_nonzero(np.isnan(data_estimate)), data_estimate))

                        logging.warning("{0} Non-finites in data estimate:\n{1}".format
                                        (np.count_nonzero(~np.isfinite(data_estimate)), data_estimate))

                        for est in self.pPCA_estimate_list:
                            logging.warning("pPCA estimate list {0}".format(est))

                self.pPCA_warm_start(data_estimate, parent_node_paramData)

            else: #do my custom warm start
                self.variance_maintaining_warm_start(parent_node_paramData=parent_node_paramData)
        else:
            self.createRandomPsi() # just make the Psi as an identity

    def variance_maintaining_warm_start(self, parent_node_paramData):
        if self.dimension > 1:
            component_variance = np.cov(self.data.T)
        else:
            component_variance = np.var(self.data)
        # diag(A)
        diagonal_variance = np.diagflat(np.diag(component_variance))
        # non-diag(A)
        off_diagonal_variance = copy.deepcopy(component_variance)
        try:
            np.fill_diagonal(off_diagonal_variance, 0)
        except ValueError:
            off_diagonal_variance = 0
        # Set \hat{Ψ}_{warm start}
        self.Psi = np.diag(off_diagonal_variance + 0.5 * diagonal_variance) # changed this
        # Set \hat{W}_{warm start}
        augmented_diagonal = np.concatenate([scipy.linalg.sqrtm(diagonal_variance),
                                             np.zeros((self.dimension,
                                                       self.parent_dimension - self.dimension))], axis=1)
        self.W = (float(1) / np.sqrt(2)) * augmented_diagonal

        # Get prediction for parent node
        parent_estimate = self.predict_parent_node()

        parent_node_paramData.pPCA_estimate_list += [parent_estimate]

    def predict_parent_node(self):
        if self.diagonalConditionalVariance:
            M = self.W.T.dot(self.W) + np.mean(np.diagflat(self.Psi)) * np.identity(self.parent_dimension)  # change
        else:
            M = self.W.T.dot(self.W) + np.mean(np.diag(self.Psi)) * np.identity(self.parent_dimension)  # change
        parent_estimate = np.linalg.inv(M).dot(self.W.T).dot(self.data.T)
        return parent_estimate.T

    def pPCA_warm_start(self, data_estimate, parent_node_paramData, debug=False):
        pPCA_object = sklearn.decomposition.PCA(n_components=self.parent_dimension).fit(data_estimate)
        # Set one's own parameters
        self.W = pPCA_object.components_.T

        if self.diagonalConditionalVariance:
            self.Psi = np.array([pPCA_object.noise_variance_ ]* self.dimension)
        else:
            self.Psi = np.dot(pPCA_object.noise_variance_, np.identity(self.dimension))  # \hat{Ψ}_{warmstart} =  σ²I # changed

        if debug:
            check_for_missing_data(self.Psi, "Psi after pPCA warm start")
            if self.diagonalConditionalVariance:
                check_for_zeros(self.Psi, "Psi after pPCA warm start" )


        self.check_for_low_rank_parameters()


        # Add latent data approximation to parent
        parent_node_paramData.pPCA_estimate_list += [pPCA_object.transform(data_estimate)]

    def createRandomPsi(self, psiMean = 0):
        """
        Creates a random Psi according to this node's dimension
        """
        if self.parent_dimension is not None:
            if self.diagonalConditionalVariance: # diagonal conditional variance           
                # Ψᵢⱼ ~ Gamma(α = psiMean, β = 1) ⇒ E[each element] = psiMean · 1 
                self.Psi =  np.random.gamma(shape=(psiMean +1), scale=1.0, size = self.dimension) # changed
            else:        
                X = np.random.normal(loc=psiMean, scale=1.0, size = (self.dimension, self.dimension)) # X: each element ~ N(psiMean, 1.0)
                self.Psi = np.dot(X, X.T) # Ψ = X Xᵀ
        else: # no parent. 
            self.Psi = np.array([1]*self.dimension)  # top level node; always has covariance I # changed
        return self.Psi 

    def MStep(self, parent, children):
        """
        Performs M step: finds W_mle, M_mle based on self and parent          
        """
        if children != []:# interior node: hidden node but non-root; e.g. z_2
            ## Update W 
            # W_mle = ( sum over samples of E[zⱼzⱼ])⁻¹ · (sum over samples of E[zⱼzᵢ]) for parent = j (section 3.1)
            self.W = np.dot(parent.paramData.invSecondMoment_E_z_z_T, self.sumParentSelfCrossExpectation_E_z_z_T).T
            
            ## Update Ψ
            # Ψ_mle = 1/N ( E[zᵢzᵢ] -  E[zⱼzᵢ]ᵀ · E[zⱼzⱼ]⁻¹ · E[zⱼzᵢ] )for parent = j (section 3.2)
            self.Psi = (1/self.N) * \
                           ( self.sumSecondMoment_E_z_z_T - np.dot(self.W, self.sumParentSelfCrossExpectation_E_z_z_T))

        else: # observed node
            try:
                data_T_dot_data = self.data_T_dot_data
            except AttributeError:
                data_T_dot_data = np.dot(self.data.T, self.data)
                self.data_T_dot_data = data_T_dot_data


            ## Update W 
            # W_mle^T = ( sum over samples E[zⱼzⱼ]) ⁻¹ · (sum over samples E[zⱼ]xᵢᵀ) for parent = j (section 3.1) 
            self.W = np.dot(parent.paramData.invSecondMoment_E_z_z_T, np.dot(parent.paramData.data.T, self.data)).T
             
            ## Update Ψ
            # Ψ_mle = 1/N ( xᵢxᵢᵀ -  xᵢE[zⱼ]ᵀ · E[zⱼzⱼ]⁻¹ · E[zⱼ]xᵢᵀ) for parent = j (section 3.2)            
            selfParentOuterProduct = np.dot(self.data.T, parent.paramData.data)  # xᵢE[zⱼ]ᵀ
            self.Psi = (1/self.N) * (data_T_dot_data -
                                     np.dot(selfParentOuterProduct,
                                            np.dot(parent.paramData.invSecondMoment_E_z_z_T.T,
                                                   selfParentOuterProduct.T)))


            check_for_missing_data(data_to_check=self.W, name="self.W after M")
            check_for_missing_data(data_to_check=self.Psi, name="self.Psi after M")

            
        
        if self.diagonalConditionalVariance: # Ψ_mle = diag(Ψ_mle)
            self.Psi = np.diag(self.Psi)

    def logLikelihood(self, parent):
        """
        Given the current data that this piece has and its current parameters, return its likelihood based on 
        log(P(x)) = -dimension/2 log(2π) - 1/2 log(|Ψ|) - 1/2 ((data - W parentData)^T Ψ⁻¹ (data - W parentData))
        """
        
        try:
            log_det_psi = np.log(np.linalg.det(self.Psi))

            if self.diagonalConditionalVariance:
                psi_inv = np.diagflat(self.Psi**-1.0) # changed
            else:
                psi_inv = np.linalg.inv(self.Psi)

            if parent is not None:
                return sum( [ - (self.dimension/2.0) * np.log(2.0 * np.pi) \
                    - (1/2.0) * log_det_psi\
                    - (1/2.0) * (self.data.iloc[n,:].T -
                               self.W.dot(parent.paramData.data.iloc[n,:].T)).T.
                            dot(psi_inv).
                            dot((self.data.iloc[n,:].T - self.W.dot(parent.paramData.data.iloc[n,:].T)))
                              for n in range(int(self.N))])
            else:
                return - self.N * (self.dimension/2.0) *np.log(2.0 * np.pi) - \
                       sum( [   (1/2.0) * self.data.iloc[n,:].dot(self.data.iloc[n,:].T) for n in range(int(self.N))])
            
        except AttributeError as e:
            sys.stderr.write("{0} \n\nError -- "
                             "parameters not initialized before likelihood; please initialize parameters.".format(e))
            sys.exit(1)
            
    def check_for_low_rank_parameters(self):
        try:
            if self.diagonalConditionalVariance:
                low_rank = (min(abs(self.Psi)) == 0)

            elif np.linalg.matrix_rank(self.Psi) < self.dimension:
                low_rank = True
            else:
                low_rank = False
        except ValueError:
            low_rank = True

        if low_rank:
            logging.debug("Node `{0}` has a Ψ rank deficient.  \n"
                            "Replacing zero eigenvalues with 1 if diagonal Ψ, else identity."
                            .format(self))

            logging.info("Full Ψ:\n{0}".format(self.Psi))

            if self.diagonalConditionalVariance:
                psi_copy = self.Psi.copy()
                psi_copy[psi_copy == 0] = 1
                self.Psi = psi_copy
                # np.place(self.Psi, self.Psi == 0, 1)
            else:
                self.Psi = np.identity(self.dimension)  # changed

        if self.diagonalConditionalVariance:
            check_for_zeros(self.Psi)


    def __str__(self):
        """
        String representation:
        """
        return(self.var_name + " dimension {0}".format(self.dimension))


def check_for_missing_data(data_to_check, name=""):
    """
    Checks a Pandas DataFrame or Numpy Array for missing data
    :param data_to_check:
    :param name:
    :return:
    """
    if isinstance(data_to_check, pd.DataFrame):
        if data_to_check.isnull().any().any():
            logging.warning("{0} NaNs or something in DataFrame {1}:\n{2}"
                            .format(data_to_check.isnull().sum().sum(), name, data_to_check))
            IPython.core.debugger.Tracer()()
    elif isinstance(data_to_check, np.ndarray):
        if np.count_nonzero(np.isnan(data_to_check)) > 0:
            logging.warning("{0} NaNs or something in ndarray {1}:\n{2}"
                            .format(np.count_nonzero(np.isnan(data_to_check)), name, data_to_check))
            IPython.core.debugger.Tracer()()

        if np.count_nonzero(~np.isfinite(data_to_check)) > 0:
            logging.warning("{0} ∞ or something in ndarray {1}:\n{2}"
                            .format(np.count_nonzero(~np.isfinite(data_to_check)), name, data_to_check))
            IPython.core.debugger.Tracer()()

    else:
        logging.warning("Don't know how to check {0}".format(name))

def check_for_zeros(data_to_check, name=""):
    """
    Checks a Pnadas Data.Frame or Numpy Array for presence of any zeros
    :param data_to_check:
    :param name:
    :return:
    """
    if isinstance(data_to_check, pd.DataFrame):
        if (data_to_check == 0).any().any():
            logging.warning("{0} zeros in DataFrame {1}:\n{2}"
                            .format((data_to_check == 0).sum().sum(), name, data_to_check))
            IPython.core.debugger.Tracer()()

    elif isinstance(data_to_check, np.ndarray):
        if np.count_nonzero(data_to_check) < data_to_check.size: # there are less nonzero elements than totla
            logging.warning("{0} zeros in ndarray {1}:\n{2}"
                            .format(data_to_check.size - np.count_nonzero(data_to_check), name, data_to_check))
            IPython.core.debugger.Tracer()()







     
        
        
        
        
        
        