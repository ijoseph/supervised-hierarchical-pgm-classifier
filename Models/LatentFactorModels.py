# -*- coding: utf-8 -*-
'''
Created on Jan 14, 2016

@author: ijoseph
'''

from __future__ import division
import os
import sys

import Models
from ParameterizedData import GaussianData
from Tree import Tree
import numpy as np
import pandas as pd
import copy
import logging
import sklearn.exceptions
import feather
import ParameterizedData


class LatentFactorModel(object):

    def fit(self, X, y, num_EM_rounds=10, show_log_likelihood=False):
        '''
        Takes in feature matrix (independent variables) X, which
        is a matrix X ∈ ℝ^(n    ×   (p_1 + p_2 + p_3 + ... p_{b-1})) , where (b-1) is the number of features.

        Also takes in response matrix (dependent variable) y ∈ ℝ^(n   × p_b)
        (where b is the total number of observed variables). Classically p_b = 1; for example, for classification,
        this will be a one-dimensional boolean.

        However, model allows for higher-dimensional predictions as well.

        Assigns X, y to the model's tree structure as appropriate (via DFSSetPrecision ordering).

        Then, performs the EM algorithm to infer parameter values for X; E step consists of finding the joint
        precision matrix programmatically, then taking various inverses/ multiplying by X. M step consists of
        calling every child node's M step method. Performs for `num_EM_rounds` many rounds.
        '''

        new = copy.deepcopy(self)

        new._check_dimensions(X, y)

        y = y.astype(float)  # Convert possibly boolean output to numeric

        X_and_y = pd.concat([X, y], axis=1)  # Set dimensions

        # set N
        new.N = float(X_and_y.shape[0])  # set N (must be float for division computation reasons)

        # Center data and set mean parameters, μᵢ ∀ i
        X_and_y = new._center_and_set_means(X_and_y)

        new.setObservedData(X_and_y)
        new.initializeParameters()

        logging.info("Beginning {0} EM rounds...".format(num_EM_rounds))
        EM_round = 1

        while True:
            if num_EM_rounds == 'convergence':
                prevLogLike = new.logLikelihood()
            logging.info("round {0}".format(EM_round))

            new._E_step(X_and_y)
            new._M_step(X_and_y)

            if show_log_likelihood:
                currentLogLike = new.logLikelihood()
                logging.info(" (ℓ(Θ)={0:.5})...".format(currentLogLike))

            if num_EM_rounds == 'convergence':
                currentLogLike = new.logLikelihood()
                if EM_round == 0:
                    firstDifference = abs(currentLogLike - prevLogLike)
                else:
                    # original criterion: difference in log likelihood less than 1% of difference of original
                    if abs(currentLogLike - prevLogLike) < (abs(firstDifference) * 0.01):
                        break
            elif (isinstance(num_EM_rounds, int) or isinstance(num_EM_rounds, float)):
                if EM_round >= num_EM_rounds:
                    break

            EM_round += 1
        logging.info("done.\n")
        return new

    def __init__(self, dataTree=None, predictModel=None, yNode =None,
                 observed_dim=None, feature_dim = None, response_dim = None,
                 dimensions=None):

        self.dataTree = dataTree
        self.predictModel = predictModel
        self.yNode = yNode
        self.observed_dim = observed_dim
        self.feature_dim = feature_dim
        self.response_dim = response_dim
        self.dimensions = dimensions

        self._setIndices()


    def __add__(self, other):
        """
        Adding two models adds all relevant data
        :param other:
        :return:
        """
        new = copy.copy(self) # new to make

        new_nodes =  new._getHidden() + new._getObserved()
        other_nodes = other._getHidden() + other._getObserved()

        for (other_node, new_node) in zip(other_nodes, new_nodes):
            try: # pandas DataFrame
                new_node.data = pd.concat((new_node.data, other_node.data), axis=0)
            except TypeError:  # Numpy array data probably
                new_node.data = np.concatenate((new_node.data, other_node.data))

        return new

    def __str__(self):
        return "<{0} with dimensions (hidden first, then observed){1}>".format(self.__class__.__name__, self.dimensions)

    def initializeParameters(self, wMean = 0, psiMean = 0, warm_start = True):
        '''
        Tells each node s to generate random parameters Θ = {W_s, Ψ_s}
        '''
        nodes_in_reverse_bfs_order = []
        queue = [self.dataTree]
        paramList = []

        # Gather nodes in reverse BFS order
        while queue:
            vertex = queue.pop(0)
            if vertex is None:
                continue
            nodes_in_reverse_bfs_order += [vertex]
            queue.extend([vertex.left, vertex.right])  # add left node, right node

        nodes_in_reverse_bfs_order.reverse()

        if not warm_start:
            for vertex in nodes_in_reverse_bfs_order:
                # Tell this node to generate some Ws and Psis
                W = vertex.paramData.create_random_w(w_mean= wMean)
                Psi = vertex.paramData.createRandomPsi(psiMean = psiMean)
                paramList += [(W, Psi)]
            paramList.reverse()

        else: # warm start
            for vertex in nodes_in_reverse_bfs_order:
                logging.debug("nodes_in_reverse_bfs_order: {0}".format(
                    [str(node) for node in nodes_in_reverse_bfs_order]))
                try:
                    parent_paramData = vertex.parent.paramData
                except AttributeError:
                    parent_paramData = None
                vertex.paramData.create_warm_start_parameters(parent_node_paramData=parent_paramData)

        self.initialized = True
        
        return paramList
    


    def check_for_singular_parameters(self):
        """
        Goes through all model Ψs and checks for those that are singular
        :return:
        """
        (hidden_nodes, observed_nodes) = self._getNodesInOrder()

        for node in hidden_nodes + observed_nodes:
            node.paramData.check_for_low_rank_parameters()


    def clear_data(self):
        """
        Clear all data
        :return:
        """
        (hidden_nodes, observed_nodes) = self._getNodesInOrder()

        for node in hidden_nodes + observed_nodes:
            node.paramData.set_data(pd.DataFrame(columns=range(node.paramData.dimension)))

    def _check_dimensions(self, X, y):
        assert X.shape[1] == self.feature_dim, \
            "There should be the same number of features as specified on model initialization. " \
            "Specified {0}, got {1}".format(self.feature_dim, X.shape[1])

        try:
            y_dim = y.shape[1]
        except IndexError:
            y_dim = 1

        assert y_dim == self.response_dim, \
            "Response variable shoudl be the same as was specified on model initialization. " \
            "Specified {0}, got {1}".format(self.response_dim, y.shape[1])

    def _center_and_set_means(self, X_and_y):
        original_means = X_and_y.mean()
        self._setMeans(original_means=original_means)  # set \hat{μ}
        X_and_y = X_and_y.sub(original_means)  # center.
        return X_and_y

    def setObservedData(self, x_data_frame, test_for_missing=True):
        '''
        Takes in x_data_frame, which is a matrix X ∈ ℝ^(n × p_1 + p_2 + p_3 + ... p_b), where b is the number of observe datatypes
        assigns that to the model's tree structure as appropriate (via DFS ordering).
        
        Also sets the indices in the joint precision matrices corresponding to each paramData block.
        
        Mean subtracts for each column. 
        '''       

        if test_for_missing:
            if isinstance(x_data_frame, pd.DataFrame):
                if x_data_frame.isnull().any().any():
                    logging.warning("missing data found when setting:{0}".format(x_data_frame))
            else:
                logging.warning("so the data isn't even a data.frame. it's {0} {1}"
                                .format(type(x_data_frame), x_data_frame))

        (_, observedDataList) = self._getNodesInOrder()
        
        inputDataCol = 0
        for observedDataPiece in observedDataList:  # each of these is a ParameterizedData instance
            observedDataPiece.paramData.set_data(x_data_frame.iloc[:, inputDataCol:(inputDataCol + observedDataPiece.paramData.dimension)])
            inputDataCol += observedDataPiece.paramData.dimension

    def _setIndices(self):
        '''
        Sets the indices for each paramDatablock for corresponding precision matrix (observed and hidden, respectively)
        '''
        (hiddenDataList, observedDataList) = self._getNodesInOrder()
        
        hiddenDimCounter = 0
        for hiddenDataNode in hiddenDataList:
            hiddenDataNode.paramData.jointPrecisionIndices = (hiddenDimCounter, hiddenDimCounter + hiddenDataNode.paramData.dimension)
            hiddenDimCounter += hiddenDataNode.paramData.dimension


        observedDimCounter = 0
        for observedDataNode in observedDataList:
            observedDataNode.paramData.jointPrecisionIndices = (observedDimCounter, observedDimCounter + observedDataNode.paramData.dimension)
            observedDimCounter += observedDataNode.paramData.dimension

    def _getNodesInOrder(self):
        '''
        Does depth-first-search (DFS) to get nodes in order: (where c is number of hidden nodes, b is number of observed nodes)
        hidden: (z_1, z_2, ... z_c); observed: (x_1, x_2, ... x_b)
        '''
        # DFS
        observedList = []
        hiddenList = []        
    
        def DFSGetNodesInOrder(node, observedList, hiddenList):
            if node == None:
                return
            if (node.left == None) and (node.right == None):                
                # Observed data; add to observedList                
                observedList += [node]
            else:
                # Hidden data; add to hiddenList
                hiddenList += [node]

            '''
            Haven't visited left; let's do that.
            '''
            DFSGetNodesInOrder(node.left, observedList, hiddenList)
            DFSGetNodesInOrder(node.right, observedList, hiddenList)
                
        DFSGetNodesInOrder(self.dataTree, observedList, hiddenList)
        
        return (hiddenList, observedList)
   
    def _getObserved(self):
        (_, observed) = self._getNodesInOrder()
        return  [o.paramData for o in observed]
 
    def _getHidden(self):
        (hidden, _) = self._getNodesInOrder()
        return [h.paramData for h in hidden]                

    def _setMeans(self, original_means):
        '''
        Sets the means of the observed data, for later use during prediction.
        This is equivalent to setting \hat{μᵢ] ∀ i ∈ observed(i).
        '''
        for observed_data_piece in self._getObserved():
            observed_data_piece.mu_hat = original_means[observed_data_piece.jointPrecisionIndices[0]:\
                observed_data_piece.jointPrecisionIndices[1]].as_matrix()


    def getYNode(self):
        raise NotImplementedError()

    def _E_step(self, xDataFrame):
        raise (NotImplementedError())

    ''' M Step '''
    def _M_step(self, xDataFrame):    
        def DFSMStep(node):
            ''' 
            Goes through each node and updates \hat{W}, \hat{Psi} 
            based on E[zⱼzᵢ |x] for j = parent, E[zᵢzᵢ | x]
            '''
            if node is None:
                return

            if (node.parent is not None):
                node.paramData.MStep(node.parent, node.children) # call M step on data partition
            
            for child in node.children:
                DFSMStep(child)
        DFSMStep(self.dataTree)
    
    def logLikelihood(self):
        def DFSLogLikelihood(node):
            if node is None:
                return 0
            return node.paramData.logLikelihood(node.parent) + sum([child.paramData.logLikelihood(node) for child in node.children])
        return DFSLogLikelihood(self.dataTree)
    
    ''' I/O'''        
    def getObservedData(self):
        '''
        Gets xDataFrame of observed data
        '''        
        observedParamDataList = self._getObserved()
        return pd.concat([observedParamData.data for observedParamData in observedParamDataList], axis='columns')

    def getX(self):
        return self.getObservedData().iloc[:, 0:self.feature_dim]

    def getY(self):
        return self.getObservedData().iloc[
                         :, self.feature_dim:(self.response_dim + self.feature_dim)]

    # def save_data(self, path="."):


class GaussianLatentFactorModel(LatentFactorModel):

    ''' E step '''
    def _E_step(self, x_data_frame):
        '''        
        Find the joint precision matrix programaticaly
        '''

        self.check_for_singular_parameters()

        # Create joint precision Λ_{zz} as a function of Θ = {W_s, Ψ_s}
        self._createJointPrecision()  
        
        # Σ_{z|x} = Λ_{zz}⁻¹
        self.conditionalHiddenCovGivenObs_Sig_z_given_x = np.linalg.inv(self.hiddenHiddenJointPrec_Lambda_zz) 
        
        #  E[z| x] = μ_{z|x} = - Σ_{z|x} Λ_{zx} x
        # ∈ ℝ^(sum of hidden dimensions × n)
        self.conditionalHiddenExpGivenObs_mu_z_x = - np.dot(self.conditionalHiddenCovGivenObs_Sig_z_given_x,
                                                            np.dot(self.hiddenObservedJointPrec_Lambda_zx,
                                                                   x_data_frame.T)) # like Cn.dot(temp[:,dn])
        # ParameterizedData.check_for_missing_data(x_data_frame, name="x_data_frame")
        # ParameterizedData.check_for_missing_data(self.conditionalHiddenExpGivenObs_mu_z_x,
        #                             name="self.conditionalHiddenExpGivenObs_mu_z_x")
        #
        # ParameterizedData.check_for_missing_data(self.conditionalHiddenCovGivenObs_Sig_z_given_x,
        #                             name="self.conditionalHiddenCovGivenObs_Sig_z_given_x")
        #
        # ParameterizedData.check_for_missing_data(self.hiddenObservedJointPrec_Lambda_zx,
        #                             name="self.hiddenObservedJointPrec_Lambda_zx")




        # Sets E[zᵢ| x], E[zᵢzᵢ | x], E[zⱼzi | x] based on above calculations
        self._setConditionalHiddenExpGivenObs()



    def _setConditionalHiddenExpGivenObs(self):
        '''        
        For every hidden node i, 
        Sets E[zᵢ| x], E[zᵢzᵢ | x], E[zⱼzi | x] for j = parent of i (f i isn't root)
        '''
        def DFSSetConditionalHiddenDistribution(node):            
            
            if (node == None) or (node.children == []): # do nothing if observed or doesn't exist
                return
            
            (selfStart, selfEnd) = (node.paramData.jointPrecisionIndices[0], node.paramData.jointPrecisionIndices[1])
            ## Sets E[zᵢ| x] ∈ ℝ^(n × dimension of this hidden node)
            node.paramData.set_data(self.conditionalHiddenExpGivenObs_mu_z_x[selfStart:selfEnd, :].T)
            
            ##  Sets E[zᵢzᵢ | x] ∈ ℝ^(dimension of this hidden node × dimension of this hidden node)
            #  Get Σ_{zᵢzᵢ|x}
            
            # equations 14, 15: E[zᵢzᵢ | x] =  Σ_{zᵢzᵢ|x} + E[zᵢ| x] E[zᵢ| x]^T
            # ⇒ overall: (N *  Σ_{zᵢzᵢ|x}) + E[Zᵢ| x] E[Zᵢ| x]^T
            node.paramData.sumSecondMoment_E_z_z_T = self.N * self.conditionalHiddenCovGivenObs_Sig_z_given_x[selfStart:selfEnd, selfStart:selfEnd] + np.dot(node.paramData.data.T, node.paramData.data)
            # also compute (E[zᵢzᵢ | x])⁻¹
            node.paramData.invSecondMoment_E_z_z_T =  np.linalg.inv(node.paramData.sumSecondMoment_E_z_z_T) # this is the part that's whining in one of the tests.

            ParameterizedData.check_for_missing_data(node.paramData.invSecondMoment_E_z_z_T,
                                                     "During E: node.paramData.invSecondMoment_E_z_z_T")

            ParameterizedData.check_for_missing_data(node.paramData.invSecondMoment_E_z_z_T,
                                                     "During E: node.paramData.invSecondMoment_E_z_z_T")


            if node.parent is not None: #  interior node: hidden node but non-root; e.g. z_2
                ## Sets E[zⱼzi | x] for j = parent of i
                # equation 16: E[zⱼzi | x] =  Σ_{zⱼzᵢ|x} + E[Zⱼ| x] E[Zᵢ| x]^T ∈ ℝ^ (dimension of parent hidden node × dimension of this hidden node)
                # ⇒ overall: ( N *  Σ_{zⱼzᵢ|x} ) + E[Zⱼ| x] E[Zᵢ| x]^T
                node.paramData.sumParentSelfCrossExpectation_E_z_z_T = self.N * self.conditionalHiddenCovGivenObs_Sig_z_given_x[node.parent.paramData.jointPrecisionIndices[0]:node.parent.paramData.jointPrecisionIndices[1],selfStart:selfEnd ] \
                                                             + np.dot(node.parent.paramData.data.T , node.paramData.data )
                                                             
                                                             
            for child in node.children: 
                DFSSetConditionalHiddenDistribution(child)
        DFSSetConditionalHiddenDistribution(self.dataTree)

    @staticmethod
    def possibly_diag_dot(left_matrix, right_matrix):
        """
        Do a possibly diagonal dot of left and right matrix, as appropriate
        :param left_matrix:
        :param right_matrix:
        :return:
        """
        if left_matrix.ndim > 1  and right_matrix.ndim > 1: # regular dot
            return np.dot(left_matrix, right_matrix)

        elif left_matrix.ndim == 1: # left is diagonal
            return left_matrix[:, np.newaxis] * right_matrix
        elif right_matrix.ndim == 1: # right is diagonal
            return left_matrix * right_matrix
        else:
            raise ValueError("can't dot")

    def _createJointPrecision(self):
        '''
        Creates joint precision matrix as a function of Θ = {W_i, Ψ_i}
        Gets  (Λ_{zz}, Λ_{zx} blocks; Λ_{xx} is just diag(Ψ₁⁻¹, Ψ₂⁻¹, ...)
        '''

        # Initialize empty matrices        
        (hiddenDataList, observedDataList) = self._getNodesInOrder()
        fullHiddenDim = sum([dat.paramData.dimension for dat in hiddenDataList])        
        fullObservedDim = sum([dat.paramData.dimension for dat in observedDataList])        

        self.hiddenHiddenJointPrec_Lambda_zz = np.zeros((fullHiddenDim, fullHiddenDim)) #  Λ_{zz}
        self.hiddenObservedJointPrec_Lambda_zx = np.zeros((fullHiddenDim, fullObservedDim)) #  Λ_{zx}


        def DFSCreatePrecision(node):
            '''
            Recursive DFSSetPrecision
            '''            
            if node == None:
                return
            
            if node.children == []:  # do nothing if observed
                return            
                        
            # add Ψᵢ⁻¹ to this

            (node_jnt_prec_start_idx, node_jnt_prec_end_idx) = \
                (node.paramData.jointPrecisionIndices[0], node.paramData.jointPrecisionIndices[1])
            try:
                if node.paramData.diagonalConditionalVariance:
                    psi_inv = np.diagflat(node.paramData.Psi ** -1.0) # changed
                else:
                    psi_inv = np.linalg.inv(node.paramData.Psi) # So maybe this is NaN

                ParameterizedData.check_for_missing_data(psi_inv, "psi_inv")
                self.hiddenHiddenJointPrec_Lambda_zz[node_jnt_prec_start_idx:node_jnt_prec_end_idx, node_jnt_prec_start_idx:node_jnt_prec_end_idx] += psi_inv
            except AttributeError as e:
                raise ValueError("{0} \n\nError -- parameters not initialized "
                                 "before inference; please initialize parameters.".format(e))

            for child in node.children:

                if child.paramData.diagonalConditionalVariance:
                    child_psi_inv = child.paramData.Psi ** -1.0 # changed
                else:
                    try:
                        child_psi_inv = np.linalg.inv(child.paramData.Psi)
                    except np.linalg.LinAlgError:
                        logging.warning("Node {0}: Could not take inverse!!!!".format(child))

                Models.ParameterizedData.check_for_missing_data(child_psi_inv, "Node {0}: child_psi_inv".format(child))

                # Add W_c^T Ψ_c⁻¹ W_c to this
                self.hiddenHiddenJointPrec_Lambda_zz[node_jnt_prec_start_idx:node_jnt_prec_end_idx, node_jnt_prec_start_idx:node_jnt_prec_end_idx] += \
                    np.dot(child.paramData.W.T, GaussianLatentFactorModel.possibly_diag_dot(child_psi_inv, child.paramData.W))

                ParameterizedData.check_for_missing_data(np.dot(child.paramData.W.T, GaussianLatentFactorModel.possibly_diag_dot(child_psi_inv, child.paramData.W)),
                                                         "(np.dot(child.paramData.W.T, GaussianLatentFactorModel.possibly_diag_dot(child_psi_inv, child.paramData.W))")


                if child.children == []:
                    # child is observed
                    jointPrec = self.hiddenObservedJointPrec_Lambda_zx
                else:
                    # child is also hidden (I'm seeing... something that was always hidden)
                    jointPrec = self.hiddenHiddenJointPrec_Lambda_zz



                # Add -W_c^T Ψ_c⁻¹
                (childStart, childEnd) = (child.paramData.jointPrecisionIndices[0], child.paramData.jointPrecisionIndices[1])                
                jointPrec[node_jnt_prec_start_idx:node_jnt_prec_end_idx, childStart:childEnd] += \
                    -GaussianLatentFactorModel.possibly_diag_dot(child.paramData.W.T, child_psi_inv )

                # ParameterizedData.check_for_missing_data(-GaussianLatentFactorModel.possibly_diag_dot(child.paramData.W.T, child_psi_inv ),
                #                                          "-GaussianLatentFactorModel.possibly_diag_dot(child.paramData.W.T, child_psi_inv )")

                
                # Add the other part of this: - Ψ_c⁻¹ W_c
                if child.children != []:
                    jointPrec[childStart:childEnd, node_jnt_prec_start_idx:node_jnt_prec_end_idx] += \
                        -GaussianLatentFactorModel.possibly_diag_dot(child_psi_inv, child.paramData.W)

            # Check for NaNs
            # ParameterizedData.check_for_missing_data(self.hiddenHiddenJointPrec_Lambda_zz,
            #                             name=str(node )+ "self.hiddenHiddenJointPrec_Lambda_zz")
            #
            '''
            Traverse children
            '''
            for child in node.children:
                DFSCreatePrecision(child)

        DFSCreatePrecision(self.dataTree)

        # check for singularity
        # if np.linalg.matrix_rank(self.hiddenHiddenJointPrec_Lambda_zz) < self.hiddenHiddenJointPrec_Lambda_zz.shape[0]:
        #     logging.warning("Λ_zz is rank deficient. Dimension {0}, rank {1}. "
        #                         "Full Λ_zz:\n{2}\nReplacing with identity."
        #                     .format(self.hiddenHiddenJointPrec_Lambda_zz.shape[0],
        #                             np.linalg.matrix_rank(self.hiddenHiddenJointPrec_Lambda_zz),
        #                             self.hiddenHiddenJointPrec_Lambda_zz))

    ''' Prediction'''
    def predict(self, X, binarize = True):
        '''
        Predict one observed variable from the rest, under the
        learned set of parameters
        '''
        try:
            self.getYNode().paramData.mu_hat
        except AttributeError:
            raise sklearn.exceptions.NotFittedError("Model has not been fit; cannot predict.")

        assert X.shape[1] == self.feature_dim, "X has the wrong number of columns; should be {0}, is {1}".\
            format(self.feature_dim, X.shape[1])

        # Give the model for predicting the input data
        X_train_centered = self.predictModel._center_and_set_means(X)
        self.predictModel.N = X_train_centered.shape[0]

        self.predictModel.setObservedData(X_train_centered)
        self.predictModel.initializeParameters()

        # Set the subordinate model with the correct parameters \hat{Θ}= {\hat{W}, \hat{Ψ}} (learned during fit(...))
        self.predictModel.setThetaList(self.getThetaList()[:-1])

        # Run E step on subordinate model to find E_\hat{Θ}} [ latent node | observed independent features]
        self.predictModel._E_step(X)



        W_y_dot_z_1 = self.getYNode().paramData.W.dot(self.predictModel.dataTree.paramData.data.T)

        # add fit mean (μ_y )to every prediction
        y_hat = W_y_dot_z_1 + self.getYNode().paramData.mu_hat[:, None]

        if(binarize):
            y_hat = pd.DataFrame(y_hat).apply(lambda x: x > 0.5, axis=1)


        # Reshape to be a column vector if appropriate
        if y_hat.shape[0] == 1:
            y_hat = np.array(y_hat)[0, :]
        return (y_hat)

    def decision_function(self, X):
        return self.predict(X, binarize=False)

    ''' I/O'''
    def getThetaList(self):
        '''
        Returns Θ = [(W₁, μ₁, Ψ₁), (W₂, μ₂, Ψ₂), ...]
        '''
        
        def DFSGetTheta(node):
            if node is None:
                return []
            if node.parent is None:
                return DFSGetTheta(node.left) + DFSGetTheta(node.right)
            else:
                return [(node.paramData.W, np.zeros((node.paramData.dimension, 1)), node.paramData.Psi)] + DFSGetTheta(node.left) + DFSGetTheta(node.right)
            
        
        return DFSGetTheta(self.dataTree)

    def setThetaList(self, theta_list):
        '''
        Sets theta_list = [(W₁, μ₁, Ψ₁), (W₂, μ₂, Ψ₂), ...]

        :param theta_list:
        :return:
        '''

        def DFSSetTheta(node, theta_list):
            if node is None:
                return
            if node.parent is None:
                DFSSetTheta(node.left, theta_list)
                DFSSetTheta(node.right, theta_list)
            else:
                theta_this_node = theta_list.pop(0)
                node.paramData.W = theta_this_node[0]
                node.paramData.Psi = theta_this_node[2]
                DFSSetTheta(node.left, theta_list)
                DFSSetTheta(node.right, theta_list)


        DFSSetTheta(self.dataTree, theta_list)


    def getExtremeParamValue(self, fn):
        '''
        Gets smallest parameter (W, Ψ) value over all nodes 
        '''
        def DFSXt(node, fn):
            if node is None:
                return                    
            if node.children == []:
                return fn([fn(node.paramData.W), fn(node.paramData.Psi)])
            if node.parent is not None:
                return fn([DFSXt(node.left, fn), DFSXt(node.right, fn), fn(node.paramData.W), fn(node.paramData.Psi)])
            else: 
                return fn([DFSXt(node.left, fn), DFSXt(node.right, fn)])
            
        return DFSXt(self.dataTree, fn)      
    
    def writeThetaListToDisk(self, dirName):
        '''
        Writes the parameters to a folder
        '''
        def writeTheta(node, nHidden):
            if node is None:
                return []
            if node.parent is None:
                [writeTheta(child, nHidden) for child in node.children]
            else:
                nodeIndex =  str(node).split()[1].split("_")[1] if node.children == [] else  str(int(str(node).split()[1].split("_")[1]) - nHidden)
                pd.DataFrame(node.paramData.W).to_csv(dirName + os.sep + "W_" + nodeIndex + ".csv")                
                pd.DataFrame(node.paramData.Psi).to_csv(dirName + os.sep + "Psi_" + nodeIndex + ".csv")
                [writeTheta(child, nHidden) for child in node.children]
            
        
        return writeTheta(self.dataTree, len(self._getHidden()))

    def save_data(self, directory="."):
        """
        Save all the contents of the model to pandas feathers
        :return:
        """

        if not os.path.exists(directory):
            os.makedirs(directory)

        (hidden, observed) = self._getNodesInOrder()

        for node in hidden + observed:
            node_name =  str(node).split(":")[1].split(" ")[1]
            feather.write_dataframe(pd.DataFrame(node.paramData.Psi).copy(),
                                    os.path.join(directory, node_name + ".Psi.feather"))

            try:
                feather.write_dataframe(pd.DataFrame(node.paramData.W).copy(),
                                        os.path.join(directory, node_name + ".W.feather"))
            except AttributeError:
                pass

            feather.write_dataframe(pd.DataFrame(node.paramData.data).copy(),
                                    os.path.join(directory, node_name + ".data.feather"))


class ThreeNodeGaussianModel(GaussianLatentFactorModel):
    """
    Implements a latent variable model with three observed nodes and two hidden nodes;
    can perform fit of parameters with fit method. Graphical layout: 
          Z_1
          /   \
        Z_2    \ 
        / \     \
    X_1   X_2    X_3
    
    Dimensions: Z_1: m x 1
                Z_2: p_0 x 1
                X_1: p_1 x 1
                X_2: p_2 x 1
                X_3: p_3 x 1  (=y)

    """

    def __init__(self, m, p_0, p_1, p_2, p_3, name=""):
        """
        Constructor: sets dimension and creates observed and latent paramData tree.
        """

        self.name = name
        self._create_data_tree(m, p_0, p_1, p_2, p_3)

        # set observed dimension
        self.observed_dim = p_1 + p_2 + p_3
        self.feature_dim = p_1 + p_2
        self.response_dim = p_3

        super(ThreeNodeGaussianModel, self).__init__(dataTree=self.dataTree, observed_dim=self.observed_dim,
                                                     feature_dim=self.feature_dim, response_dim=self.response_dim)

        self.predictModel = TwoNodeGaussianModel(m, p_0, p_1, p_2, name="Predict_Model")

        self.dimensions = (m, p_0, p_1, p_2, p_3)

    def _create_data_tree(self, m, p_0, p_1, p_2, p_3):
        self.dataTree = Tree(GaussianData(m, None, var_name="z_1", diagonalConditionalVariance=True))  # z_1: root
        self.dataTree.insertLeft(GaussianData(p_0, m, var_name="z_2"))  # z_2
        self.dataTree.insertRight(GaussianData(p_3, m, var_name="x_3", diagonalConditionalVariance=True))  # x_3 = y
        self.dataTree.left.insertLeft(GaussianData(p_1, p_0, var_name="x_1", diagonalConditionalVariance=True))  # x_1
        self.dataTree.left.insertRight(GaussianData(p_2, p_0, var_name="x_2", diagonalConditionalVariance=True))  # x_2


    def getYNode(self):
        """
        :return: Node corresponding to Y
        """
        return self.dataTree.right


    def outputSamples(self, output=sys.stdout):
        '''
        Outputs sample of X_1, X_2, X_3        
        '''
        
        (X_1, X_2, X_3) = self._getObserved()
        (Z_1, Z_2) = self._getHidden()
        # write header
        for varName in (Z_1, Z_2, X_1, X_2, X_3):
            output.write("\t".join(varName.data.columns.values))
            output.write("\t")
        output.write("\n")
        
        for rowIdx in range(X_1.data.shape[0]):  # write samples row by row
            for varName in (Z_1, Z_2, X_1, X_2, X_3):
                np.savetxt(fname=output, X=varName.data.values[rowIdx], fmt="%.3f", newline="\t")
            output.write("\n")


class OneNodeGaussianModel(GaussianLatentFactorModel):
    '''
    Implements a latent variable model with one observed and one hidden node, with diagonal conditional variance
     equivalent to factor analysis
    
          Z_1
           |
           |
          X_1
    
    Dimensions: Z_1: m x 1
                X_1: p_1 x 1
    '''


    def __init__(self, m, p_1, name =""):
        '''
        Constructor: sets dimension and creates observed and latent paramData tree.
        '''
        self.name = name
        self._createDataTree(m, p_1)

        self.observed_dim = p_1
        self.feature_dim = p_1
        self.response_dim = 0

        super(OneNodeGaussianModel, self).__init__(dataTree=self.dataTree, observed_dim=self.observed_dim,
                                                     feature_dim = self.feature_dim, response_dim = self.response_dim)

    def _createDataTree(self, m,p1):
        self.dataTree = Tree(GaussianData(m, None, var_name="z_1", diagonalConditionalVariance=True))  # z_1: root
        self.dataTree.insertLeft(GaussianData(p1, m, var_name="x_1", diagonalConditionalVariance=True))  # x_1


class OneNodeNonDiagonalErrorGaussianModel(GaussianLatentFactorModel):
    '''     
    Implements a latent variable model with one observed and one hidden node, withOUT diagonal conditional variance
    Would be equivalent to factor analysis if the observed node had diagonal conditional variance.     
    
          Z_1
           |
           |
          X_1
    
    Dimensions: Z_1: m x 1
                X_1: p_1 x 1
    '''
    
    def __init__(self, m, p_1, name = ""):
        '''
        Constructor: sets dimension and creates observed and latent paramData tree.
        '''
        self.name = name
        self._createDataTree(m, p_1)
        super(OneNodeNonDiagonalErrorGaussianModel, self).__init__()

        self.observed_dim = p_1
        self.feature_dim = p_1
        self.response_dim = 0
        super(OneNodeNonDiagonalErrorGaussianModel, self).__init__()

                
    def _createDataTree(self, m,p1):
        self.dataTree = Tree(GaussianData(m, None, var_name="z_1", diagonalConditionalVariance=True))  # z_1: root
        self.dataTree.insertLeft(GaussianData(p1, m, var_name="x_1", diagonalConditionalVariance=False))  # x_1


class TwoNodeGaussianModel(GaussianLatentFactorModel):
    '''
    Used for ThreeNodeGaussianModel.predict

    Implements a latent variable model with two observed nodes and two hidden nodes;
    can perform fit of parameters with fit method. Graphical layout:
          Z_1
          /
        Z_2
        / \
    X_1   X_2

    Dimensions: Z_1: m x 1
                Z_2: p_0 x 1
                X_1: p_1 x 1
                X_2: p_2 x 1
    '''

    def __init__(self, m, p_0, p_1, p_2, name=""):
        self.name = name
        self._createDataTree(m, p_0, p_1, p_2)

        # set observed dimension
        self.observed_dim = p_1 + p_2
        self.feature_dim = p_1 + p_2
        self.response_dim = 0

        super(TwoNodeGaussianModel, self).__init__(dataTree=self.dataTree, observed_dim=self.observed_dim,
                                                     feature_dim = self.feature_dim, response_dim = self.response_dim)

    def _createDataTree(self, m, p_0, p_1, p_2):
        self.dataTree = Tree(GaussianData(m, None, var_name="z_1", diagonalConditionalVariance=True))  # z_1: root
        self.dataTree.insertLeft(GaussianData(p_0, m, var_name="z_2"))  # z_2
        self.dataTree.left.insertLeft(GaussianData(p_1, p_0, var_name="x_1", diagonalConditionalVariance=True))  # x_1
        self.dataTree.left.insertRight(GaussianData(p_2, p_0, var_name="x_2", diagonalConditionalVariance=True))  # x_2