# -*- coding: utf-8 -*-
'''
Created on Apr 10, 2016

@author: ijoseph
'''

import copy

import scipy.linalg

import matplotlib.pyplot as plt
import numpy as np
import inspect

from sklearn.decomposition import PCA


class ModelComparer(object):
    '''
    Compares two models
    '''


    def __init__(self, genModel, recoverModel):
        '''
        Constructor: sets two models to be compared
        '''
        self.genModel = genModel
        self.recoverModel = recoverModel

    @staticmethod
    def make_matrix_heatmap(data, axis, title="", vmin = None, vmax = None):
        
        im = axis.matshow(data, cmap="bwr", vmin=vmin, vmax=vmax)
        if data.shape[0] * data.shape[1] <= 25:   # add text if 25 cells or smaller
            for y in range(data.shape[0]):
                for x in range(data.shape[1]):
                    # print ("(x = {0}, y={1}, data={2}".format(x,y, data))
                    axis.text(x , y , '%.1f' % data[y, x],
                             horizontalalignment='center',
                             verticalalignment='center')

        if inspect.ismodule(axis): # axis actually just matplotlib.pyplot module
            axis.title(title)
            axis.axis('off')
        else: # axis is an instance of matplotlib.axes._subplots.AxesSubplot
            axis.set_title(title)
            axis.set_axis_off()
        return im        
        
    def compareAll(self):
        '''
        Compares two instances of this model: latent spaces and parameter values
        
        '''
        self.compareLatent(showPlots = False)
        self.compareParameters(showPlots= False)                    
        
        plt.show()

    '''Latent variables'''

    def compareLatent(self, showPlots = True):
        '''
        Compares latent parameters  
        '''
        
        self.compareHeatmapPlotZ() # compare based on heatmaps
        self.compareScatterPlotZ(showPlots=False)
        if showPlots:
            plt.show()
        
    ''' Latent variables: Scatters'''
    @staticmethod
    def _assign_group(Z, number_of_groups):
        '''
        From S.M. assign a column for group color based on quadrant
        '''
        dz, number_of_samples = Z.shape
        group = np.empty((1, number_of_samples))
        if dz != 2:
            print("don't know how to assign group!")
            print ("Shape = {0}".format(Z.shape))
            return
        else:
            angle = 360 / number_of_groups
            for dn in range(number_of_samples):
                angle_p = np.arctan2(Z[1, dn], Z[0, dn]) * 180 / np.pi
                if angle_p < 0:
                    angle_p = 360 + angle_p
                for nangle in range(number_of_groups):
                    if nangle * angle < angle_p and angle_p <= (
                            nangle + 1) * angle:
                        group[:, dn] = nangle
        return group           

    def compareScatterPlotZ(self, showPlots = True):
        (genHiddenList, recoverHiddenList) = self.genModel._getHidden(), self.recoverModel._getHidden()
        fig, axes = plt.subplots(2, len(genHiddenList))
        
        if len(axes.shape) == 1:
            axesFixed = [[axes[0]], [axes[1]]]
        else:
            axesFixed = axes 
        
 
        
        for i, (genModelPiece, recoverModelPiece) in enumerate(zip(genHiddenList, recoverHiddenList)):
            colors = ModelComparer._assign_group(genModelPiece.data.T.as_matrix()[0:2,:], 4)[0]
            self._scatterPlotZ(fig, genModelPiece, axesFixed[0][i], colors)
            self._scatterPlotZ(fig, recoverModelPiece, axesFixed[1][i], colors)
            
        axesFixed[0][0].set_ylabel(self.genModel.name + " Generating Latent Space", size='large')
        axesFixed[1][0].set_ylabel(self.recoverModel.name + " Inferred Latent Space", size='large')
        fig.set_size_inches(5.25, 5.25)
        fig.suptitle("Scatter Plots of First Two Dimensions of Hidden Space")
        if showPlots:
            plt.show()
    
    def _scatterPlotZ(self, fig, piece, axis, colors =None):
        '''
        Scatters first two dimensions 
        '''

        assert piece.data.iloc[:,0].shape[0] != 0, "Model for latent space comparison appears to not have any data. "
             
        axis.scatter(piece.data.iloc[:,0], piece.data.iloc[:,1], c=colors, s= 100, alpha=0.8)
        axis.set_title(str(piece))
    
    ''' Latent variables: Heatmaps '''    

    def compareHeatmapPlotZ(self):
        '''
        Compare latent between two models by plotting heatmpas
        ''' 
        (firstHiddenList, secondHiddenList) = self.genModel._getHidden(), self.recoverModel._getHidden()
        fig, axes = plt.subplots(2, len(firstHiddenList))
        if len(axes.shape) == 1:
            axesFixed = [[axes[0]], [axes[1]]]
        else:
            axesFixed = axes 
            
            
        
        # Get minimum and maximum to make colors the same across all subplots
        minOverall = np.min([np.min(np.min(piece.data)) for piece in firstHiddenList] + [np.min(np.min(piece.data)) for piece in secondHiddenList])
        maxOverall = np.max([np.max(np.max(piece.data)) for piece in firstHiddenList] + [np.max(np.max(piece.data)) for piece in secondHiddenList])
        for i, (firstModelPiece, secondModelPiece) in enumerate(zip(firstHiddenList, secondHiddenList)):
            self._heatmapPlotZ(fig, firstModelPiece, axesFixed[0][i], (minOverall, maxOverall))
            im = self._heatmapPlotZ(fig, secondModelPiece, axesFixed[1][i], (minOverall, maxOverall))
        
        axesFixed[0][0].set_ylabel(self.genModel.name + " Generating Latent Space", size='large')
        axesFixed[1][0].set_ylabel(self.recoverModel.name + " Inferred Latent Space", size='large')
        
        fig.colorbar(im, ax=axes.ravel().tolist())
        fig.set_size_inches(9.25, 5.25)
    
    def _heatmapPlotZ(self, fig, piece, axis, limits):
        '''
        Plot hidden zᵢ
        '''
        try:
            im =  self.make_matrix_heatmap(piece.data.as_matrix(), axis, str(piece), vmin=limits[0], vmax = limits[1])
        except AttributeError: # huge kludge to get around data either being an ndarray or a pandas data.frame
            im = self.make_matrix_heatmap(piece.data, axis, str(piece), vmin=limits[0], vmax = limits[1])
        return im
    
    ''' Parameters '''
    
    def compareParameters(self, showPlots=True):
        self._compareHeatmapParams()
        if showPlots:
            plt.show()           
    
    
    def _rotateKabsch(self, P, Q):
        """
        Rotate matrix P unto matrix Q using Kabsch algorithm
        """
        U = self._kabsch(P, Q)
    
        # Rotate P
        P_rotated = np.dot(P, U)
        return P_rotated   
    
    def _kabsch(self, P, Q):
        """
        The optimal rotation matrix U is calculated and then used to _rotateKabsch matrix
        P unto matrix Q so the minimum root-mean-square deviation (RMSD) can be
        calculated.
        Using the Kabsch algorithm with two sets of paired point P and Q,
        centered around the center-of-mass.
        Each vector set is represented as an NxD matrix, where D is the
        the dimension of the space.
        The algorithm works in three steps:
        - a translation of P and Q
        - the computation of a covariance matrix C
        - computation of the optimal rotation matrix U
        http://en.wikipedia.org/wiki/Kabsch_algorithm
        Parameters:
        P -- (N, number of points)x(D, dimension) matrix
        Q -- (N, number of points)x(D, dimension) matrix
        Returns:
        U -- Rotation matrix
        """
    
        # Computation of the covariance matrix
        C = np.dot(np.transpose(P), Q)
    
        # Computation of the optimal rotation matrix
        # This can be done using singular value decomposition (SVD)
        # Getting the sign of the det(V)*(W) to decide
        # whether we need to correct our rotation matrix to ensure a
        # right-handed coordinate system.
        # And finally calculating the optimal rotation matrix U
        # see http://en.wikipedia.org/wiki/Kabsch_algorithm
        V, S, W = np.linalg.svd(C)
        d = (np.linalg.det(V) * np.linalg.det(W)) < 0.0
    
        if d:
            S[-1] = -S[-1]
            V[:, -1] = -V[:, -1]
    
        # Create Rotation matrix U
        U = np.dot(V, W)
    
        return U    
    
    def _rotateOrthogonalProcrustes(self, genMatrix, recoMatrix):
        (U, _) = scipy.linalg.orthogonal_procrustes(recoMatrix,genMatrix)
        P_rotated = np.dot(recoMatrix, U)
        return P_rotated
        
    
    def createRotationallySimilarRecovered(self, method):
        '''
        Creates a new model that's a copy of the recovered model, except with all parameters 
        rotated to be as close as possible to the generated model.
        '''
        
        # Create copy of recovered model for modification 
        rotationallySimilarRecoverModel = copy.deepcopy(self.recoverModel)
        
        def DFSMatchRotations(generatedNode, recoveredNode ):
            '''
            Traverse both models and modify recovered parameters 
            '''
            if generatedNode.parent is not None: # Not a root node, so has some parameters; rotate them
                recoveredNode.paramData.W = method(recoveredNode.paramData.W, generatedNode.paramData.W)                
                recoveredNode.paramData.Psi = method(recoveredNode.paramData.Psi, generatedNode.paramData.Psi)                
                
            
            if generatedNode is not None: # Do the same for children 
                for (generatedChild, recoveredChild) in zip (generatedNode.children, recoveredNode.children):
                    DFSMatchRotations(generatedChild, recoveredChild)
        
        DFSMatchRotations(self.genModel.dataTree, rotationallySimilarRecoverModel.dataTree)
        
        return rotationallySimilarRecoverModel
                
                    
    ''' PCA plot'''
    @staticmethod
    def compare_pca(X_gen, X_pred):
        (fig, axes)  = plt.subplots(1,2)

        colors = ModelComparer.plot_pca(X_gen, axes[0])
        # print ("colors is {0}".format(colors))
        ModelComparer.plot_pca(X_pred, axes[1], colors=colors)





    @staticmethod
    def plot_pca( X, axis=plt, colors=None):

        pca_obj = PCA(n_components=2)
        X_pca = pca_obj.fit_transform(X)

        if colors is None:
            colors = ModelComparer._assign_group(X_pca.T, 4)[0]

        axis.scatter(X_pca[:,0], X_pca[:,1], c=colors, alpha=0.8, s=100)


        return colors


        
    
    
    ''' Parameters: Heatmaps '''

    def _compareHeatmapParams(self):
        """
        Compare parameters between two models by plotting heatmaps        
        """

        rotationallySimilarRecoverModel = self.createRotationallySimilarRecovered(self._rotateOrthogonalProcrustes) # Create rotationally similar
        
        smallest = np.min([self.genModel.getExtremeParamValue(np.min), rotationallySimilarRecoverModel.getExtremeParamValue(np.min)])
        largest = np.max([self.genModel.getExtremeParamValue(np.max), rotationallySimilarRecoverModel.getExtremeParamValue(np.max)])     

        self.plot_model_parameters(self.genModel, self.genModel.name + "\nGenerating Model Parameters", smallest, largest)
        self.plot_model_parameters(rotationallySimilarRecoverModel, rotationallySimilarRecoverModel.name + "\nRotationally Maximized Inferred Model Parameters", smallest, largest)
        self.plot_model_parameters(self.recoverModel, self.recoverModel.name + "\nInferred Model Parameters")
        
    @staticmethod
    def plot_model_parameters(model, title="", vmin= None, vmax = None):
        '''
        Plots W, Ψ    
        '''
        
        allNodes = model._getNodesInOrder()
        (fig, axes ) = plt.subplots(2, len(allNodes[0] + allNodes[1]) - 1)
        
        if len(axes.shape) == 1:
            axesFixed = [[axes[0]], [axes[1]]]
        else:
            axesFixed = axes 
                    
        ctr = 0
        for node in allNodes[0] + allNodes[1]:
            if node.parent is None:
                continue
            nodeIndex =  str(node).split()[1].split("_")[1] if node.children == [] else  str(int(str(node).split()[1].split("_")[1]) - len(allNodes[0]))
            try:
                ModelComparer.make_matrix_heatmap(node.paramData.W, axesFixed[0][ctr], "$\mathbf{W}_" + nodeIndex + "$", vmin = vmin, vmax = vmax)
                ModelComparer.make_matrix_heatmap(node.paramData.Psi, axesFixed[1][ctr], "$\mathbf{\Psi}_" + nodeIndex + "$", vmin = vmin, vmax = vmax)
                ctr +=1
            except TypeError:
                raise ValueError("Cannot plot parameters of a model that has not been initialized.")
        plt.suptitle(title + "$\mathbf{W}, \mathbf{\Psi}$")
        fig.set_size_inches(9.25, 5.25)     
        
    




    


        
        
        