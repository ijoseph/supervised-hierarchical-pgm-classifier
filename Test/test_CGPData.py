from unittest import TestCase
import Data.Parsers
import Performance.classification.CrossValidation
import sklearn


class TestCGPData(TestCase):

    def setUp(self):
        """
        Create CCLE data with various
        :return:
        """
        self.cgp_data = Data.Parsers.CGPData(
            mutation=open("/Users/ijoseph/Code/Data/GradProjects/Cancer-Dimensionality-Reduction/"
                          "Cancer-Genome-Project/Mutation/CosmicCLP_MutantExport.tsv.gz.micro.tsv.gz"),
            expression=open("/Users/ijoseph/Code/Data/GradProjects/Cancer-Dimensionality-Reduction/"
                            "Cancer-Genome-Project/Gene-Expression/"
                            "CosmicCLP_CompleteGeneExpression.tsv.gz.micro.tsv.gz"),
            sensitivity=open("/Users/ijoseph/Code/Data/GradProjects/Cancer-Dimensionality-Reduction/"
                             "Cancer-Genome-Project/Compound-Sensitivity/binarized.tsv"))

    def test__get_two_platform_data_frame_collection(self):
        """
        Tests various NaN settings
        :return:
        """

        # Test missing data imputation
        merged = self.cgp_data.get_two_platform_data_frame_collection("mutation", "expression", "Erlotinib")

        # Should be no missing values
        self.assertFalse(merged.independentDF.isnull().values.any())
        self.assertFalse(merged.dependentDF.isnull().values.any())

        # Should be no zero-variance columns
        self.assertGreater(min(merged.independentDF.var()), 0)
        self.assertGreater(min(merged.dependentDF.var()), 0)

        self.assertEqual(merged.num_columns_of_low_variance(), 0)
        self.assertEqual(merged.num_missing_values(), 0)

        # Test that this can actually be run through CV
        cv = Performance.classification.CrossValidation.CrossValidation(sklearn.linear_model.LogisticRegression(),
                                                                        merged, 10)
        cv.cross_validate()
        cv.output_results()

