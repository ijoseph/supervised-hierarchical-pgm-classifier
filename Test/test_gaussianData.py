import unittest
import Models.LatentFactorModels
import Models.ParameterizedData
import Simulators.Simulator

import numpy as np
import pandas as pd
np.random.seed(seed=101)

class TestGaussianData(unittest.TestCase):

    def setUp(self):
        """

        :return:
        """

        (m, p_0, p_1, p_2, p_3) = (2, 3, 2, 2, 1)
        self.gen_model= Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
        _ = self.gen_model.initializeParameters(psiMean=2, wMean=20, warm_start=False)
        simulator = Simulators.Simulator.Simulator(self.gen_model)
        simulator.simulate(5)

        self.z_1 = self.gen_model.dataTree.paramData
        self.x_3 = self.gen_model.dataTree.right.paramData
        self.x_1 = self.gen_model.dataTree.left.left.paramData
        self.z_2 = self.gen_model.dataTree.left.paramData


    def test_create_warm_start_parameters(self):

        # 1 self dim with 2 parent dim
        self.x_3.create_warm_start_parameters(self.z_1)
        component_wise = self.x_3.W.dot(self.x_3.W.T) + self.x_3.Psi
        total = np.var(self.x_3.data)

        self.assertTrue(np.allclose(component_wise, total), "{0} vs. {1}".format(component_wise, total))

        # 2 self dim with 3 parent dim

        self.x_1.create_warm_start_parameters(self.z_2)

        component_wise = self.x_1.W.dot(self.x_1.W.T) + self.x_1.Psi
        total = np.cov(self.x_1.data.T)
        self.assertTrue(np.allclose(component_wise, total), "{0} vs. {1}".format(component_wise, total))

    def test_diag_sim(self, n=4):

        small_model = Models.LatentFactorModels.OneNodeGaussianModel(m=2, p_1 = 3, name ="test")
        small_model.initializeParameters(warm_start=False)

        small_model_sim = Simulators.Simulator.Simulator(model=small_model)
        # Auto simulate
        small_model_sim.simulate(n)

        # Simulate by doing manual gaussian stuff
        x_node = small_model.dataTree.left.paramData
        z_node = small_model.dataTree

        manual = pd.DataFrame(index=range(n), columns=range(x_node.dimension), dtype=float)
        for i in range(n):
            manual.iloc[i] = np.random.multivariate_normal(mean=np.dot(x_node.W, z_node.paramData.data.iloc[i]),
                                                           cov=x_node.Psi)

        # s/t like 5% chance of false failure
        self.assertTrue(np.allclose(a=x_node.data, b=manual, atol=np.max(x_node.Psi) * 2))

        # import seaborn as sns, matplotlib.pyplot as plt
        # ax = sns.heatmap(x_node.data)
        # ax.set_title("Auto sim")
        # plt.figure(2)
        # ax = sns.heatmap(manual)
        # ax.set_title("Manual")
        # plt.show()








