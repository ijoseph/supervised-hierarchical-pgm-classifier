# -*- coding: utf-8 -*- 
'''
Created on Apr 19, 2016

@author: ijoseph
'''
import unittest

import numpy as np
import pandas as pd
from sklearn.decomposition import FactorAnalysis

from Models.LatentFactorModels import OneNodeGaussianModel
from Models.ModelComparer import ModelComparer
from Simulators.Simulator import Simulator
from Test.shannon_code.survival_funct_v1 import EM_all


class Test(unittest.TestCase):
    
    ''' Setup '''
    def setUp(self):
        '''
        Set up: run Shannon's factor analysis code, run my factor analysis code 
        '''
        np.random.seed(seed=50)
        nPoints = 1000
        
        self._setUpMine(nPoints) # Sets self.myTheta, self.genTheta
        self._setUpShannon() # Sets self.shannonTheta
        self._setUpSklearn() # Sets up sklearn
        
        def is_interactive():
            '''
            Check if plots should be shown
            '''
            import __main__ as main
            return not hasattr(main, '__file__')
        
        if is_interactive():
            self.showPlots = True
        else:
            self.showPlots=False
        
    def _setUpShannon(self):
        '''        
        Inference from simulated from mine
        '''
        # [(X, (datatype,  θ, sparse))] 
        dataparamX = [(self.genModel.getObservedData().as_matrix().T,('normal', (self.WInit, np.zeros(self.p_0), np.reshape(np.diag(self.PsiInit), (self.p_0,1)) ), [False]))]
         
        (dataparamX0, _, _, _, _, EZ) = EM_all(dataparamX, False, 2, 10,  0.0001, None, None, False, False,  '')
        
        self.shannonTheta = [dataparamX0[0][1][1]] #[( W₁, μ₁, Ψ₁)]
        self.shannonTheta = [((self.shannonTheta[0][0], np.reshape(self.shannonTheta[0][1], (self.p_0,1)), np.diagflat(self.shannonTheta[0][2])))]
        
        # Inject results into a fake model for easy comparison
        self.shannonFakeModel = OneNodeGaussianModel(self.m,self.p_0, name = "Shannon's Model")
        self.shannonFakeModel.dataTree.left.paramData.set_data(self.genModel.getObservedData().as_matrix())  # set X
        self.shannonFakeModel.dataTree.paramData.set_data(EZ.T) # set z
        self.shannonFakeModel.dataTree.left.paramData.Psi= self.shannonTheta[0][2] # set Ψ
        self.shannonFakeModel.dataTree.left.paramData.W= self.shannonTheta[0][0] # set W
        
    def _setUpMine(self, nPoints):
        '''
        Generate, simulate, inference.
        '''
        (self.m, self.p_0) = (2,5)
        self.genModel = OneNodeGaussianModel(self.m,self.p_0, name = "Generating")
        self.genModel.initializeParameters(psiMean = 2, wMean=2) # initialize parameters
        
        #simulate
        simulator = Simulator(self.genModel)
        simulator.simulate(nPoints)
        
        self.fitModelMine = OneNodeGaussianModel(self.m, self.p_0, name = "My model")
        [(_, _), (self.WInit, self.PsiInit)] = self.fitModelMine.initializeParameters()
        self.fitModelMine.fit(self.genModel.getObservedData(), num_EM_rounds= 10)
        # Get   θ = [#[( W₁, μ₁, Ψ₁)]
        self.genTheta = self.genModel.getThetaList()
        self.myTheta = self.fitModelMine.getThetaList()
        
    def _setUpSklearn(self):            
        fa = FactorAnalysis(n_components=2)
        faFit = fa.fit(self.genModel.getObservedData())
        latent= faFit.transform(self.genModel.getObservedData())

        self.skLearnFakeModel = OneNodeGaussianModel(self.m,self.p_0, name = "SkLearn")
        self.skLearnFakeModel.dataTree.left.paramData.set_data(self.genModel.getObservedData().as_matrix())  # set X
        self.skLearnFakeModel.dataTree.paramData.set_data(latent) # set z
        self.skLearnFakeModel.dataTree.left.paramData.Psi= np.diagflat(faFit.noise_variance_) #set Ψ
        self.skLearnFakeModel.dataTree.left.paramData.W= faFit.components_.T # set W
        
    def tearDown(self):
        pass

    ''' Tests '''
         
    def testTwoNodeModelParams(self): 

        
               
        ModelComparer(self.genModel, self.fitModelMine).compareParameters(showPlots=self.showPlots) # compare my recovered to generated
        ModelComparer(self.genModel, self.shannonFakeModel).compareParameters(showPlots=self.showPlots) # compare shannon's recovered to generated
        ModelComparer(self.genModel, self.skLearnFakeModel).compareParameters(showPlots=self.showPlots) # compare sklearn's recovered to generated
        
        # Actually ensure my learned parameters are the same as Shannon's
        for (shanParameter, myParam) in zip(self.shannonTheta[0], self.myTheta[0]):
            assert np.allclose(shanParameter, myParam), "My and Shannon's learned parameters appear to differ."
        
    def testTwoNodeModelHidden(self):

            
        ModelComparer(self.genModel, self.fitModelMine).compareScatterPlotZ(showPlots=self.showPlots) # compare my recovered to generated
        ModelComparer(self.genModel, self.shannonFakeModel).compareScatterPlotZ(showPlots=self.showPlots) # compare shannon's recovered to generated
        ModelComparer(self.genModel, self.skLearnFakeModel).compareScatterPlotZ(showPlots=self.showPlots) # compare sklearn's recovered to generated
        
        assert np.allclose(self.shannonFakeModel.dataTree.paramData.data, self.fitModelMine.dataTree.paramData.data), \
        "My and Shannon's learned latent space appear to differ \n {0} \n \n {1} ; ".format(pd.DataFrame.head(self.shannonFakeModel.dataTree.paramData.data), pd.DataFrame.head(self.fitModelMine.dataTree.paramData.data))
        
        


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testTwoNodeModel']
    unittest.main()