from unittest import TestCase
import Data.Parsers


class TestCGPDataFromSupplement(TestCase):

    def setUp(self):
        self.cgp_fs = Data.Parsers.CGPDataFromSupplement(sensitivity=
                                             open("/Users/ijoseph/Code/Data/GradProjects/Cancer-Dimensionality-Reduction/"
                                             "Cancer-Genome-Project/Compound-Sensitivity/binarized.tsv"),
                                                         mutations="/Users/ijoseph/Code/Data/GradProjects/"
                                                       "Cancer-Dimensionality-Reduction/Cancer-Genome-Project/"
                                                       "Paper-Supp/mutations.csv",
                                                         cnv="/Users/ijoseph/Code/Data/GradProjects/Cancer-Dimensionality-Reduction"
                                                 "/Cancer-Genome-Project/Paper-Supp/cnv.csv")

    def test_impute(self):
        """
        :return:
        """

        dfc = self.cgp_fs.get_two_platform_data_frame_collection("mutation", "cnv", compound_name="Sunitinib",
                                                           impute_type="mean_column")

        self.assertLess(dfc.indIndices[0], dfc.independentDF.shape[1])







