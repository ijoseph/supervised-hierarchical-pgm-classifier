# -*- coding: utf-8 -*-
'''
Created on May 2, 2016

@author: ijoseph
'''
import unittest

from Models.LatentFactorModels import ThreeNodeGaussianModel
from Models.ModelComparer import ModelComparer
from Simulators.Simulator import Simulator
import numpy as np
import pandas as pd
import matplotlib.pyplot

class Test(unittest.TestCase):


    def setUp(self):
        
        self.N = float(100)      
        
        np.random.seed(seed=101)        
        
        
        def is_interactive():
            '''
            Check if plots should be shown
            '''
            import __main__ as main
            return not hasattr(main, '__file__')
        
        if is_interactive():
            self.showPlots = True
        else:
            self.showPlots=False
        
        

        
        (self.m, self.p_0, self.p_1, self.p_2, self.p_3) = (2,5,7,7,7)
        self.genModel = ThreeNodeGaussianModel(self.m, self.p_0, self.p_1, self.p_2, self.p_3)
        self.genModel.initializeParameters(psiMean = 2, wMean=2, warm_start= False)
        
        simulator = Simulator(self.genModel)
        simulator.simulate(self.N)
        
        
        self.fitModel = ThreeNodeGaussianModel(self.m, self.p_0, self.p_1, self.p_2, self.p_3)
        self.fitModel.initializeParameters(warm_start=False)
        
        
        [(self.W_0_init, _ , self.Psi_0_init), (self.W_1_init, _ , self.Psi_1_init),\
         (self.W_2_init, _ , self.Psi_2_init), (self.W_3_init, _, self.Psi_3_init)] = self.fitModel.getThetaList()

        # Save generating X and y

        self.generatingX =self.genModel.getObservedData().iloc[:, 0:self.genModel.feature_dim]

        self.generatingY = self.genModel.getObservedData().iloc[
                                 :, self.genModel.feature_dim:(self.genModel.response_dim+ self.genModel.feature_dim)]

    def testShowPlots(self):
        ModelComparer(self.genModel, self.fitModel).compareParameters(showPlots =self.showPlots)
        
    def testJointPrecisionCreation(self):
        '''
        Tests the creation of initial Λ_zz, Λ_zx based on parameters
        '''
        # Tell model to make joint precision
        self.fitModel._createJointPrecision()
        
        # Λ_zz
        # Λ_z₁_z₁
        # Artificially created Λ_z_1_z_1: W_3^T Ψ_0⁻¹W_3 + W_0^T Ψ_0⁻¹W_0 + I
        manualHiddenHiddenJointPrec_Lambda_z_1_z_1 = self.W_3_init.T.dot( np.linalg.inv(self.Psi_3_init)).dot(self.W_3_init)  + self.W_0_init.T.dot(np.linalg.inv(self.Psi_0_init)).dot(self.W_0_init) + np.identity(self.m)
        
        assert np.allclose( self.fitModel.hiddenHiddenJointPrec_Lambda_zz[0:self.m, 0:self.m], manualHiddenHiddenJointPrec_Lambda_z_1_z_1), "Λ_z_1_z_1 doesn't match\n manual \n {0} \n auto \n {1}".format(pd.DataFrame(self.genModel.hiddenHiddenJointPrec_Lambda_zz[0:self.m, 0:self.m]), pd.DataFrame(manualHiddenHiddenJointPrec_Lambda_z_1_z_1)) 
        
        # Λ_z₁_z₂
        # Artificially created Λ_z_1_z_2: -W_0^T Ψ_0⁻¹
        manualHiddenHiddenJointPrec_Lambda_z_1_z_2 = - self.W_0_init.T.dot(np.linalg.inv(self.Psi_0_init))
        assert np.allclose(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[0:self.m, self.m:(self.m+self.p_0)], manualHiddenHiddenJointPrec_Lambda_z_1_z_2 ), "Λ_z_1_z_2 doesn't match \n manual \n {0} \n auto \n {1}".format(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[0:self.m, self.m:(self.m+self.p_0)], manualHiddenHiddenJointPrec_Lambda_z_1_z_2 )
        # Λ_z₂_z₁
        assert np.allclose(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[self.m:(self.m+self.p_0), 0:self.m], manualHiddenHiddenJointPrec_Lambda_z_1_z_2 .T), "Λ_z_2_z_1 doesn't match \n manual \n {0} \n auto \n {1}".format(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[self.m:(self.m+self.p_0), 0:self.m], manualHiddenHiddenJointPrec_Lambda_z_1_z_2 .T)
        
        # Λ_z₂_z₂
        # Artificially created Λ_z_1_z_1: W_1^T Ψ_1⁻¹W_1 + W_2^T Ψ_2⁻¹W_2 + Ψ_0⁻¹
        manualHiddenHiddenJointPrec_Lambda_z_2_z_2 = self.W_1_init.T.dot(np.linalg.inv(self.Psi_1_init)).dot(self.W_1_init) + self.W_2_init.T.dot(np.linalg.inv(self.Psi_2_init)).dot(self.W_2_init) + np.linalg.inv(self.Psi_0_init) 
        assert np.allclose(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[self.m:(self.m+self.p_0), self.m:(self.m+self.p_0)], manualHiddenHiddenJointPrec_Lambda_z_2_z_2 ), "Λ_z_2_z_2 doesn't match \n manual \n {0} \n auto \n {1}".format(self.fitModel.hiddenHiddenJointPrec_Lambda_zz[self.m:(self.m+self.p_0), self.m:(self.m+self.p_0)], manualHiddenHiddenJointPrec_Lambda_z_2_z_2 )
        
        # # Λ_zx
        # Λ_z₁x₁
        assert np.allclose(self.fitModel.hiddenObservedJointPrec_Lambda_zx[0:self.m, 0:self.p_1], np.zeros((self.m, self.p_1))) # all zero
        # Λ_z₁x₂
        assert np.allclose(self.fitModel.hiddenObservedJointPrec_Lambda_zx[0:self.m, self.p_1:(self.p_1+ self.p_2)], np.zeros((self.m, self.p_2))) # all zero
        # Λ_z₁x₃: - W₃^TΨ₃⁻¹
        manualHiddenObservedJointPrec_Lambda_z_1_x_3 = - self.W_3_init.T.dot(np.linalg.inv(self.Psi_3_init))
        assert np.allclose(manualHiddenObservedJointPrec_Lambda_z_1_x_3, self.fitModel.hiddenObservedJointPrec_Lambda_zx[0:self.m, (self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)])
        
        # Λ_z₂x₁
        manualHiddenObservedJointPrec_Lambda_z_2_x_1 = - self.W_1_init.T.dot(np.linalg.inv(self.Psi_1_init))
        assert np.allclose(manualHiddenObservedJointPrec_Lambda_z_2_x_1, self.fitModel.hiddenObservedJointPrec_Lambda_zx[self.m:(self.m+self.p_0), 0:self.p_1])
        
        # Λ_z₂x₂
        manualHiddenObservedJointPrec_Lambda_z_2_x_2 = - self.W_2_init.T.dot(np.linalg.inv(self.Psi_2_init))
        assert np.allclose(manualHiddenObservedJointPrec_Lambda_z_2_x_2, self.fitModel.hiddenObservedJointPrec_Lambda_zx[self.m:(self.m+self.p_0),self.p_1:(self.p_1+self.p_2)])

        
        # Λ_z₂x₃
        assert np.allclose(np.zeros((self.p_0, self.p_3)), self.fitModel.hiddenObservedJointPrec_Lambda_zx[self.m:(self.m+self.p_0), (self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)])
        
    def testEStep(self):
        '''
        Tests whether E step was done according to manual manipulations         
        '''
        self.fitModel._createJointPrecision()
        
        # Manually get results
        # μ_{z|x} = Λ_zz⁻¹ ⁻Λ_zx  X
        xDataFrame = self.genModel.getObservedData().sub(self.genModel.getObservedData().mean()) 
        manualConditionalHiddenExpGivenObs_mu_z_x =  \
        - np.linalg.inv(self.fitModel.hiddenHiddenJointPrec_Lambda_zz).dot(self.fitModel.hiddenObservedJointPrec_Lambda_zx).dot(xDataFrame.T)
        
        
    
        # Tell model to do E step
        fit_model = self.fitModel.fit(X=self.generatingX, y=self.generatingY, num_EM_rounds=1)
        
        # Test μ_{z|x}
        assert np.allclose(fit_model.conditionalHiddenExpGivenObs_mu_z_x, manualConditionalHiddenExpGivenObs_mu_z_x),\
         "μ_z|x doesn't match \n manual \n {0} \n auto \n {1}"\
         .format(pd.DataFrame(manualConditionalHiddenExpGivenObs_mu_z_x), \
                 pd.DataFrame(fit_model.conditionalHiddenExpGivenObs_mu_z_x))
         
         
        # Test setting of expected values to pieces
        # μ_{z₁|x}
        assert np.allclose(manualConditionalHiddenExpGivenObs_mu_z_x[0:self.m,:], fit_model.dataTree.paramData.data.T)
        # μ_{z₂|x}
        assert np.allclose(manualConditionalHiddenExpGivenObs_mu_z_x[self.m:(self.m+ self.p_0),:], fit_model.dataTree.left.paramData.data.T)
        
        
        # Test cross expectations
        # E[z₁z₁ᵀ] = Σ_z₁z₁|x + μ_z₁|x μ_z₁|xᵀ (eqn 14)
        # (sum over all samples E[z₁z₁ᵀ] )  = (N * Σ_z₁z₁|x) + (μ_Z₁|x μ_Z₁|xᵀ)
        manualSecondMoment_E_z_1_z_1_T =  (self.N * fit_model.conditionalHiddenCovGivenObs_Sig_z_given_x[0:self.m, 0:self.m] )+  manualConditionalHiddenExpGivenObs_mu_z_x[0:self.m,:].dot(manualConditionalHiddenExpGivenObs_mu_z_x[0:self.m,:].T)
        assert np.allclose(manualSecondMoment_E_z_1_z_1_T, fit_model.dataTree.paramData.sumSecondMoment_E_z_z_T)
        
        # E[z₂z₂ᵀ] = Σ_z₂z₂|x + μ_z₂|x μ_z₂|xᵀ (eqn 15)
        manualSecondMoment_E_z_2_z_2_T = (self.N * fit_model.conditionalHiddenCovGivenObs_Sig_z_given_x[self.m:(self.m+self.p_0), self.m:(self.m+self.p_0)]) + manualConditionalHiddenExpGivenObs_mu_z_x[self.m:(self.m+self.p_0), :].dot(manualConditionalHiddenExpGivenObs_mu_z_x[self.m:(self.m+self.p_0), :].T)
        assert np.allclose(manualSecondMoment_E_z_2_z_2_T, fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T)
        
        # E[z₁z₂ᵀ] = Σ_z₁z₂|x + μ_z₁|x μ_z₂|xᵀ (eqn 16)
        manualSecondMoment_E_z_1_z_2_T = (self.N * fit_model.conditionalHiddenCovGivenObs_Sig_z_given_x[0:self.m,self.m:(self.m+self.p_0)]) + manualConditionalHiddenExpGivenObs_mu_z_x[0:self.m,:].dot(manualConditionalHiddenExpGivenObs_mu_z_x[self.m:(self.m+self.p_0), :].T)
        assert np.allclose(manualSecondMoment_E_z_1_z_2_T, fit_model.dataTree.left.paramData.sumParentSelfCrossExpectation_E_z_z_T)

    def testMStep(self):
        '''Tests whether M step was done correctly,  according to manual manipulations'''
        
        # Tell model to do E, then M step

        # Tell model to do E step
        fit_model = self.fitModel.fit(X=self.generatingX, y=self.generatingY, num_EM_rounds=1)

        xDataFrame = self.genModel.getObservedData().sub(self.genModel.getObservedData().mean()) 
        
        ## W
        # \hat{W_0}_{MLE}
        manuallyUpdatedW_0 = np.linalg.inv(fit_model.dataTree.paramData.sumSecondMoment_E_z_z_T).dot(fit_model.dataTree.left.paramData.sumParentSelfCrossExpectation_E_z_z_T)
        assert np.allclose(manuallyUpdatedW_0.T, fit_model.dataTree.left.paramData.W)
        
        # \hat{W_1}_{MLE}
        manuallyUpdatedW_1 = np.linalg.inv(fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T).dot(fit_model.dataTree.left.paramData.data.T).dot(xDataFrame.iloc[:,0:self.p_1])
        assert np.allclose(manuallyUpdatedW_1.T, fit_model.dataTree.left.left.paramData.W)
        
        #\hat{W_2}_{MLE}
        manuallyUpdatedW_2 = np.linalg.inv(fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T).dot(fit_model.dataTree.left.paramData.data.T).dot(xDataFrame.iloc[:,self.p_1:(self.p_1+self.p_2)])
        assert np.allclose(manuallyUpdatedW_2.T, fit_model.dataTree.left.right.paramData.W)
        
        #\hat{W_3}_{MLE}
        manuallyUpdatedW_3 = np.linalg.inv(fit_model.dataTree.paramData.sumSecondMoment_E_z_z_T).dot(fit_model.dataTree.paramData.data.T).dot(xDataFrame.iloc[:,(self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)])
        assert np.allclose(manuallyUpdatedW_3.T, fit_model.dataTree.right.paramData.W)
        
        ## Ψ
        
        #\hat{Ψ₀}_{MLE}
        manuallyUpdatedPsi_0 = (1/self.N) * ( fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T  - \
                                              fit_model.dataTree.left.paramData.sumParentSelfCrossExpectation_E_z_z_T.T\
                                              .dot(np.linalg.inv(fit_model.dataTree.paramData.sumSecondMoment_E_z_z_T))\
                                              .dot(fit_model.dataTree.left.paramData.sumParentSelfCrossExpectation_E_z_z_T))
        
        assert np.allclose(manuallyUpdatedPsi_0, fit_model.dataTree.left.paramData.Psi)
        
        #\hat{Ψ₁}_{MLE}
        manuallyUpdatedPsi_1 = np.diagflat(np.diag((1/self.N) * (xDataFrame.iloc[:,0:self.p_1].T.dot(xDataFrame.iloc[:,0:self.p_1])    - \
                                             xDataFrame.iloc[:,0:self.p_1].T\
                                             .dot(fit_model.dataTree.left.paramData.data)\
                                             .dot(np.linalg.inv(fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T))\
                                             .dot(fit_model.dataTree.left.paramData.data.T)\
                                             .dot(xDataFrame.iloc[:,0:self.p_1]))))
        assert np.allclose(manuallyUpdatedPsi_1, fit_model.dataTree.left.left.paramData.Psi),  "Ψ₁ doesn't match \n manual \n {0} \n auto \n {1}".format(manuallyUpdatedPsi_1, fit_model.dataTree.left.left.paramData.Psi)
                                             
                                             
        #\hat{Ψ₂}_{MLE}
        manuallyUpdatedPsi_2 = np.diagflat(np.diag((1/self.N) * (xDataFrame.iloc[:,self.p_1:(self.p_1+self.p_2)].T.dot(xDataFrame.iloc[:,self.p_1:(self.p_1+self.p_2)])    - \
                                             xDataFrame.iloc[:,self.p_1:(self.p_1+self.p_2)].T\
                                             .dot(fit_model.dataTree.left.paramData.data)\
                                             .dot(np.linalg.inv(fit_model.dataTree.left.paramData.sumSecondMoment_E_z_z_T))\
                                             .dot(fit_model.dataTree.left.paramData.data.T)\
                                             .dot(xDataFrame.iloc[:,self.p_1:(self.p_1+self.p_2)]))))  
        
        assert np.allclose(manuallyUpdatedPsi_2, fit_model.dataTree.left.right.paramData.Psi), "Ψ₂ doesn't match \n manual \n {0} \n auto \n {1}".format(manuallyUpdatedPsi_2, fit_model.dataTree.left.right.paramData.Psi)
        
        #\hat{Ψ₃}_{MLE}
        manuallyUpdatedPsi_3 = np.diagflat(np.diag((1/self.N) * (xDataFrame.iloc[:,(self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)].T.dot(xDataFrame.iloc[:,(self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)])    - \
                                             xDataFrame.iloc[:,(self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)].T\
                                             .dot(fit_model.dataTree.paramData.data)\
                                             .dot(np.linalg.inv(fit_model.dataTree.paramData.sumSecondMoment_E_z_z_T))\
                                             .dot(fit_model.dataTree.paramData.data.T)\
                                             .dot(xDataFrame.iloc[:,(self.p_1+self.p_2):(self.p_1+self.p_2+self.p_3)]))))  
        
        assert np.allclose(manuallyUpdatedPsi_3, fit_model.dataTree.right.paramData.Psi)
        
    def testPredict(self):
        '''
        Test whether prediction works by comparing to direct analytic solution
        :return:
        '''

        ## Test running to completion
        # Test on same set of data that upon which it was generated (training accuracy)
        fit_model = self.fitModel.fit(X=self.generatingX, y=self.generatingY)
        fit_model.predict(X=self.generatingX)

        # Test on some random other data
        fit_model.predict(X=pd.DataFrame(np.random.rand(10, self.generatingX.shape[1])))

        # TODO (possibly): test equivalent to analytic solution

    def test_warmStart(self):
        warm_start_model = ThreeNodeGaussianModel(self.m, self.p_0, self.p_1, self.p_2, self.p_3)
        fit_warm_start_model = warm_start_model.fit(X=self.generatingX, y=self.generatingY)
        # ModelComparer.plot_model_parameters(fit_warm_start_model)
        # matplotlib.pyplot.show()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testThreeNode']
    unittest.main()