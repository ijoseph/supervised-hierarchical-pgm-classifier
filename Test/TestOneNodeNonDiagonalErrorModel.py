# -*- coding: utf-8 -*-
'''
Created on May 2, 2016

@author: ijoseph
'''
import unittest

from Models.LatentFactorModels import OneNodeNonDiagonalErrorGaussianModel
from Models.ModelComparer import ModelComparer
from Simulators.Simulator import Simulator
import numpy as np


class Test(unittest.TestCase):


    def setUp(self):
        
        self.N = float(1000)      
        
        np.random.seed(seed=101)        
        
        
        def is_interactive():
            '''
            Check if plots should be shown
            '''
            import __main__ as main
            return not hasattr(main, '__file__')
        
        if is_interactive():
            self.showPlots = True
        else:
            self.showPlots=True
        
        

        
        (self.m, self.p_0) = (2,5)
        self.genModel = OneNodeNonDiagonalErrorGaussianModel(self.m, self.p_0)
        self.genModel.initializeParameters(psiMean = 2, wMean=2)
        
        simulator = Simulator(self.genModel)
        simulator.simulate(self.N)
        
        
        self.fitModel = OneNodeNonDiagonalErrorGaussianModel(self.m, self.p_0)
        self.fitModel.initializeParameters()
        
        


    def testShowPlots(self):
        self.fitModel.fit(self.genModel.getObservedData())
        ModelComparer(self.genModel, self.fitModel).compareParameters(showPlots = self.showPlots)        
        ModelComparer(self.genModel, self.fitModel).compareScatterPlotZ(showPlots = self.showPlots)
        

        

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testThreeNode']
    unittest.main()