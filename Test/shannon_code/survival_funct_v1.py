from __future__ import division
from time import time
import sys
import os
import numpy as np
import scipy
import scipy.linalg.blas
import pandas as pd
import multiprocessing
import random
import sklearn
from sklearn import linear_model
import pickle
import scipy.stats
import scipy.sparse
import scipy.special
import math
import multiprocessing
##plotting commands
import matplotlib
#workaround for x - windows
matplotlib.use('Agg')
from ggplot import *
import pylab as pl
import glob
import random
import re
import matplotlib.pyplot as plt
# import cindex_code

#X = dx by nsamp
#tE = 1 by nsamp
#Delta = 1 by n samp
#Wx = dx by dz
#wt = 1 by dz
#Z = dz by nsamp
#Lam = scalar
#alpha = 1 by nsamp
##sparse = [True, sparsity threshold, sparsedensity (for createparam)]
#paramX = [(datatypeX, thetaX, sparseX), (),()]
## dataparam = [[X, (datatypeX, thetaX, sparseX)], [],[]]
#datatypes for paramX
#normal
#binom
#multinom
#etime
#cind
#cindIC


def paramcount(nfeatures, nnormalfeatures, dz, nsamp):
    """
    this does a parameter count for non - sparse models.  if delta is included
    in nfeatures, it only does informative censoring.  need to think about
    multinomial model and counting properly - - i think it's just
    nfeature_multi = dim_multi - 1.
    """
    nW = nfeatures * dz
    nunident = dz * (dz - 1) / 2
    ##insert rotation matrix
    nmu = nfeatures
    nPsi = nnormalfeatures
    nmodel = nW - nunident + nmu + nPsi
    if nsamp > nfeatures:
        print("nsamp more than nfeatures")
        pcount = (nmu + nfeatures * (nfeatures + 1) / 2) - nmodel
    else:
        pcount = (nmu + nsamp * (2 * nfeatures - nsamp + 1) / 2) - nmodel
    print('hope this is positive: dof of data - dof of model',
          nfeatures * nsamp - nmodel)
    return pcount, nmodel


def softmax(x):
    dx, nsamp = x.shape
    e_x = np.exp(x)
    sm = (e_x / np.sum(e_x, axis=0))
    ##summing over features dx
    return sm


def lamxi(xi):
    if xi.shape == ():
        xi = np.array([xi])
    lamx = (scipy.special.expit(xi) - 1 / 2) / (2 * xi)
    lamx[xi == 0] = 1 / 8
    lamx[xi > 709] = (1 / (4 * xi) )[xi>709]
    return lamx


#create parameters
def createparam(dx, dz, nsamp, datatype, b, sparse):
    if datatype == 'normal' or datatype == 'binom' or datatype == 'multinom':
        if not sparse[0]:
            Wx = np.random.multivariate_normal(
                np.zeros(dz), np.diag(np.ones(dz)), dx)
        else:
            Wx = scipy.sparse.rand(dx, dz, sparse[2])
            Wx = Wx.toarray() + np.vstack((np.diag(np.random.normal(
                [0], [1], dz)), np.zeros((dx - dz, dz))))
    if datatype == 'normal':
        Psix = np.random.gamma([1], [1], dx)[:, None]
        Mux = np.zeros(dx)
        theta = (Wx, Mux, Psix)
    elif datatype == 'binom':
        Mux = np.zeros(dx)
        xi = np.ones((dx, nsamp))
        theta = (Wx, Mux, xi, b)
    elif datatype == 'multinom':
        Wx[dx - 1, :] = 0
        ##fix for multinom
        Mux = np.zeros(dx)
        Mux[-1] = 0
        ##fix for multinom
        xi = np.ones((dx, nsamp))
        # xi[-1:, :] = 0
        # ##fix for multinom
        alpha = np.ones((1, nsamp))
        theta = (Wx, Mux, xi, alpha, b)
    elif datatype == 'etime' or datatype == 'cindIC':
        wt = np.random.normal([0], [1], dz)
        wt = np.reshape(wt, (1, dz))
        Lam = np.random.gamma(shape=[1], scale=[1])
        theta = (wt, Lam)
    elif datatype == 'cind':
        Lamc = np.random.gamma(shape=[1], scale=[1])
        theta = (Lamc)
    else:
        print('unknown datatype')
    return (datatype, theta, sparse)


#simulating from the model.
def gen_data(nsamp, dz, paramX, paramt=False, CEN=False):
    Zmean = np.zeros(dz)
    Zcov = np.diag(np.ones(dz))
    Z = (np.random.multivariate_normal(Zmean, Zcov, nsamp)).T
    dataparamX = []
    for i, para in enumerate(paramX):
        datatype, theta, sparse = para
        if datatype == 'normal':
            Wx, Mux, Psix = theta
            # X = Wx.dot(Z) + np.random.multivariate_normal(
            #     Mux, np.diag(Psix[:, 0]), nsamp).T
            # # faster way to simulate multivariate normal
            X = (Wx.dot(Z) + np.array([Mux, ] * nsamp).T
                 + np.sqrt(Psix) * (np.random.normal(size=(Wx.shape[0], nsamp))))
            dataparamX.append([X, para])
        elif datatype == 'binom':
            Wx, Mux, xi, b = theta
            Muxn = np.array([Mux, ] * nsamp).T
            X = np.random.binomial(b, scipy.special.expit(Muxn + Wx.dot(Z)))
            dataparamX.append([X, para])
        elif datatype == 'multinom':
            Wx, Mux, xi, alpha, b = theta
            assert np.sum(Wx[-1:, :]) == 0
            Muxn = np.array([Mux, ] * nsamp).T
            assert np.sum(Muxn[-1:, :]) == 0
            # assert np.sum(xi[-1:, :]) == 0
            dx = Wx.shape[0]
            p = softmax(Muxn + Wx.dot(Z))
            X = np.empty((dx, nsamp))
            for n in range(0, nsamp):
                X[:, n] = np.random.multinomial(b, p[:, n])
            dataparamX.append([X, para])
        else:
            print('unknown datatype')
            return
    if not paramt:
        dataparamt = False
    else:
        if not CEN:
            datatype, theta, sparse = paramt
            wt, Lam = theta
            t = np.random.exponential(1 / (Lam * np.exp(wt.dot(Z))))
            dataparamt = [t, paramt]
        elif CEN:
            [(datatypet, thetat, sparset),
             (datatypec, thetac, sparsec)] = paramt
            wt, Lam = thetat
            t = np.random.exponential(1 / (Lam * np.exp(wt.dot(Z))))
            if datatypec == 'cind':
                Lamc = thetac
                c = np.random.exponential(1 / (Lamc), nsamp)
                c = np.reshape(c, (1, nsamp))
            elif datatypec == 'cindIC':
                wtc, Lamc = thetac
                c = np.random.exponential(1 / (Lamc * np.exp(wtc.dot(Z))))
            tE = np.min(np.array([t, c]), axis=0)
            Delta = 1 * (tE == t)
            dataparamt = [[tE, (datatypet, thetat, sparset)],
                          [Delta, (datatypec, thetac, sparsec)]]
    return (dataparamX, dataparamt, Z)


#simulating reps of Z
def sim_Z(dz, nsamp, nrep):
    Zmean = np.zeros(dz)
    Zcov = np.diag(np.ones(dz))
    Zrep = np.empty((nrep, dz, nsamp))
    ZZtnrep = np.empty((nrep, nsamp, dz, dz))
    for i in range(0, nrep):
        Zrep[i, :, :] = (np.random.multivariate_normal(Zmean, Zcov, nsamp)).T
        ZZtnrep[i, :, :, :] = np.array([np.outer(Zrep[i, :, dn], Zrep[
            i, :, dn]) for dn in range(0, nsamp)])
    return (Zrep, ZZtnrep)


def sim_Z_Cox_functions(Zrep, wt):
    nrep, dz, nsamp = Zrep.shape
    expwtZrep = np.empty((nrep, nsamp))
    ZexpwtZrep = np.empty((nrep, dz, nsamp))
    ZZtexpwtZrep = np.empty((nrep, nsamp, dz, dz))
    for i in range(0, nrep):
        expwtZrep[i, :] = np.exp(wt.dot(Zrep[i, :, :]))
        ZexpwtZrep[i, :, :] = np.multiply(Zrep[i, :, :],
                                          np.exp(wt.dot(Zrep[i, :, :])))
        ZZtexpwtZrep[i, :, :, :] = np.array([np.outer(ZexpwtZrep[
            i, :, dn], Zrep[i, :, dn]) for dn in range(0, nsamp)])
    return (expwtZrep, ZexpwtZrep, ZZtexpwtZrep)


##using samples from Z|X metropolis - hastings to calculate E - step
def E_step_MH(Zrep):
    if len(Zrep.shape) == 3:
        nrep, dz, nsamp = Zrep.shape
        EZ = np.average(Zrep, axis=0)
        #EZZt = np.average([Zrep[x, :, :].dot(Zrep[x, :, :].T)
        #for x in range(0, nrep)], axis=0)
        EZZtn = np.array([np.average(
            [np.outer(Zrep[x, :, n], Zrep[x, :, n]) for x in range(0, nrep)],
            axis=0) for n in range(0, nsamp)])
    elif len(Zrep.shape) == 2:
        ##this use case is not an expectation, it simply calculates different
        ## functions of Z for use in lnprobdatagivenZ
        dz, nsamp = Zrep.shape
        EZ = Zrep
        #EZZt = Zrep.dot.(Zrep.T)
        EZZtn = np.array([np.outer(Zrep[:, dn], Zrep[:, dn])
                          for dn in range(0, nsamp)])
    #return(EZ, EZZt, EZZtn)
    return (EZ, EZZtn)


def E_step_MH_Cox(Zrep, wt):
    if len(Zrep.shape) == 3:
        nrep, dz, nsamp = Zrep.shape
        EexpwtZ = np.average(
            [np.exp(wt.dot(Zrep[x, :, :])) for x in range(0, nrep)],
            axis=0)
        ZexpwtZ = [np.multiply(Zrep[x, :, :], np.exp(wt.dot(Zrep[x, :, :])))
                   for x in range(0, nrep)]
        EZexpwtZ = np.average(ZexpwtZ, axis=0)
        #np.multiply(Zrep[x, :,:],np.exp(wt.dot(Zrep[x, :,:])))[:,0] -
        #Zrep[x, :, 0] * np.exp(wt.dot(Zrep[x, :,:]))[0, 0]
        EZZtexpwtZ = np.average(
            np.array([[np.outer(ZexpwtZ[x][:, y], Zrep[x, :, y])
                       for x in range(0, nrep)] for y in range(0, nsamp)]),
            axis=1)
    elif len(Zrep.shape) == 2:
        ##this use case is not an expectation, it simply calculates different
        #functions of Z for use in lnprobdatagivenZ
        dz, nsamp = Zrep.shape
        EexpwtZ = np.exp(wt.dot(Zrep))
        EZexpwtZ = np.multiply(Zrep, np.exp(wt.dot(Zrep)))
        #np.multiply(Zrep[x, :,:],np.exp(wt.dot(Zrep[x, :,:])))[:,0] -
        #Zrep[x, :, 0] * np.exp(wt.dot(Zrep[x, :,:]))[0, 0]
        EZZtexpwtZ = np.array([np.outer(EZexpwtZ[:, dn], Zrep[:, dn])
                               for dn in range(0, nsamp)])
    return (EexpwtZ, EZexpwtZ, EZZtexpwtZ)


##exact (with variational approx) calculation for no - survival E - step factor
## analysis with different datatypes.
def E_step_no_surv(dataparamX, dz):
    nsamp = dataparamX[0][0].shape[1]
    Cninv = np.empty((nsamp, dz, dz))
    for dn in range(0, nsamp):
        Cninv[dn, :, :] = np.diag(np.ones(dz))
    EZ = np.empty((dz, nsamp))
    EZZtn = np.empty((nsamp, dz, dz))
    temp = np.zeros((dz, nsamp))
    for k, datapara in enumerate(dataparamX):
        [X, (datatype, theta, sparse)] = datapara
        dx = X.shape[0]
        if datatype == 'normal':
            Wx0, Mux0, Psix0 = theta
            Psix0_inv = 1 / Psix0
            Mux0n = np.array([Mux0, ] * nsamp).T
            temp = temp + Wx0.T.dot(Psix0_inv * (X - Mux0n))
            for dn in range(0, nsamp):
                Cninv[dn, :, :] = (
                    Cninv[dn, :, :] + Wx0.T.dot(Psix0_inv * Wx0))
        elif datatype == 'binom':
            Wx0, Mux0, xi0, b = theta
            Mux0n = np.array([Mux0, ] * nsamp).T
            lamxi_xi0 = lamxi(xi0)
            temp = (temp + Wx0.T.dot(X - b / 2 * np.ones(X.shape)) - 2 * b *
                    Wx0.T.dot((lamxi_xi0 * Mux0n)))
            ## or the same, Mux - 2 * b * Wx0.T.dot((np.array([Mux0, ]).T * lamxi_xi0
            for dn in range(0, nsamp):
                for dz1 in range(0, dz):
                    for dz2 in range(0, dz):
                        Cninv[dn, dz1, dz2] = (
                            Cninv[dn, dz1, dz2] + 2 * b *
                            np.sum([lamxi_xi0[i, dn] * Wx0[i, dz1] * Wx0[
                                i, dz2] for i in range(0, dx)]))
        elif datatype == 'multinom':
            Wx0, Mux0, xi0, alpha0, b = theta
            Mux0n = np.array([Mux0, ] * nsamp).T
            lamxi_xi0 = lamxi(xi0)
            temp = (temp + Wx0.T.dot(X - b / 2 * np.ones(X.shape)) + Wx0.T.dot(
                2 * b * lamxi_xi0 * alpha0) - 2 * b *
                    Wx0.T.dot((lamxi_xi0 * Mux0n)))
            for dn in range(0, nsamp):
                for dz1 in range(0, dz):
                    for dz2 in range(0, dz):
                        Cninv[dn, dz1, dz2] = (
                            Cninv[dn, dz1, dz2] + 2 * b *
                            np.sum([lamxi_xi0[i, dn] * Wx0[i, dz1] * Wx0[
                                i, dz2] for i in range(0, dx)]))
        else:
            print('unknown datatype')
            return
    for dn in range(0, nsamp):
        Cn = scipy.linalg.inv(Cninv[dn, :, :])
        EZ[:, dn] = Cn.dot(temp[:, dn])
        EZZtn[dn, :, :] = Cn + np.outer(EZ[:, dn], EZ[:, dn])
    return (EZ, EZZtn)


## Q's are lnP(datatype|Z), and built to take in either Z or E[f(Z)]'s
def Q_Cox(wt, Lam, tE, Delta, EZ, EexpwtZ, datatype):
    #print('q_cox datatype', datatype)
    if datatype == 'cind':
        q_cox = np.sum(Delta) * np.log(Lam) - Lam * np.sum(tE)
    elif datatype == 'cindIC' or datatype == 'etime':
        q_cox = (np.sum(Delta) * np.log(Lam) + wt.dot(EZ).dot(Delta.T) - Lam *
                 EexpwtZ.dot(tE.T))
    return q_cox


def Q_Norm(Wx, Mux, Psix, X, EZ, EZZtn, sparse):
    nsamp = X.shape[1]
    dx, dz = Wx.shape
    EZZt = np.sum(EZZtn, 0)
    Psix_inv = 1 / Psix
    ##because Psix is diagonal.
    #Psix_inv = scipy.linalg.inv(Psix)
    Ginv = Wx.T.dot(Psix_inv * Wx)
    ##normal part of Cinv
    #print('Ginv in Q', Ginv)
    Muxn = np.array([Mux, ] * nsamp).T
    Psix_invdotMuxn = Psix_inv * Muxn
    WxdotEZ = Wx.dot(EZ)
    ##np.log(np.prod(np.diag(Psix))) instead of np.linalg.slogdet(Psix)[1]
    #(because Psix is diagonal) and actually, need to use:
    # - nsamp/2 * np.sum(np.log(np.diag(Psix)))
    #because otherwise product becomes 0 and it becomes infinite
    q_FA = (
        -nsamp * dx / 2 * np.log(2 * np.pi) - nsamp / 2 * np.sum(np.log(Psix))
        - 1 / 2 * np.sum(np.diag(X.T.dot(Psix_inv * X))) +
        np.sum(np.diag(X.T.dot(Psix_inv * WxdotEZ))) - 1 / 2 *
        np.sum(np.diag(EZZt.dot(Ginv))))
    #print('at q_norm', q_FA)
    q_FA = (q_FA - 1 / 2 * np.sum(np.diag(Muxn.T.dot(Psix_invdotMuxn))) -
            np.sum(np.diag(Muxn.T.dot(Psix_inv * WxdotEZ))) +
            np.sum(np.diag(X.T.dot(Psix_invdotMuxn))))
    #print('at q_norm', q_FA)
    if sparse[0]:
        q_FA = q_FA - sparse[1] * np.sum(np.abs(Wx))
    return q_FA


def Q_BIN(Wx, Mux, xi, alpha, b, X, EZ, EZZtn, sparse, datatype):
    nsamp = X.shape[1]
    dx, dz = Wx.shape
    temp = 0
    lamxi_xi = lamxi(xi)
    Muxn = np.array([Mux, ] * nsamp).T
    WxdotEZ = Wx.dot(EZ)
    ####need to caculate the prefactor out of the loop.
    #also, answers are independent of this constant factor.
    # if b != 1 and datatype == 'binom':
    #     prefactor = np.sum([np.log(scipy.misc.comb(
    #                       b * np.ones(X.shape[1]), X[i, :])) for i in
    #                        range(dx)])
    # elif b != 1 and datatype == 'multinom':
    #     prefactor = np.sum([np.log(scipy.misc.factorial(b))
    #                   - np.sum(np.log(scipy.misc.factorial(X[:, dn]))) for
    #                   dn in range(nsamp)])
    # else:         prefactor = 0
    prefactor = 0
    if datatype == 'multinom':
        ##adding in the alpha_n pieces.
        prefactor = (prefactor - b * (1 - dx / 2) * np.sum(alpha) - b *
                     np.sum(lamxi_xi.dot((alpha**2).T)) + 2 * b *
                     np.diag(lamxi_xi.T.dot(WxdotEZ)).dot(alpha.T))
        prefactor = prefactor + 2 * b * np.sum(lamxi_xi * Muxn * alpha)
        # print('alpha part', prefactor)
    #this part is now calcuated in the tensor expression below.
    # for dn in range(0, nsamp):
    #     for i in range(0, dx):
    #         temp = (temp - b * lamxi_xi[i, dn]
    #                 * Wx[i, :].dot(EZZtn[dn, :, :]).dot(Wx[i, :].T)
    temp = (-b * np.sum(lamxi_xi * np.einsum('i...i->i...',
                                             np.tensordot(
                                                 np.tensordot(Wx,
                                                              EZZtn,
                                                              axes=([1], [1])),
                                                 Wx,
                                                 axes=([2], [1])))))
    q_BIN = (prefactor +
             np.sum(np.diag((X - b / 2 * np.ones(X.shape)).T.dot(WxdotEZ))) + b
             * np.sum(np.log(scipy.special.expit(xi))) - b / 2 * np.sum(xi) +
             temp + np.sum(b * lamxi_xi * (xi**2)))
    q_BIN = (q_BIN + np.sum(X * Muxn) - b / 2 * np.sum(Muxn) - b * 2 * np.sum(
        WxdotEZ * Muxn * lamxi_xi) - b / 2 * np.sum(lamxi_xi * Muxn**2))
    if sparse[0]:
        q_BIN = q_BIN - sparse[1] * np.sum(np.abs(Wx))
    return q_BIN


def Q_Latent(EZZtn):
    nsamp, dz, dz = EZZtn.shape
    q_latent = (
        -nsamp * dz / 2 * np.log(2 * np.pi) - 1 / 2 *
        np.sum([np.sum(np.diag(EZZtn[dn, :, :])) for dn in range(nsamp)]))
    return q_latent


##simulating ln(p(X|Z)) for use in loss function - ln(p(x)).
##for Q put in EZ, EZZtn, EexpwtZ, EexptwtcZ
def lnprobdatagivenZ(dataparamX, dataparamt, Z, ZZtn, expwtZ, expwtcZ, CEN, VERBOSE=False):
    ##if dataparamt == False, then make expwtZ, expwtcZ == False.
    lnprobdatagivenZ = 0
    for i, datapara in enumerate(dataparamX):
        [X, (datatype, theta, sparse)] = datapara
        nsamp = X.shape[1]
        if datatype == 'normal':
            Wx, Mux, Psix = theta
            dx, dz = Wx.shape
            lnprobdatagivenZ = (
                lnprobdatagivenZ + Q_Norm(Wx, Mux, Psix, X, Z, ZZtn, sparse))
            if VERBOSE: print('qnorm', Q_Norm(Wx, Mux, Psix, X, Z, ZZtn, sparse))
        elif datatype == 'binom':
            Wx, Mux, xi, b = theta
            lnprobdatagivenZ = (
                lnprobdatagivenZ + Q_BIN(Wx, Mux, xi, np.zeros((1, nsamp)), b,
                                         X, Z, ZZtn, sparse, datatype))
            if VERBOSE: print('qbin', Q_BIN(Wx, Mux, xi, np.zeros((1,nsamp)), b, X, Z, ZZtn, sparse, datatype))
        elif datatype == 'multinom':
            Wx, Mux, xi, alpha, b = theta
            lnprobdatagivenZ = (lnprobdatagivenZ + Q_BIN(
                Wx, Mux, xi, alpha, b, X, Z, ZZtn, sparse, datatype))
            if VERBOSE: print('qmultinom', Q_BIN(
                Wx, Mux, xi, alpha, b, X, Z, ZZtn, sparse, datatype))
        else:
            print('unknown datatype')
            return
    #if dataparamt == False:
    #print('lnprobdatagivenZ: no time leg')
    if dataparamt != False:
        if not CEN:
            [tE, (datatypet, thetat, sparset)] = dataparamt
            wt, Lam = thetat
            # print('lnprobdatagivenZ', datatypet)
            lnprobdatagivenZ = (lnprobdatagivenZ + Q_Cox(
                wt, Lam, tE, np.ones((1, nsamp)), Z, expwtZ, datatypet))
            if VERBOSE: print('qcox', Q_Cox(wt, Lam, tE, np.ones((1, nsamp)), Z, expwtZ, datatypet))
        elif CEN:
            [[tE, (datatypet, thetat, sparset)],
             [Delta, (datatypec, thetac, sparsec)]] = dataparamt
            wt, Lam = thetat
            Deltac = np.ones(Delta.shape) - Delta
            lnprobdatagivenZ = (lnprobdatagivenZ + Q_Cox(wt, Lam, tE, Delta, Z,
                                                         expwtZ, datatypet))
            if VERBOSE: print('qcox', Q_Cox(wt, Lam, tE, Delta, Z, expwtZ, datatypet))
            if datatypec == 'cind':
                Lamc = thetac
                lnprobdatagivenZ = (lnprobdatagivenZ + Q_Cox(
                    np.zeros((1, dz)), Lamc, tE, Deltac, Z, False, datatypec))
                if VERBOSE: print('qcox cind', Q_Cox(np.zeros((1, dz)), Lamc, tE, Deltac, Z, False, datatypec))
            elif datatypec == 'cindIC':
                wtc, Lamc = thetac
                lnprobdatagivenZ = (lnprobdatagivenZ + Q_Cox(
                    wtc, Lamc, tE, Deltac, Z, expwtcZ, datatypec))
                if VERBOSE: print('qcox cindIC', Q_Cox(wtc, Lamc, tE, Deltac, Z, expwtcZ, datatypec))
    assert np.isinf(lnprobdatagivenZ) == False
    return lnprobdatagivenZ


##exact (with variational approx) calculation for no - survival loss function
#- ln(p(x)) with different datatypes.
def loss_function_no_surv(dataparamX, EZ, EZZtn):
    nsamp = dataparamX[0][0].shape[1]
    Z = np.zeros(EZ.shape)
    ZZtn = np.zeros(EZZtn.shape)
    lnprobdata = lnprobdatagivenZ(dataparamX, False, Z, ZZtn, False, False,
                                  False)
    ##this calcuates the bits that are independent of z in p(x|z)
    for dn in range(0, nsamp):
        Cn = EZZtn[dn, :, :] - np.outer(EZ[:, dn], EZ[:, dn])
        ##Cn is the covariance matrix, EZ[:, dn] is the mean.
        lnprobdata = (lnprobdata + 1 / 2 *
                      EZ[:, dn].T.dot(scipy.linalg.inv(Cn)).dot(EZ[:, dn]) -
                      np.linalg.slogdet(Cn)[1] / 2)
        ## no numerical part because that canceled out in the integration.
    return -lnprobdata


##ln(p(X, Z)) for metropolis - hastings.  this function also calculates Q(model) when Z arguments are expectations of functions of Z.
def jointlogprob(dataparamX, dataparamt, Z, ZZtn, expwtZ, expwtcZ, CEN):
    jntlnprob = lnprobdatagivenZ(dataparamX, dataparamt, Z, ZZtn, expwtZ,
                                 expwtcZ, CEN) + Q_Latent(ZZtn)
    return jntlnprob


def pick_out_ith_individual(i, dataparamX, dataparamt, CEN):
    ##pick out the ith individual
    dataparamXi = []
    for k, datapara in enumerate(dataparamX):
        [X, (datatype, theta, sparse)] = datapara
        Xi = X[:, i]
        Xi = np.reshape(Xi, (X.shape[0], 1))
        if datatype == 'binom':
            Wx, Mux, xi, b = theta
            theta = (Wx, Mux, np.reshape(xi[:, i], (X.shape[0], 1)), b)
        elif datatype == 'multinom':
            Wx, Mux, xi, alpha, b = theta
            theta = (Wx, Mux, np.reshape(xi[:, i], (X.shape[0], 1)),
                     np.reshape(alpha[:, i], (1, 1)), b)
        dataparamXi.append([Xi, (datatype, theta, sparse)])
    if dataparamt == False:
        dataparamti = False
    else:
        if not CEN:
            [tE, (datatype, theta, sparse)] = dataparamt
            tEi = np.reshape(tE[:, i], (1, 1))
            dataparamti = [tEi, (datatype, theta, sparse)]
        elif CEN:
            [[tE, (datatypet, thetat, sparset)],
             [Delta, (datatypec, thetac, sparsec)]] = dataparamt
            tEi = np.reshape(tE[:, i], (1, 1))
            Deltai = np.reshape(Delta[:, i], (1, 1))
            dataparamti = [[tEi, (datatypet, thetat, sparset)],
                           [Deltai, (datatypec, thetac, sparsec)]]
    return (dataparamXi, dataparamti)


def calculate_time_functions_for_MH(dataparamt, Zti, CEN):
    #calculating functions of Z for jointlogprob
    Zti, ZZti = E_step_MH(Zti)
    if dataparamt == False:
        expwtZi, expwtcZi = False, False
    #print('MH_sim_Z_given_data_p: no time leg')
    else:
        if not CEN:
            [tE, (datatype, theta, sparse)] = dataparamt
            wt, Lam = theta
            expwtZi, ZexpwtZi, ZZtexpwtZi = E_step_MH_Cox(Zti, wt)
            expwtcZi = False
        elif CEN:
            [[tE, (datatypet, thetat, sparset)],
             [Delta, (datatypec, thetac, sparsec)]] = dataparamt
            wt, Lam = thetat
            if datatypec == 'cind':
                expwtZi, ZexpwtZi, ZZtexpwtZi = E_step_MH_Cox(Zti, wt)
                expwtcZi = False
            elif datatypec == 'cindIC':
                wtc, Lamc = thetac
                expwtZi, ZexpwtZi, ZZtexpwtZi = E_step_MH_Cox(Zti, wt)
                expwtcZi, ZexpwtcZi, ZZtexpwtcZi = E_step_MH_Cox(Zti, wtc)
    return (Zti, ZZti, expwtZi, expwtcZi)


##simulating Z|X for metropolis - hastings.
##consider using emcee package instead.  i would have to adapt the package to make it possible to use with missing data, however.
## to add missing data, simulate missing elements while going through dataparamX.  since Psi is diagonal, it is okay to simulate the missing components independently.  i am not sure what to do instead when psi is not diagonal.
##must remember to copy functions of Z at the bottom.
##this new version is twice as fast, since jointlogprob is called 1/2 as many times.  also saves the trouble of copying all the functions.
def MH_sim_Z_given_data_p((i, dz, dataparamX, dataparamt, nrep, CEN, burn,
                           SEED)):
    #print(SEED)
    np.random.seed(SEED)
    (dataparamXi, dataparamti) = pick_out_ith_individual(i, dataparamX,
                                                         dataparamt, CEN)
    Zcov = np.diag(np.ones(dz))
    Zrep = np.empty((nrep + burn, dz))
    #counter = 0
    #initialize
    sigma2 = 1
    Zti = np.random.multivariate_normal(np.zeros(dz), sigma2 * Zcov)
    Zti = np.reshape(Zti, (dz, 1))
    ##this is simply to get the random calls to align with the missing data, to enable checking.
    #Zti= so_can_compare(np.zeros(dz), dz, dataparamXi)
    (Zti, ZZti, expwtZi, expwtcZi) = calculate_time_functions_for_MH(
        dataparamti, Zti, CEN)
    q = jointlogprob(dataparamXi, dataparamti, Zti, ZZti, expwtZi, expwtcZi,
                     CEN)
    c = 0
    #acceptance = np.empty((nrep + burn, 1))
    #done = False
    #simulate
    #while c<(nrep + burn) and done == False:
    while c < (nrep + burn):
        Ztpi = np.random.multivariate_normal(Zti[:, 0], sigma2 * Zcov)
        Ztpi = np.reshape(Ztpi, (dz, 1))
        ##this is simply to get the random calls to align with the missing data, to enable checking.
        #Ztpi= so_can_compare(Zti[:, 0], dz, dataparamXi)
        (Ztpi, ZZtnpi, expwtZpi, expwtcZpi) = calculate_time_functions_for_MH(
            dataparamti, Ztpi, CEN)
        #calculating cutoff
        qp = jointlogprob(dataparamXi, dataparamti, Ztpi, ZZtnpi, expwtZpi,
                          expwtcZpi, CEN)
        #cutoff = np.exp(jointlogprob(dataparamXi, dataparamti, Ztpi, ZZtnpi, expwtZpi, expwtcZpi, CEN) - jointlogprob(dataparamXi, dataparamti, Zti, ZZti, expwtZi, expwtcZi, CEN))
        cutoff = np.exp(qp - q)
        # print(c, cutoff)
        # print('qp, q', qp, q, cutoff == np.exp(jointlogprob(dataparamXi, dataparamti, Ztpi, ZZtnpi, expwtZpi, expwtcZpi, CEN) - jointlogprob(dataparamXi, dataparamti, Zti, ZZti, expwtZi, expwtcZi, CEN)))
        assert np.isnan(cutoff) == False
        if cutoff >= 1:
            Zrep[c, :] = np.copy(Ztpi[:, 0])
            Zti = np.copy(Ztpi)
            # ZZti = np.copy(ZZtnpi)
            # expwtZi = np.copy(expwtZpi)
            # expwtcZi = np.copy(expwtcZpi)
            q = np.copy(qp)
        #    acceptance[c, :] = 1
        else:
            if np.random.binomial(1, cutoff) == 1:
                Zrep[c, :] = np.copy(Ztpi[:, 0])
                Zti = np.copy(Ztpi)
                # ZZti = np.copy(ZZtnpi)
                # expwtZi = np.copy(expwtZpi)
                # expwtcZi = np.copy(expwtcZpi)
                q = np.copy(qp)
        #        acceptance[c, :] = 1
            else:
                Zrep[c, :] = np.copy(Zti[:, 0])
        #        acceptance[c, :] = 0
        #        if c == burn:
        #            arate = np.sum(acceptance[0:burn, :])/burn
        #            #print(arate)
        #            counter = counter + 1 * (arate>0.1 and arate<.6)
        #            #print(counter)
        c = c + 1
    return Zrep[burn:(burn + nrep), :]


## simulating log p(x) using metropolis hastings
def mh_sim_log_marginal_data((i, dz, dataparamX, dataparamt, Zrep, CEN, SEED)):
    ##Zrep needs to be the samples from mh_sim_Z_given_data_p
    np.random.seed(SEED)
    (dataparamXi, dataparamti) = pick_out_ith_individual(i, dataparamX,
                                                         dataparamt, CEN)
    ##making the first sample the reference point.
    zstar = Zrep[0, :, i, None]
    (Zti, ZZti, expwtZi, expwtcZi) = calculate_time_functions_for_MH(
        dataparamti, zstar, CEN)
    qstar = jointlogprob(dataparamXi, dataparamti, Zti, ZZti, expwtZi,
                         expwtcZi, CEN)
    #print('estimate of log p(zstar, x)', qstar, i)
    ##initializing arrays
    nsamp_sim = Zrep.shape[0] - 1
    qrep = np.empty(nsamp_sim)
    gen_den_zrep = np.empty(nsamp_sim)
    q_gen_den_samp = np.empty(nsamp_sim)
    cutoff_zrep = np.empty(nsamp_sim)
    cutoff_gen_den_samp = np.empty(nsamp_sim)
    ##generating samples from the candidate generating density
    sigma2 = 1
    Zcov = np.diag(np.ones(dz))
    gen_den_samp = np.random.multivariate_normal(zstar[:, 0], sigma2 * Zcov,
                                                 nsamp_sim)
    for k in range(nsamp_sim):
        ##calculating the q function for points sampled from z|x.  (the zeroth point is the reference point)
        Zti = Zrep[k + 1, :, i, None]
        (Zti, ZZti, expwtZi, expwtcZi) = calculate_time_functions_for_MH(
            dataparamti, Zti, CEN)
        qrep[k] = jointlogprob(dataparamXi, dataparamti, Zti, ZZti, expwtZi,
                               expwtcZi, CEN)
        #the candidate generating probability density for the points sampled from z|x and the reference point zstar
        gen_den_zrep[k] = 1 / (np.power(
            np.sqrt(2 * np.pi), dz)) * np.exp(-np.sum((zstar - Zti)**2) / 2)
        ##calculating the q function for points sampled from the candidate generating density
        Zti = gen_den_samp[k, :, None]
        (Zti, ZZti, expwtZi, expwtcZi) = calculate_time_functions_for_MH(
            dataparamti, Zti, CEN)
        q_gen_den_samp[k] = jointlogprob(dataparamXi, dataparamti, Zti, ZZti,
                                         expwtZi, expwtcZi, CEN)
        cutoff_zrep[k] = np.min([1, np.exp(qstar - qrep[k])])
        cutoff_gen_den_samp[k] = np.min([1, np.exp(q_gen_den_samp[k] - qstar)])
    ## estimate of p(zstar|x)
    p_zstargivendata = np.sum(cutoff_zrep *
                              gen_den_zrep) / np.sum(cutoff_gen_den_samp)
    #print('estimate of p(zstar|x)', p_zstargivendata, np.log(p_zstargivendata), i)
    log_marginal_data = qstar - np.log(p_zstargivendata)
    return log_marginal_data


def M_step_Norm(Wx0, Psix0, X, EZ, EZZtn, sparse):
    nsamp = X.shape[1]
    dx, dz = Wx0.shape
    EZZt = np.sum(EZZtn, axis=0)
    if not sparse[0]:
        Wxp = X.dot(EZ.T).dot(scipy.linalg.inv(EZZt))
        #Psixp = 1/nsamp * (X.dot(X.T) - Wxp.dot(EZZt).dot(Wxp.T))
        Psixp = 1 / nsamp * np.diag(X.dot(X.T) - Wxp.dot(EZZt).dot(
            Wxp.T))[:, None]
        #print(Psixp)
        #Psixp = 1/nsamp * np.diag(X.dot(X.T) - Wxp.dot(EZ.dot(X.T)))
        #print(Psixp)
        #Psixp = np.diag(Psixp)
    else:
        Wxp = np.empty((dx, dz))
        #alt = np.empty((dx, dz))
        LEZZt = np.linalg.cholesky(EZZt)
        print('checking svd of cholesky', scipy.linalg.svd(LEZZt)[1])
        Y = X.dot(EZ.T).dot(scipy.linalg.inv(LEZZt.T))
        for i in range(0, dx):
            #print('cutoff', Psix0[i, i] * sparse[1]/dz)
            LLfit = linear_model.LassoLars(Psix0[i, 0] * sparse[1] / dz,
                                           fit_intercept=False)
            ##otherwise intercept is included in code and not penalized.
            LLfit.fit(LEZZt.T, Y[i, :])
            Wxp[i, :] = LLfit.coef_
            print('Wxp all zero?', np.sum(Wxp[i, :]) == 0)
            ##testing ronglai's algorithm.
            #            print('sparse Wxp solution', Wxp[i, :])
            #            print(1/(2 * dz) * (Y[i, :] - Wxp[i, :].dot(LEZZt)).dot(Y[i, :] - Wxp[i, :].dot(LEZZt)) + Psix0[i, i] * sparse[1]/ dz * np.sum(np.abs(Wxp[i, :])))
            #            alt_aas = (X[i, :].dot(EZ.T).dot(scipy.linalg.inv(EZZt + 2 * Psix0[i, i] * sparse[1] * scipy.linalg.inv(np.diag(np.abs(Wx0[i, :])))))
            #            print('not as good solution', alt)
            #            print(1/(2 * dz) * (Y[i, :] - alt_aas.dot(LEZZt)).dot(Y[i, :] - alt_aas.dot(LEZZt)) + Psix0[i, i] * sparse[1]/ dz * np.sum(np.abs(alt_aas)))
            #            ind = 1 * scipy.greater((X.dot(EZ.T).dot(scipy.linalg.inv(EZZt)))[i, :] - Psix0[i, i] * sparse[1]/ dz, np.zeros((1, dz)))
            #            pospart = ind * ((X.dot(EZ.T).dot(scipy.linalg.inv(EZZt)))[i, :] - Psix0[i, i] * sparse[1]/ dz)
            #            alt = np.sign((X.dot(EZ.T).dot(scipy.linalg.inv(EZZt)))[i, :]) * pospart
            #            alt = alt[0]
            #            print('not as good solution', alt)
            #            print(1/(2 * dz) * (Y[i, :] - alt.dot(LEZZt)).dot(Y[i, :] - alt.dot(LEZZt)) + Psix0[i, i] * sparse[1]/ dz * np.sum(np.abs(alt)))
            ##end test.
            #print(1/(2 * dz) * (Y[i, :] - Wxp[i, :].dot(LEZZt)).dot(Y[i, :] - Wxp[i, :].dot(LEZZt)))
            #Lfit = linear_model.LinearRegression(fit_intercept=False)
            #Lfit.fit(LEZZt.T,Y[i, :])
            #alt[i, :] = Lfit.coef_
            #print("difference from regular regression", Wxp[i, :] - alt[i, :])
            #print(1/(2 * dz) * (Y[i, :] - alt[i, :].dot(LEZZt)).dot(Y[i, :] - alt[i, :].dot(LEZZt)) + Psix0[i, i] * sparse[1]/ dz * np.sum(np.abs(alt[i, :])))
            #print(1/(2 * dz) * (Y[i, :] - alt[i, :].dot(LEZZt)).dot(Y[i, :] - alt[i, :].dot(LEZZt)))
            #print(Y[i, :] - LEZZt.T.dot(alt[i, :]))
        print('M_step_Norm, sparse: should all be positive, abs(full) - abs(sparse)', np.abs(
            X.dot(EZ.T).dot(scipy.linalg.inv(EZZt))) - np.abs(Wxp))
        #print(' should match', X.dot(EZ.T).dot(scipy.linalg.inv(EZZt)) - alt)
        #print(np.diag(Psix0))
        Psixp = 1 / nsamp * np.diag(X.dot(X.T) - Wxp.dot(EZZt).dot(
            Wxp.T))[:, None]
        assert Psixp.shape == (dx, 1)
        small = 0.00005
        print("checks on w, psi", np.sum(Psixp > small), np.linalg.matrix_rank(
            Wxp,
            tol=None))
        assert np.sum(Psixp > small) == dx
        #assert np.linalg.matrix_rank(Wxp, tol=None) == np.min([dx, dz])
        #Psixp = np.diag(Psixp)
    return (Wxp, Psixp)


def M_step_BIN(Wx0, Mux0, xi0, alpha0, b, X, EZ, EZZtn, sparse, datatype):
    ##EZZtn[n, j, k]
    nsamp = X.shape[1]
    dx, dz = Wx0.shape
    Wxp = np.empty((dx, dz))
    xip = np.empty((dx, nsamp))
    temp = np.tensordot(Wx0, EZZtn, axes=([1], [1]))
    ##[dx = i, nsamp, dz = i, k]
    Mux0n = np.array([Mux0, ] * nsamp).T
    print('M_step_BIN:  isnan temp', np.sum(np.isnan(temp)),
          np.sum(np.isnan(EZZtn)))
    for i in range(0, dx):
        xip[i, :] = np.sqrt(
            np.tensordot(Wx0[i, :],
                         temp[i, :, :],
                         axes=([0], [1])) + 2 * Mux0[i] * Wx0[i, :].dot(EZ) +
            Mux0n[i, :]**2 + (datatype == 'multinom') * (-2 * Wx0[i, :].dot(
                EZ) * alpha0 + alpha0**2 - 2 * alpha0 * Mux0[i]))
        #print('less than zero?',
        #    np.sum((np.tensordot(Wx0[i, :], temp[i, :, :], axes=([0], [1]))
        #    + (datatype == 'multinom') * ( - 2 * Wx0[i, :].dot(EZ) * alpha0
        #    + alpha0 ** 2))<0), 'equal zero?',
        #   np.sum((np.tensordot(Wx0[i, :], temp[i, :, :], axes=([0], [1]))
        #   + (datatype == 'multinom') * ( - 2 * Wx0[i, :].dot(EZ) * alpha0
        #    + alpha0 ** 2)) == 0))
        print('Wx0 all zero?', np.sum(Wx0[i, :]) == 0)
        print('isnan?', np.sum(np.isnan(lamxi(xip[i, :]))),
              np.sum(np.isnan(xip[i, :])))
    # if datatype == 'multinom':
    #     xip[-1:, :] = 0
    #         ##fix for multinom
    lamxi_xip = lamxi(xip)
    if datatype == 'multinom':
        alphap = (
            np.diag(lamxi_xip.T.dot(Wx0).dot(EZ)) + lamxi_xip.T.dot(Mux0) -
            (1 - dx / 2) / 2) / np.sum(lamxi_xip,
                                       axis=0)
        alphap = np.reshape(alphap, (1, nsamp))
        print('alpha', alphap[0][0:5])
    else:
        alphap = alpha0
    if not sparse[0]:
        for i in range(0, dx):
            L = np.linalg.cholesky(2 * b * np.tensordot(lamxi_xip[i, :],
                                                        EZZtn,
                                                        axes=([0], [0])))
            #print('checking svd of cholesky', scipy.linalg.svd(L)[1])
            Y = ((X[i, :] - b / 2 * np.ones(X[i, :].shape) - 2 * b * Mux0[i] *
                  lamxi_xip[i, :] + (datatype == 'multinom') * 2 * b *
                  (alphap *
                   lamxi_xip[i, :])).dot(EZ.T).dot(scipy.linalg.inv(L.T)))
            Lfit = linear_model.LinearRegression(fit_intercept=False)
            Lfit.fit(L.T, Y.T)
            Wxp[i, :] = Lfit.coef_
    else:
        for i in range(0, dx):
            L = np.linalg.cholesky(2 * b * np.tensordot(lamxi_xip[i, :],
                                                        EZZtn,
                                                        axes=([0], [0])))
            #print('checking svd of cholesky', scipy.linalg.svd(L)[1])
            Y = ((X[i, :] - b / 2 * np.ones(X[i, :].shape) - 2 * b * Mux0[i] *
                  lamxi_xip[i, :] + (datatype == 'multinom') * 2 * b *
                  (alphap *
                   lamxi_xip[i, :])).dot(EZ.T).dot(scipy.linalg.inv(L.T)))
            LLfit = linear_model.LassoLars(sparse[1] / dz, fit_intercept=False)
            ##otherwise intercept is included in code and not penalized.
            LLfit.fit(L.T, Y.T)
            Wxp[i, :] = LLfit.coef_
    Muxp = ((np.sum(X,
                    axis=1) - b / 2 * nsamp - 2 * b *
             np.diag(lamxi_xip.dot(EZ.T).dot(Wxp.T)) + 2 * b *
             lamxi_xip.dot(alphap.T)[:, 0]) / (2 * b * np.sum(lamxi_xip,
                                                              axis=1)))
    if datatype == 'multinom':
        assert np.sum(Wx0[-1:, :]) == 0
        assert np.sum(Mux0n[-1:, :]) == 0
        # assert np.sum(xi0[-1:, :]) == 0
        Wxp[-1:, :] = 0
        ##fix for multinom
        Muxp[-1] = 0
        ##fix for multinom
        #assert np.linalg.matrix_rank(Wxp, tol=None) == np.min([dx-1, dz])
    else:
        print(np.linalg.matrix_rank(Wxp, tol=None))
        # assert np.linalg.matrix_rank(Wxp, tol=None) == np.min([dx, dz])
    # print np.linalg.matrix_rank(Wxp, tol=None)
    return (Wxp, Muxp, xip, alphap)


##newton - raphson m - step update for exponential time data
def M_step_NR(wt0, Lam0, tE, Delta, EZ, EexpwtZ, EZexpwtZ, EZZtexpwtZ, step):
    dz, nsamp = EZ.shape
    A = EexpwtZ.dot(tE.T)
    A = np.reshape(A, (1, 1))
    B = EZexpwtZ.dot(tE.T)
    B = np.reshape(B, (1, dz))
    D = np.sum(
        np.array([tE[:, dn] * EZZtexpwtZ[dn, :, :] for dn in range(0, nsamp)]),
        axis=0)
    #(Ap, Bp, Cp, DCAB) = blockinv(A, B, B.T, D)
    ##checking inverse
    #print(scipy.linalg.inv(np.bmat([[A, B], [B.T, D]])) - np.bmat([[Ap, Bp], [Bp.T, DCAB]]))
    #print(np.bmat([[Ap, Bp], [Bp.T, DCAB]]).dot(np.bmat([[A, B], [B.T, D]])))
    #print(scipy.linalg.inv(np.bmat([[A, B], [B.T, D]])).dot(np.bmat([[A, B], [B.T, D]])))
    alle = np.bmat([[A, B], [B.T, D]])
    assert scipy.linalg.svd(alle)[1][-1:] > .000001
    all_inv = scipy.linalg.inv(alle)
    Ap = all_inv[0, 0, None, None]
    Bp = all_inv[0, 1:, None].T
    DCAB = all_inv[1:, 1:]
    print('M_step_NR: svd of [[Ap, Bp], [Bp.T, DCAB]] ',
          scipy.linalg.svd(np.bmat([[Ap, Bp], [Bp.T, DCAB]]))[1])
    Mp = np.sum(Delta) - Lam0 * A
    Mp = np.reshape(Mp, (1, 1))
    Np = EZ.dot(Delta.T).T - Lam0 * B
    #print("Mp", Mp)
    #print("Np", Np)
    beta0 = np.log(Lam0)
    betap = beta0 + step * (Ap * Mp + Bp.dot(Np.T)) / Lam0
    Lamp = np.exp(betap)
    #print("betap", betap)
    wtp = wt0 + step * (Bp.T * Mp + DCAB.dot(Np.T)).T / Lam0
    #print("wtp", wtp, wtp.shape)
    return (wtp, Lamp)


def pPCAsoln(X, dz):
    dx, nsamp = X.shape
    Ux, lamx, VxT = scipy.linalg.svd(X, full_matrices=False)
    if dx > dz:
        SigpPCA = sum(lamx[dz:]**2) / (nsamp * (dx - dz))
        WxpPCA = Ux[:, 0:dz].dot(np.diag(np.sqrt(lamx[0:dz]**2 - SigpPCA * np.ones(
            dz))))
        #PsixpPCA = np.diag(SigpPCA * np.ones(dx))
        PsixpPCA = (SigpPCA * np.ones(dx))[:, None]
    if dx <= dz:
        SigpPCA = lamx[-1]**2 / nsamp
        WxpPCA = np.ones((dx, dz))
        PsixpPCA = (SigpPCA * np.ones(dx))[:, None]
        print('dx<= dz, not ppca initialization')
    return WxpPCA, PsixpPCA


def EM_initialize(dataparamX, dataparamt, dz, CEN):
    nsamp = dataparamX[0][0].shape[1]
    Xmeans = []
    dataparamX0 = []
    dataparamt0 = []
    for i, datapara in enumerate(dataparamX):
        [X, (datatype, theta, sparse)] = datapara
        dx = X.shape[0]
        if datatype == 'normal':
            Xmean = np.mean(X, 1)
            Xmeans.append(Xmean)
            # reporting out means for normal data.
            Xc = X - np.array([Xmean, ] * nsamp).T
            #initialize.  centering normal data and giving it mean zero.
            (Wx0, Psix0) = pPCAsoln(Xc, dz)
            Mux0 = np.zeros(Xmean.shape)
            theta0 = (Wx0, Mux0, Psix0)
            datapara0 = [Xc, (datatype, theta0, sparse)]
            dataparamX0.append(datapara0)
        elif datatype == 'binom':
            Xmean = np.mean(X, 1)
            Xmeans.append(Xmean)
            Xc = X - np.array([Xmean, ] * nsamp).T
            #initialize
            (Wx0, Psix0) = pPCAsoln(Xc, dz)
            xi0 = np.ones((dx, nsamp))
            b = theta[3]
            theta0 = (Wx0, Xmean, xi0, b)
            datapara0 = [X, (datatype, theta0, sparse)]
            dataparamX0.append(datapara0)
        elif datatype == 'multinom':
            Xmean = np.mean(X, 1)
            Xmeans.append(Xmean)
            Xc = X - np.array([Xmean, ] * nsamp).T
            #initialize
            (Wx0, Psix0) = pPCAsoln(Xc, dz)
            Wx0[-1:, :] = 0
            ##fix for multinom
            Xmean[-1] = 0
            ##fix for multinom
            xi0 = np.ones((dx, nsamp))
            # xi0[-1:, :] = 0
            # ##fix for multinom
            alpha0 = np.ones((1, nsamp))
            b = theta[4]
            theta0 = (Wx0, Xmean, xi0, alpha0, b)
            datapara0 = [X, (datatype, theta0, sparse)]
            dataparamX0.append(datapara0)
        else:
            print('unknown datatype')
            return
    if dataparamt == False:
        print('initialization: no time leg')
        dataparamt0 = dataparamt
    else:
        if not CEN:
            [tE, (datatype, theta, sparse)] = dataparamt
            Lam0 = nsamp / np.sum(tE)
            wt0 = np.zeros((1, dz))
            #wt0 = np.random.normal([0], [1], dz)
            #wt0 = np.reshape(wt0, (1, dz))
            theta0 = (wt0, Lam0)
            dataparamt0 = [tE, (datatype, theta0, sparse)]
        elif CEN:
            [[tE, (datatypet, thetat, sparset)],
             [Delta, (datatypec, thetac, sparsec)]] = dataparamt
            Lam0 = np.sum(Delta) / np.sum(tE)
            wt0 = np.zeros((1, dz))
            #wt0 = np.random.normal([0], [1], dz)
            #wt0 = np.reshape(wt0, (1, dz))
            thetat0 = wt0, Lam0
            Deltac = np.ones(Delta.shape) - Delta
            if datatypec == 'cind':
                Lamc0 = np.sum(Deltac) / np.sum(tE)
                thetac0 = Lamc0
            elif datatypec == 'cindIC':
                Lamc0 = np.sum(Deltac) / np.sum(tE)
                wtc0 = np.zeros((1, dz))
                #wtc0 = np.random.normal([0], [1], dz)
                #wtc0 = np.reshape(wtc0, (1, dz))
                thetac0 = wtc0, Lamc0
            dataparamt0 = [[tE, (datatypet, thetat0, sparset)],
                           [Delta, (datatypec, thetac0, sparsec)]]
    return (dataparamX0, dataparamt0, Xmeans)


def check_convergence(dataparamXp, dataparamtp, dataparamX0, dataparamt0, CEN):
    maxdiff = 0
    for i, datapara in enumerate(dataparamX0):
        [X, (datatype, theta0, sparse)] = datapara
        #theta0 = datapara[1][1]
        #[X, (datatype, thetap, sparse)] = dataparamXp[i]
        thetap = dataparamXp[i][1][1]
        if datatype == 'normal':
            Wx0, Mux0, Psix0 = theta0
            Wxp, Muxp, Psixp = thetap
            maxdiff = np.max([np.max(np.abs(Wxp - Wx0)), maxdiff])
        elif datatype == 'binom':
            Wx0, Mux0, xi0, b = theta0
            Wxp, Muxp, xip, b = thetap
            maxdiff = np.max([np.max(np.abs(Wxp - Wx0)), maxdiff])
        elif datatype == 'multinom':
            Wx0, Mux0, xi0, alpha0, b = theta0
            Wxp, Muxp, xip, alphap, b = thetap
            maxdiff = np.max([np.max(np.abs(Wxp - Wx0)), maxdiff])
        else:
            print('unknown datatype')
            return
    if dataparamt0 == 0:
        pass
#         print('check_convergence: no time leg')
    else:
        if not CEN:
            [tE, (datatype, thetat0, sparse)] = dataparamt0
            #thetat0 = dataparamt0[1][1]
            thetatp = dataparamtp[1][1]
            wt0, Lam0 = thetat0
            wtp, Lamp = thetatp
            maxdiff = np.max([np.max(np.abs(wtp - wt0)), maxdiff])
        elif CEN:
            [[tE, (datatypet, thetat0, sparset)],
             [Delta, (datatypec, thetac0, sparsec)]] = dataparamt0
            #thetat0 = dataparamt0[0][1][1]
            thetatp = dataparamtp[0][1][1]
            #thetac0 = dataparamt0[1][1][1]
            thetacp = dataparamtp[1][1][1]
            wt0, Lam0 = thetat0
            wtp, Lamp = thetatp
            if datatypec == 'cind':
                #Lamcp = thetac0
                maxdiff = np.max([np.max(np.abs(wtp - wt0)), maxdiff])
            elif datatypec == 'cindIC':
                wtc0, Lamc0 = thetac0
                wtcp, Lamcp = thetacp
                maxdiff = np.max([np.max(np.abs(wtp - wt0)), np.max(np.abs(
                    wtcp - wtc0)), maxdiff])
    return maxdiff


def EM_all(dataparamX, dataparamt, dz, niter, convgap, mh_params, step, CEN,
           INIT, plotloc):
    #initialize
    if INIT:
        (dataparamX0, dataparamt0, Xmeans) = EM_initialize(dataparamX,
                                                           dataparamt, dz, CEN)
    else:
        
        # IJ Added: mean-centering even if not initialized 
        [[X, [datatpye, theta, sparse]]]  = dataparamX
        XDF = pd.DataFrame(X.T)
        XDF = XDF.sub(XDF.mean())
        X = XDF.as_matrix().T
        dataparamX = [[X, [datatpye, theta, sparse]]]
        # done with IJ additions.       
        
        dataparamX0, dataparamt0 = dataparamX, dataparamt        
        Xmeans = None
    del dataparamX, dataparamt
    nsamp = dataparamX0[0][0].shape[1]
    q = np.empty((niter, 2))
    Ldata = np.empty((niter, 2))
    for i in range(0, niter):
        #E - step
        if dataparamt0 == 0:
#             print('E - step: no time leg, estep is analytical ' +
#                   '(under variational approximation)')
            EZ, EZZtn = E_step_no_surv(dataparamX0, dz)
            EexpwtZ, EexpwtcZ = False, False
            ## recording parameters
            qq = jointlogprob(dataparamX0, dataparamt0, EZ, EZZtn, EexpwtZ,
                              EexpwtcZ, CEN)
            q[i, :] = np.array([qq, i])
            Ldata[i, :] = Ldata[i, :] = np.array([loss_function_no_surv(
                dataparamX0, EZ, EZZtn), i])
        #print('loss function diff', loss_function_no_surv(dataparamX0,
        #EZ, EZZtn) - loss_function_no_surv_alt(dataparamX0))
        #print('here')
        else:
            ##creating samples for empirical expectations.
            #Zrep = MH_sim_Z_given_data((dz, dataparamX0, dataparamt0,
            #nrep, CEN, burn = 1000))
            batch_number, nrep_batch, burn, MULTIPRO, PRE_SEED = mh_params
            nrep = batch_number * nrep_batch
            Zrep = np.empty((nrep, dz, nsamp))
            if not MULTIPRO:
                for j in range(0, batch_number):
#                     Zrepj = MH_sim_Z_given_data((dz, dataparamX0, dataparamt0,
#                                                  nrep_batch, CEN, burn))
                    raise NotImplementedError("Don't have MH_sim_Z_given_data")
                    Zrepj = None
                    Zrep[(nrep_batch * j):(nrep_batch * j + nrep_batch
                                           ), :, :] = Zrepj
            elif MULTIPRO:
                if sys.platform == "linux" or sys.platform == "linux2":
                    numThreads = 50
                elif sys.platform == "darwin":
                    numThreads = 4
                if not os.path.exists(plotloc + 'mh_seeds/'):
                    os.makedirs(plotloc + 'mh_seeds/')
                if not PRE_SEED:
                    if sys.platform == "linux" or sys.platform == "linux2":
                        large = 100000000000
                    elif sys.platform == "darwin":
                        large = 4294967295
                    SEED = np.random.random_integers(
                        0,
                        large,
                        size=(nsamp * batch_number))
                    assert np.unique(SEED).shape == SEED.shape
                    f = open(plotloc + 'mh_seeds/'+ 'seed_' + str(i) + '.py', 'w')
                    pickle.dump(SEED, f)
                    f.close()
                else:
                    f = open(plotloc +  'mh_seeds/'+ 'seed_' + str(i) + '.py', 'r')
                    SEED = pickle.load(f)
                    f.close()
                pool = multiprocessing.Pool(numThreads)
                results = pool.map_async(MH_sim_Z_given_data_p,
                                         [(dn, dz, dataparamX0, dataparamt0,
                                           nrep_batch, CEN, burn,
                                           SEED[dn * batch_number + j])
                                          for dn in range(nsamp)
                                          for j in range(batch_number)])
                # set the pool to work
                pool.close()
                # party's over, kids
                pool.join()
                # wait for all tasks to finish
                results = results.get()
                for dn in range(nsamp):
                    for j in range(batch_number):
                        Zrep[(nrep_batch * j):(nrep_batch * j + nrep_batch), :,
                             dn] = (results[dn * batch_number + j])
            ## simulating marginal likelihood of data (up to a constant)
            ##need save the seed to make reproducable.
            if sys.platform == "linux" or sys.platform == "linux2":
                large = 100000000000
            elif sys.platform == "darwin":
                large = 4294967295
            SEED2 = np.random.random_integers(0, large, size=nsamp)
            log_marg_data = np.empty(nsamp)
            for dn in range(nsamp):
                log_marg_data[dn] = mh_sim_log_marginal_data((
                    dn, dz, dataparamX0, dataparamt0, Zrep, CEN, SEED2[dn]))
            ##calculating emperical expectations
            EZ, EZZtn = E_step_MH(Zrep)
            if not CEN:
                [tE, (datatype, thetat0, sparse)] = dataparamt0
                wt0, Lam0 = thetat0
                (EexpwtZ, EZexpwtZ, EZZtexpwtZ) = E_step_MH_Cox(Zrep, wt0)
                EexpwtcZ = False
            elif CEN:
                [[tE, (datatypet, thetat0, sparset)],
                 [Delta, (datatypec, thetac0, sparsec)]] = dataparamt0
                wt0, Lam0 = thetat0
                (EexpwtZ, EZexpwtZ, EZZtexpwtZ) = E_step_MH_Cox(Zrep, wt0)
                if datatypec == 'cind':
                    Lamc0 = thetac0
                    EexpwtcZ = False
                elif datatypec == 'cindIC':
                    wtc0, Lamc0 = thetac0
                    (EexpwtcZ, EZexpwtcZ, EZZtexpwtcZ) = E_step_MH_Cox(Zrep,
                                                                       wtc0)
                    ## recording parameters
            qq = jointlogprob(dataparamX0, dataparamt0, EZ, EZZtn, EexpwtZ,
                              EexpwtcZ, CEN)
            q[i, :] = np.array([qq, i])
            Ldata[i, :] = np.array([np.sum(log_marg_data), i])
            #M - step
        dataparamXp = []
        for k, datapara in enumerate(dataparamX0):
            [X, (datatype, theta0, sparse)] = datapara
            if datatype == 'normal':
                Wx0, Mux0, Psix0 = theta0
                (Wxp, Psixp) = M_step_Norm(Wx0, Psix0, X, EZ, EZZtn, sparse)
                dataparamXp.append([X, (datatype, (Wxp, Mux0, Psixp), sparse)])
                ## no update for mean
            elif datatype == 'binom':
                Wx0, Mux0, xi0, b = theta0
                alpha0 = np.zeros((1, nsamp))
                (Wxp, Muxp, xip, alphap) = M_step_BIN(
                    Wx0, Mux0, xi0, alpha0, b, X, EZ, EZZtn, sparse, datatype)
                ##no update for alpha0
                dataparamXp.append([X, (datatype, (Wxp, Muxp, xip, b), sparse)
                                    ])
            elif datatype == 'multinom':
                Wx0, Mux0, xi0, alpha0, b = theta0
                (Wxp, Muxp, xip, alphap) = M_step_BIN(
                    Wx0, Mux0, xi0, alpha0, b, X, EZ, EZZtn, sparse, datatype)
                dataparamXp.append([X, (datatype, (Wxp, Muxp, xip, alphap, b),
                                        sparse)])
            else:
                print('unknown datatype')
        if dataparamt0 == 0:
#             print('Mstep: no time leg')
            dataparamtp = dataparamt0
            EexpwtpZ, EexpwtcpZ, EexpwtZ, EexpwtcZ = False, False, False, False
        else:
            if not CEN:
                [tE, (datatype, thetat0, sparse)] = dataparamt0
                #wt0, Lam0 = thetat0
                ##do update
                wtp, Lamp = M_step_NR(wt0, Lam0, tE, np.ones((1, nsamp)), EZ,
                                      EexpwtZ, EZexpwtZ, EZZtexpwtZ, step)
                thetatp = (wtp, Lamp)
                dataparamtp = [tE, (datatype, thetatp, sparse)]
                (EexpwtpZ, EZexpwtpZ, EZZtexpwtpZ) = E_step_MH_Cox(Zrep, wtp)
                EexpwtcpZ = False
            elif CEN:
                #[[tE, (datatypet, thetat0, sparset)],
                # [Delta, (datatypec, thetac0, sparsec)]] = dataparamt0
                #wt0, Lam0 = thetat0
                #print(wt0, Lam0, tE, Delta)
                (wtp, Lamp) = M_step_NR(wt0, Lam0, tE, Delta, EZ, EexpwtZ,
                                        EZexpwtZ, EZZtexpwtZ, step)
                thetatp = (wtp, Lamp)
                (EexpwtpZ, EZexpwtpZ, EZZtexpwtpZ) = E_step_MH_Cox(Zrep, wtp)
                Deltac = np.ones(Delta.shape) - Delta
                if datatypec == 'cind':
                    #Lamcp = thetac0
                    ## there are no updates past initialization for this case.
                    dataparamtp = [[tE, (datatypet, thetatp, sparset)],
                                   [Delta, (datatypec, thetac0, sparsec)]]
                    EexpwtcpZ = False
                elif datatypec == 'cindIC':
                    #wtc0, Lamc0 = thetac0
                    (wtcp, Lamcp) = M_step_NR(wtc0, Lamc0, tE, Deltac, EZ,
                                              EexpwtcZ, EZexpwtcZ, EZZtexpwtcZ,
                                              step)
                    thetacp = (wtcp, Lamcp)
                    dataparamtp = [[tE, (datatypet, thetatp, sparset)],
                                   [Delta, (datatypec, thetacp, sparsec)]]
                    (EexpwtcpZ, EZexpwtcpZ, EZZtexpwtcpZ) = E_step_MH_Cox(Zrep,
                                                                          wtcp)
        ### checking that Q increases
        ##BUG!  EexpwtZ, EexpwtcZ need to be different in the "p" call!!
        qqp = jointlogprob(dataparamXp, dataparamtp, EZ, EZZtn, EexpwtpZ,
                           EexpwtcpZ, CEN)
        if (qqp < qq):
            print("didn't maximize! trying smaller step sizes Newton -" +
                  " Raphson updates on the time arms.")
            #print("Psis", scipy.linalg.svd(Psixp)[1], scipy.linalg.svd(Psix0)[1])
            #print(" - Lam * np.exp(wt.dot(Z)).dot(tE.T)",
            #  - Lamp * EexpwtZ.dot(tE.T), - Lam0 * EexpwtZ.dot(tE.T))
            for j in range(10, 110, 10):
                print(j)
                if dataparamt0 == 0:
                    print('Mstep: no time leg')
                    dataparamtp = dataparamt0
                else:
                    stepj = step / j
                    if not CEN:
                        [tE, (datatype, thetat0, sparse)] = dataparamt0
                        #wt0, Lam0 = thetat0
                        wtp, Lamp = M_step_NR(wt0, Lam0, tE,
                                              np.ones((1, nsamp)), EZ, EexpwtZ,
                                              EZexpwtZ, EZZtexpwtZ, stepj)
                        thetatp = (wtp, Lamp)
                        dataparamtp = [tE, (datatype, thetatp, sparse)]
                        (EexpwtpZ, EZexpwtpZ, EZZtexpwtpZ) = E_step_MH_Cox(
                            Zrep, wtp)
                        EexpwtcpZ = False
                    elif CEN:
                        [[tE, (datatypet, thetat0, sparset)],
                         [Delta, (datatypec, thetac0, sparsec)]] = dataparamt0
                        print('sumdelta', np.sum(Delta))
                        #wt0, Lam0 = thetat0
                        (wtp, Lamp) = M_step_NR(wt0, Lam0, tE, Delta, EZ,
                                                EexpwtZ, EZexpwtZ, EZZtexpwtZ,
                                                stepj)
                        thetatp = (wtp, Lamp)
                        (EexpwtpZ, EZexpwtpZ, EZZtexpwtpZ) = E_step_MH_Cox(
                            Zrep, wtp)
                        #Deltac = np.ones(Delta.shape) - Delta
                        if datatypec == 'cind':
                            #Lamcp = thetac0
                            ## there are no updates past initialization for this case.
                            dataparamtp = [[tE, (datatypet, thetatp, sparset)],
                                           [Delta, (datatypec, thetac0,
                                                    sparsec)]]
                            EexpwtcpZ = False
                        elif datatypec == 'cindIC':
                            #wtc0, Lamc0 = thetac0
                            (wtcp, Lamcp) = M_step_NR(wtc0, Lamc0, tE, Deltac,
                                                      EZ, EexpwtcZ, EZexpwtcZ,
                                                      EZZtexpwtcZ, stepj)
                            thetacp = (wtcp, Lamcp)
                            dataparamtp = [[tE, (datatypet, thetatp, sparset)],
                                           [Delta, (datatypec, thetacp,
                                                    sparsec)]]
                            (EexpwtcpZ, EZexpwtcpZ,
                             EZZtexpwtcpZ) = E_step_MH_Cox(Zrep, wtcp)
                ##BUG!  EexpwtZ, EexpwtcZ need to be different in the "p" call!!
                qqp = jointlogprob(dataparamXp, dataparamtp, EZ, EZZtn,
                                   EexpwtpZ, EexpwtcpZ, CEN)
                if (qqp > qq):
                    print('yay maximized')
                    break
                else:
                    continue
            ##BUG!  EexpwtZ, EexpwtcZ need to be different in the "p" call!!
            if (qqp < qq):
                print('did not manage to maximize')
                return (dataparamXp, dataparamtp, dataparamX0, dataparamt0, EZ,
                        EZZtn, EexpwtZ, EexpwtcZ, EexpwtpZ, EexpwtcpZ, q, Ldata, i)
        if np.isnan(qqp):
            return (dataparamXp, dataparamtp, dataparamX0, dataparamt0, EZ,
                    EZZtn, EexpwtZ, EexpwtcZ, q, Ldata, i)
        ##here
        maxdiff = check_convergence(dataparamXp, dataparamtp, dataparamX0,
                                    dataparamt0, CEN)
        print(maxdiff)
        if maxdiff < convgap:
            print('resassigning and ending, abs(diff) less than convgap',
                  convgap, 'iter', i)
            dataparamX0 = np.copy(dataparamXp)
            dataparamt0 = np.copy(dataparamtp)
            q = q[0:i + 1]
            Ldata = Ldata[0:i + 1]
            break
        else:
            dataparamX0 = np.copy(dataparamXp)
            dataparamt0 = np.copy(dataparamtp)
    ##one more e-step to save E[z|x, t]
    ##
    if not os.path.exists(plotloc + 'expected_values/'):
        os.makedirs(plotloc + 'expected_values/')
#     EZ = e_step(dataparamX0, dataparamt0, dz, nsamp, mh_params, CEN, plotloc + 'expected_values/') # don't need to do another round
    #expsurvtime() saves E[z|x], and E[t|x]
    if dataparamt0 == 0:
        print('no time prediction')
#     else:
#         (texp, var) = expsurvtime(dataparamX0, dataparamt0, plotloc + 'expected_values/')
    if not os.path.exists(plotloc + 'learned_parameters/'):
        os.makedirs(plotloc + 'learned_parameters/')
#     save_params(dataparamX0, dataparamt0, Xmeans, dz, niter, mh_params, Ldata, CEN, plotloc + 'learned_parameters/')    
    return (dataparamX0, dataparamt0, Xmeans, q, Ldata, EZ) 


## could do more encapsulation!
def e_step(dataparamX0, dataparamt0, dz, nsamp, mh_params, CEN, plotloc):
    """
    this performs the e-step and saves E[Z|x] for no time and E[Z|x, t, Delta] with time.
    """
    if dataparamt0 == 0:
        print('E - step: no time leg, estep is analytical ' +
              '(under variational approximation)')
        EZ, EZZtn = E_step_no_surv(dataparamX0, dz)
        # f = open(plotloc + 'EZ_given_x.py', 'w')
        # pickle.dump(EZ, f)
        # f.close()
#         pd.DataFrame(EZ).to_csv(plotloc + 'EZ_given_x.csv')
    else:
        ##creating samples for empirical expectations.
        #Zrep = MH_sim_Z_given_data((dz, dataparamX0, dataparamt0,
        #nrep, CEN, burn = 1000))
        batch_number, nrep_batch, burn, MULTIPRO, PRE_SEED = mh_params
        nrep = batch_number * nrep_batch
        Zrep = np.empty((nrep, dz, nsamp))
        if not MULTIPRO:
            for j in range(0, batch_number):
#                 Zrepj = MH_sim_Z_given_data((dz, dataparamX0, dataparamt0,
#                                              nrep_batch, CEN, burn))
                raise NotImplementedError("Don't have MH_sim_Z_given_data")
                Zrepj = None
                Zrep[(nrep_batch * j):(nrep_batch * j + nrep_batch
                                       ), :, :] = Zrepj
        elif MULTIPRO:
            if sys.platform == "linux" or sys.platform == "linux2":
                numThreads = 50
            elif sys.platform == "darwin":
                numThreads = 4
            if not PRE_SEED:
                if sys.platform == "linux" or sys.platform == "linux2":
                    large = 100000000000
                elif sys.platform == "darwin":
                    large = 4294967295
                SEED = np.random.random_integers(
                    0,
                    large,
                    size=(nsamp * batch_number))
                assert np.unique(SEED).shape == SEED.shape
                f = open(plotloc + 'seed_EZ_given_x_t_Delta.py', 'w')
                pickle.dump(SEED, f)
                f.close()
            else:
                f = open(plotloc + 'seed_EZ_given_x_t_Delta.py', 'r')
                SEED = pickle.load(f)
                f.close()
            pool = multiprocessing.Pool(numThreads)
            results = pool.map_async(MH_sim_Z_given_data_p,
                                     [(dn, dz, dataparamX0, dataparamt0,
                                       nrep_batch, CEN, burn,
                                       SEED[dn * batch_number + j])
                                      for dn in range(nsamp)
                                      for j in range(batch_number)])
            # set the pool to work
            pool.close()
            # party's over, kids
            pool.join()
            # wait for all tasks to finish
            results = results.get()
            for dn in range(nsamp):
                for j in range(batch_number):
                    Zrep[(nrep_batch * j):(nrep_batch * j + nrep_batch), :,
                         dn] = (results[dn * batch_number + j])
        ##calculating emperical expectations
        EZ, EZZtn = E_step_MH(Zrep)
        # f = open(plotloc + 'EZ_given_x_t_Delta.py', 'w')
        # pickle.dump(EZ, f)
        # f.close()
        pd.DataFrame(EZ).to_csv(plotloc + 'EZ_given_x_t_Delta.csv')
    return pd.DataFrame(EZ)


def save_params(dataparamX0, dataparamt0, Xmeans, dz, niter, mh_params, Ldata, CEN, plotloc):
    ##consider instead http://docs.scipy.org/doc/numpy-1.10.0/reference/generated/numpy.savetxt.html#numpy.savetxt
    ## numpy.savetxt ?
    with open(plotloc + 'EM_log.txt', 'w') as g:
        g.write('time of run' + ',' + str(time()) + '\n')
        g.write('plotloc' + ',' + str(plotloc) + '\n')
        g.write('dz' + ',' + str(dz) + '\n')
        g.write('number of iterations' + ',' + str(niter) + '\n')
        g.write('metropolis hastings parameters' + ',' + str(mh_params) + '\n')
        g.write('likelihood of data' + ',' + str(Ldata) + '\n' )
        for k, datapara in enumerate(dataparamX0):
            [X, (datatype, theta, sparse)] = datapara
            g.write(str(k) + ',' + str(X.shape) + ',' + datatype + ',' + str(sparse) + '\n')
            # with open(plotloc + 'sparse_' + str(k) + '.py', 'w') as f:
            #     pickle.dump(sparse, f)
            pd.DataFrame(sparse).to_csv(plotloc + 'sparse_' + str(k) + '.csv')
            if datatype == 'normal':
                Wx, Mux, Psix = theta
                if sum(Mux) == 0:
                    #Mux = Xmeans[k]
                    Mux = None
                theta = (Wx, Mux, Psix)
                vnames = ['Wx', 'Mux', 'Psix']
                for j, item in enumerate(theta):
                    # with open(plotloc + vnames[j] + '_' + str(k) + '.py', 'w') as f:
                    #     pickle.dump(item, f)
                    pd.DataFrame(item).to_csv(plotloc + vnames[j] + '_' + str(k) + '.csv')
                g.write(str(k) + ',' + 'num Wx elements =0, ' + str(np.sum(theta[0] == 0)) + '\n')
            elif datatype == 'binom':
                vnames = ['Wx', 'Mux', 'xi', 'b']
                for j, item in enumerate(theta):
                    # with open(plotloc + vnames[j] + '_' + str(k) + '.py', 'w') as f:
                    #     pickle.dump(item, f)
                    if type(item) != np.ndarray:
                        item = [item]
                    pd.DataFrame(item).to_csv(plotloc + vnames[j] + '_' + str(k) + '.csv')
                g.write(str(k) + ',' + 'num Wx elements =0, ' + str(np.sum(theta[0] == 0)) + '\n')   
            elif datatype == 'multinom':
                vnames = ['Wx', 'Mux', 'xi', 'alpha', 'b']
                for j, item in enumerate(theta):
                    # with open(plotloc + vnames[j] + '_' + str(k) + '.py', 'w') as f:
                    #     pickle.dump(item, f)
                    if type(item) != np.ndarray:
                        item = [item]
                    pd.DataFrame(item).to_csv(plotloc + vnames[j] + '_' + str(k) + '.csv')
                g.write(str(k) + ',' + 'num Wx elements =0, ' + str(np.sum(theta[0] == 0)) + '\n')
            else:
                print('unknown datatype')
        if dataparamt0 == 0:
            g.write('no time data' + '\n')
        else:
            if not CEN:
                g.write('time data, no censoring' + '\n')
                [tE, (datatype, thetat, sparse)] = dataparamt0
                vnames = ['wt', 'Lam']
                for j, item in enumerate(thetat):
                    # with open(plotloc + vnames[j] + '.py', 'w') as f:
                    #     pickle.dump(item, f)
                    pd.DataFrame(item).to_csv(plotloc + vnames[j] + '.csv') 
            elif CEN:
                [[tE, (datatypet, thetat, sparset)],
                 [Delta, (datatypec, thetac, sparsec)]] = dataparamt0
                vnames = ['wt', 'Lam']
                for j, item in enumerate(thetat):
                    # with open(plotloc + vnames[j] + '.py', 'w') as f:
                    #     pickle.dump(item, f)
                    pd.DataFrame(item).to_csv(plotloc + vnames[j] + '.csv') 
                if datatypec == 'cind':
                    g.write('time data, with non-informative censoring' + '\n')
                    vnames = ['Lamc']
                    for j, item in enumerate([thetac]):
                        # with open(plotloc + vnames[j] + '.py', 'w') as f:
                        #     pickle.dump(item, f)
                        pd.DataFrame([item]).to_csv(plotloc + vnames[j] + '.csv')
                elif datatypec == 'cindIC':
                    g.write('time data, with informative censoring' + '\n')
                    vnames = ['wtc', 'Lamc']
                    for j, item in enumerate(thetac):
                        # with open(plotloc + vnames[j] + '.py', 'w') as f:
                        #     pickle.dump(item, f)
                        pd.DataFrame(item).to_csv(plotloc + vnames[j] + '.csv')
