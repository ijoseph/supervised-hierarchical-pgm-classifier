# -*- coding: utf-8 -*- 
from __future__ import division

from Test.shannon_code.survival_funct_v1 import *

plotloc = ''
(nsamp, ntrain, niter, convgap, nrep, step) = (50, 1, 5, 0.0001, 100, 1)
(dx, dy, dz) = 10, 20, 2
sparse = [True, step, .75]
INIT = True
CEN = False

paramX = createparam(dx, dz, nsamp, 'normal', sparse) # paramXY = (datatype,  θ, sparse), where θ = (W, μ, Ψ)  for normal
paramY = createparam(dy, dz, nsamp, 'multinom', [False])
paramXY = [paramX, paramY]
paramt = False

batch_number = 5
nrep_batch = 20
burn = 200
MULTIPRO = True
PRE_SEED = False
mh_params = batch_number, nrep_batch, burn, MULTIPRO, PRE_SEED

dataparamX, dataparamt, Z = gen_data(nsamp, dz, paramXY, paramt, CEN) # dataparamX a list of 2-tuples (one for each data partition): (\x, (Wx, Mux, Psix)) for Normal 

##this fits the parameters of the model, using the samples.  the output is
##dataparamXp, dataparamtp, Xmeans, q, Ldata where the original data
## (minus means for normal data) + fitted params are in dataparamXp, dataparamtp.
##NOTE: right now the number of threads for 
##parallel processing is hardcoded into EM_all based on the platform. 
##4 threads for darwin
##30 for linux or linux2.

t = time()
test = EM_all(dataparamX, dataparamt, dz, niter, convgap, mh_params, step, CEN,
              INIT, plotloc)


dataparamXp, dataparamtp, Xmeans, q, Ldata = test
print(time() - t) / 60, 'min'