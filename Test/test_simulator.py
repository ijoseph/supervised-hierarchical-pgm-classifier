from unittest import TestCase
import Models.LatentFactorModels
import Simulators.Simulator
import logging
import sys
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


class TestSimulator(TestCase):

    def test_simulate(self):
        test_model = Models.LatentFactorModels.ThreeNodeGaussianModel(2, 3, 2, 2, 1)
        test_model.initializeParameters(warm_start=False)
        sim = Simulators.Simulator.Simulator(test_model)
        test_model = sim.simulate(N=10)

        self.assertEqual(test_model.dataTree.paramData.data.shape[0], 10)

        # Test the clearing of data.
        test_model = sim.simulate(N=10)
        self.assertEqual(test_model.dataTree.paramData.data.shape[0], 10)



