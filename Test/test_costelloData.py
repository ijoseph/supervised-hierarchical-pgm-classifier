from unittest import TestCase
import Data.Parsers
import logging

class TestCostelloData(TestCase):

    def setUp(self):
        self.cd = Data.Parsers.CostelloData()

    def test_parse_mutations(self):
        self.cd.parse_mutations("/Volumes/Costello_reworked/TMZ/Molecular/Somatic-Mutations/Final-Patient-Set/"
                                "All_prirec_ann_MA,proveansift.txt.1000.txt")

        self.assertEqual(self.cd.mutation_df.shape, (6,8))


    def test_create_mut_dfc(self):
        self.cd.parse_mutations("/Volumes/Costello_reworked/TMZ/Molecular/Somatic-Mutations/Final-Patient-Set/"
                                "All_prirec_ann_MA,proveansift.txt.1000.txt")

        self.cd.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        dfc = self.cd.get_mutation_dfc()

        logging.info(dfc.head(3))

        self.assertEquals(dfc.dependentDF.HM.dtype , bool)

        # No missing data
        self.assertEquals(dfc.independentDF.isnull().any().any(), 0)
        self.assertEquals(dfc.dependentDF.isnull().any().any(), 0)


    def test_create_meth_dfc(self):
        self.cd.parse_methylation(methylation_feather_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                                           "Final-Patient-Set/processed/all.feather",
                                  spec_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                            "Final-Patient-Set/2016-1202_LGGpri_idats_to_Isaac/"
                                            "2016-1202_LGGpri_idats_to_Isaac.txt", keep_top_n=500)


        self.cd.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        dfc = self.cd.get_methylation_dfc()
        self.assertEqual(dfc.independentDF.shape[0], 13)


    def test_meth_and_mut_dfc(self):
        self.cd.parse_methylation(methylation_feather_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                                           "Final-Patient-Set/processed/all.feather",
                                  spec_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                            "Final-Patient-Set/2016-1202_LGGpri_idats_to_Isaac/"
                                            "2016-1202_LGGpri_idats_to_Isaac.txt", keep_top_n=500)


        self.cd.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        self.cd.parse_mutations("/Volumes/Costello_reworked/TMZ/Molecular/Somatic-Mutations/Final-Patient-Set/"
                                "All_prirec_ann_MA,proveansift.txt.1000.txt", keep_top_n=500)


        dfc_both = self.cd.get_methylation_and_mutation_dfc()

        self.assertEqual(dfc_both.independentDF.shape, (3,507))


    def test_create_clinical_dfc(self):
        self.cd.parse_clinical("/Volumes/Costello_reworked/TMZ/Clinical/Complete-LGG-Overview.xlsx")

        self.cd.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        dfc = self.cd.get_clinical_dfc()
        dfc.dependentDF['HM'].value_counts()


    def test_all_dfc(self):
        self.cd.parse_mutations("/Volumes/Costello_reworked/TMZ/Molecular/Somatic-Mutations/Final-Patient-Set/"
                                "All_prirec_ann_MA,proveansift.txt.1000.txt")

        self.cd.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        self.cd.parse_methylation(methylation_feather_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                                           "Final-Patient-Set/processed/all.feather",
                                  spec_file="/Volumes/Costello_reworked/TMZ/Molecular/Methylation/"
                                            "Final-Patient-Set/2016-1202_LGGpri_idats_to_Isaac/"
                                            "2016-1202_LGGpri_idats_to_Isaac.txt", keep_top_n=500)

        self.cd.parse_clinical("/Volumes/Costello_reworked/TMZ/Clinical/Complete-LGG-Overview.xlsx")

        dfc = self.cd.get_methylation_and_mutation_and_clinical_dfc()

        self.assertEqual(dfc.independentDF.shape, (3,523))






























