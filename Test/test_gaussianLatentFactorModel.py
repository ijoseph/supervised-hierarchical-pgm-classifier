from unittest import TestCase
import Performance.classification.CrossValidation
import sklearn
import pandas as pd
import Models.LatentFactorModels
import Simulators.Simulator


class TestGaussianLatentFactorModel(TestCase):

    def setUp(self):
        (m, p_0, p_1, p_2, p_3) = (2, 3, 5, 5, 1)
        self.gen_model = Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
        _ = self.gen_model.initializeParameters(psiMean=2, wMean=20, warm_start=False)
        simulator = Simulators.Simulator.Simulator(self.gen_model)
        self.gen_model = simulator.simulate(10)


    def test_save_data(self):
        self.gen_model.save_data(directory="save_data_test")
