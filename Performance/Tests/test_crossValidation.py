# -*- coding: utf-8 -*-
if __name__ == '__main__':
    import os, sys
    base_dir = os.path.join(os.getcwd())
    sys.path.append(base_dir)


from unittest import TestCase
import unittest
import Performance.classification.CrossValidation
import Data.Parsers
import pandas as pd
import numpy as np
import sklearn.linear_model
import Models.LatentFactorModels
import Simulators.Simulator
import logging
import sys
# level=50 for nothing
logging.basicConfig(stream=sys.stderr, level=50)
import time
import seaborn as sns
import scipy.stats
import warnings
import datetime


class TestCrossValidation(TestCase):

    """Setup"""
    def setUp(self):
        np.random.seed(seed=2)


        self.setUp_small_N_small_p()
        self.setUp_large_N_small_p()
        self.setUp_large_N_small_p_problematic_cv_split()

    """ Small N, p """

    def setUp_small_N_small_p(self):
        dependent_df = pd.DataFrame({"class": [False, True, False, True, True, False],
                                     "sample_name": ["one", "two", "three", "four", "five", "six"]}). \
            set_index("sample_name")
        sample_df_1 = pd.DataFrame({"sample_name": list(dependent_df.index),
                                    "value1": [1.1, 1.2, 1.3, 1.4, 1.5, 1.6],
                                    "value2": [2.1, 2.2, 2.3, 2.4, 2.5, 2.7]})
        independent_df = pd.concat([df.set_index("sample_name") for df in [sample_df_1] * 2],
                                   axis=1)
        self.dfc = Performance.classification.CrossValidation.DataFrameCollection(independent_data=independent_df,
                                                                                  dependent_data=dependent_df)
        self.cv_logistic_regression = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=sklearn.linear_model.LogisticRegression(),
            dataFrameCollection=self.dfc, k=2)
        self.cv_three_node_model = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(2, 3, 2, 2, 1),
            dataFrameCollection=self.dfc, k=2)

    def test_cross_validate_small_N_small_p(self):
        # Logistic Regression
        self.cv_logistic_regression.cross_validate()
        self.cv_logistic_regression.output_results()

        # Three Node Model

        self.cv_three_node_model.cross_validate()
        self.cv_three_node_model.output_results()

    """ N >> p """
    def setUp_large_N_small_p(self):
        # Simualte data
        (m, p_0, p_1, p_2, p_3) = (2, 3, 5, 5, 1)
        gen_model = Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
        _ = gen_model.initializeParameters(psiMean=2, wMean=20, warm_start=False)
        simulator = Simulators.Simulator.Simulator(gen_model)
        gen_model = simulator.simulate(100)

        # Gather data
        generating_x = gen_model.getX()

        generating_y = gen_model.getY()


        dfc = Performance.classification.CrossValidation.DataFrameCollection(dependent_data=generating_y,
                                                                             independent_data=generating_x)

        self.cv_three_node_model_large = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
            dataFrameCollection=dfc, k=10)

        self.cv_logistic_regression_large = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=sklearn.linear_model.LogisticRegression(),
            dataFrameCollection=dfc, k=10)

        self.cv_logistic_regression_large_auto_tune = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=sklearn.linear_model.LogisticRegression(C=17),
            dataFrameCollection=dfc, k=10, auto_tune=True)

    def setUp_large_N_small_p_problematic_cv_split(self):
        (m, p_0, p_1, p_2, p_3) = (2, 3, 5, 5, 1)
        self.gen_model_problematic_cv_split = Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
        _ = self.gen_model_problematic_cv_split.initializeParameters(psiMean=2, wMean=20, warm_start=False)
        simulator = Simulators.Simulator.Simulator(self.gen_model_problematic_cv_split)
        self.gen_model_problematic_cv_split = simulator.simulate(20)

        dfc = Performance.classification.CrossValidation.DataFrameCollection(
            dependent_data=self.gen_model_problematic_cv_split.getY(),
            independent_data=self.gen_model_problematic_cv_split.getX())

        dfc_non_binarized = Performance.classification.CrossValidation.DataFrameCollection(
            dependent_data=self.gen_model_problematic_cv_split.getY(),
            independent_data=self.gen_model_problematic_cv_split.getX(), binarize_dependent=False)

        self.cv_problematic = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
            dataFrameCollection=dfc, k=10)

        self.cv_problematic_non_binarized = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
            dataFrameCollection=dfc_non_binarized, k=10)

    def test_cross_validate_large_N_small_p(self):
        """
        Test of that which is being used for Jupyter-Notebook
        :return:
        """

        self.cv_three_node_model_large.cross_validate()
        self.cv_three_node_model_large.output_results()

    def test_cross_validate_large_N_small_p_problematic_cv_split(self):
        """
        See why model is going insane on one CV fold
        :return:
        """
        np.random.seed(seed=2)
        self.cv_problematic.cross_validate()
        self.cv_problematic.output_results()
        import seaborn as sns
        import matplotlib.pyplot as plt

        # Only if INFO,  DEBUG but not WARNING, ERROR, CRITICAL
        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            ax = sns.regplot(x=self.gen_model_problematic_cv_split.getY(),
                             y=np.array(self.cv_problematic.cv_results.proba_list))
            ax.set(xlabel="$x_3$", ylabel="$\hat{x}_3'$")
            plt.show()

    def test_cross_validate_large_N_small_p_problematic_cv_split_non_binarized(self):
        """
        See why model is going insane on one CV fold
        :return:
        """
        np.random.seed(seed=2)
        self.cv_problematic_non_binarized.cross_validate(binarize_output=False)
        self.cv_problematic_non_binarized.output_results()

        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            import seaborn as sns
            import matplotlib.pyplot as plt

            # Pathological fold: plot predicted versus actual on validate

            ax = sns.regplot(x=self.gen_model_problematic_cv_split.getY(),
                             y=np.array(self.cv_problematic_non_binarized.cv_results.proba_list))
            ax.set(xlabel="y", ylabel="$\hat{y}'$")
            ax.set_title("Overall: All validation-based predictions")
            Performance.classification.CrossValidation.CrossValidation.add_identity(ax)
            plt.show()

    def test_grid_search(self):

        fit_grid = self.cv_logistic_regression_large.grid_search()
        sns.plt.show()

        print "best score {0}, best parameters {1}".format(fit_grid.best_score_, fit_grid.best_params_)

    def test_auto_tune(self):
        self.assertNotEqual(self.cv_logistic_regression_large_auto_tune.predictorClass.get_params()['C'], 17)


class TimeTests(TestCase):

    """ N << p """
    def test_sim_large_N_large_p(self):
        """
        Set up large N, large p by simulation
        """

        (m, p_0, p_2, p_3) = (2, 3, 5, 1)
        p_1_dims = [100, 200, 500]

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="Polyfit may be poorly conditioned")
            def dimension_simulation_runtime_tests(m, p_0, p_1_dims, p_2, p_3, n=10):
                times = []
                for p_1 in p_1_dims:
                    start_time = time.time()
                    self._simulate_from_three_node_model(m, p_0, p_1, p_2, p_3,n=n)
                    times += [time.time() - start_time]
                    logging.warning("Elapsed: {0} seconds for {1} dim".format(time.time() - start_time, p_1))
                if len(times) > 1:
                    sns.jointplot(x=np.array(p_1_dims), y=np.array(times), kind="reg", order=2) \
                        .set_axis_labels(xlabel="p_1", ylabel="time(s)")
                    slope, intercept, r_value, _, _ = scipy.stats.linregress(x=np.array(p_1_dims), y=np.array(times))
                    logging.warning(
                        "time (seconds) = {0} (dimension of p_1) + {1}; r^2 = {2}".format(slope, intercept, r_value))
                    logging.warning("⇒ Simulation of p_1 = 100,000 will take {0:.0f} seconds for n = {1}"
                                    .format(slope * 1e5 + intercept, n))

            dimension_simulation_runtime_tests(m, p_0, p_1_dims, p_2, p_3)
            ns = [10, 50, 100, 500]

            def n_simulation_runtime_tests(m, p_0, p_1, p_2, p_3, ns):
                times = []
                for n in ns:
                    start_time = time.time()
                    self._simulate_from_three_node_model(m,p_0, p_1, p_2, p_3, n=n)
                    times += [time.time() - start_time]
                    logging.warning("Elapsed: {0} seconds for n = {1}, p = {2} ".format(time.time() - start_time, n, p_1))
                sns.plt.figure()
                sns.jointplot(x=np.array(ns), y=np.array(times), kind="reg", order=2)\
                    .set_axis_labels(xlabel="N", ylabel="time (s)")


                slope, intercept, r_value, _, _ = scipy.stats.linregress(x=np.array(ns), y=np.array(times))
                logging.info("time (seconds) = {0} (n) + {1}; r^2 = {2}".format(slope, intercept, r_value))
                logging.info("⇒ Simulation of n= 1,000 will take {0:.0f} seconds for p = {1} ".format(slope * 1e3 + intercept, p_1))

            n_simulation_runtime_tests(m, p_0, 10, p_2, p_3, ns=ns)

            if logging.getLogger().getEffectiveLevel() <= logging.INFO:
                sns.plt.show()

    def test_fit_large_N_large_p(self):
        """ How long does fitting (versus simulating) take?"""

        def dimension_fit_runtime_tests(m, p_0, p_1_dims, p_2, p_3, n=100):
            times = []
            for p_1 in p_1_dims:
                gen_model = self._simulate_from_three_node_model(m, p_0, p_1, p_2, p_3, n=n)
                fit_model = Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
                start = time.time()
                fit_model.fit(X=gen_model.getX(),y=gen_model.getY())
                times += [time.time() - start]


            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", message="Polyfit may be poorly conditioned")
                sns.jointplot(x=np.array(p_1_dims), y=np.array(times), kind="reg", order=2) \
                    .set_axis_labels(xlabel="p_1", ylabel="time(s)")

                sns.plt.title("Fitting times")
                slope, intercept, r_value, _, _ = scipy.stats.linregress(x=np.array(p_1_dims), y=np.array(times))
                logging.warning(
                    "time (seconds) = {0} (dimension of p_1) + {1}; r^2 = {2}".format(slope, intercept, r_value))
                logging.warning("⇒ Fitting: p_1 = 100,000 will take {0:.0f} for n = {1}"
                                .format(slope * 1e5 + intercept, n))

                # sns.plt.show()

        (m, p_0, p_2, p_3) = (2, 3, 5, 1)
        p_1_dims = [100, 200, 500, 1000]

        dimension_fit_runtime_tests(m, p_0, p_1_dims, p_2, p_3)

    def test_cv_large_N_large_p(self):
        (m, p_0, p_2, p_3) = (2, 3, 5, 1)
        p_1_dims = [100, 200, 500, 1000]

        def dimension_cv_runtime_tests(m, p_0, p_1_dims, p_2, p_3, n=100):
            times = []
            for p_1 in p_1_dims:
                gen_model = self._simulate_from_three_node_model(m, p_0, p_1, p_2, p_3, n=n)


                start = time.time()
                dfc = Performance.classification.CrossValidation.DataFrameCollection(independent_data=gen_model.getX(),
                                                                                     dependent_data=gen_model.getY())

                cv_mfaa = Performance.classification.CrossValidation.CrossValidation(
                    predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
                    dataFrameCollection=dfc, k=10)

                cv_mfaa.cross_validate()

                times += [time.time() - start]

            with warnings.catch_warnings():
                warnings.filterwarnings("ignore", message="Polyfit may be poorly conditioned")
                # sns.jointplot(x=np.array(p_1_dims), y=np.array(times), kind="reg", order=2) \
                #     .set_axis_labels(xlabel="p_1", ylabel="time(s)")

                # sns.plt.title("Fitting times")
                slope, intercept, r_value, _, _ = scipy.stats.linregress(x=np.array(p_1_dims), y=np.array(times))
                c_2, c_1, c_0 = np.polyfit(p_1_dims, times, deg=2)

                logging.warning(
                    "time (seconds) = {0} (dimension of p_1) + {1}; r^2 = {2}".format(slope, intercept, r_value))
                logging.warning("⇒ CV: p_1 = 100,000 will take {0} for n = {1}".format(datetime.timedelta(seconds=(c_2 * 1e5**2) + (c_1 * 1e5) + c_0),n))
                # sns.plt.show()


        dimension_cv_runtime_tests(m, p_0, p_1_dims, p_2, p_3)

    def _simulate_from_three_node_model(self, m, p_0, p_1, p_2, p_3, n=10):
        gen_model = Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3)
        logging.warning("Initializing parameters of large_N_large_p")
        _ = gen_model.initializeParameters(psiMean=2, wMean=20, warm_start=False)
        logging.warning("Simulating from large_N_large_p")
        simulator = Simulators.Simulator.Simulator(gen_model)
        gen_model = simulator.simulate(n)
        self.assertEqual(gen_model.dataTree.paramData.data.shape[0], n) # number of simulations correct

        # Show simulation data heatmap for INFO,  DEBUG but not WARNING, ERROR, CRITICAL
        if logging.getLogger().getEffectiveLevel() <= logging.DEBUG:
            ax = sns.heatmap(gen_model.getObservedData())
            ax.set_title("N = {0}, p = {1}".format(n, gen_model.observed_dim))
            sns.plt.show()

        return gen_model


class CGPTests(TestCase):
    def setUp(self):
        self.cgp_dfc = Performance.classification.CrossValidation.\
            DataFrameCollection.from_directory("/Users/ijoseph/Code/workspace/FactorAssociationAnalysisCancerGenomics/"
                                               "Jupyter-Notebooks/Prediction/Erlotinib")
        self.p = 100
        self.cgp_dfc_down_sampled = self.cgp_dfc.down_sample(self.p, random_state=55) # down_sample


    def test_cv_down_sampled(self):
        (m, p_0, p_3) = (2, 3, 1)
        # p_1, p_2 = mut_dfc.indIndices[0], mut_dfc.independentDF.shape[1] - sum(mut_dfc.indIndices)
        p_1, p_2 = self.p/2, self.p/2
        cv_mfaa = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
                                                                 dataFrameCollection=self.cgp_dfc_down_sampled,k =10)

        cv_mfaa.cross_validate()

        # cv_mfaa.output_results()
    #
    #
    # def test_cv_full(self):
    #     (m, p_0, p_3) = (2, 3, 1)
    #     p_1, p_2 = self.cgp_dfc.indIndices[0], self.cgp_dfc.independentDF.shape[1] - sum(self.cgp_dfc.indIndices)
    #
    #
    #     cv_mfaa = Performance.classification.CrossValidation.CrossValidation(
    #         predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(m, p_0, p_1, p_2, p_3),
    #         dataFrameCollection=self.cgp_dfc, k=10)
    #
    #     cv_mfaa.cross_validate()


class CostelloTests(TestCase):
    def setUp(self):
        self.mut_parser = Data.Parsers.CostelloData()
        self.mut_parser.parse_mutations(
            "/Volumes/Costello_reworked/TMZ/Molecular/Somatic-Mutations/Final-Patient-Set/"
            "All_prirec_ann_MA,proveansift.txt", keep_top_n=10)

        self.mut_parser.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        self.mut_dfc = self.mut_parser.get_mutation_dfc()


        self.clin_parser = Data.Parsers.CostelloData()
        self.clin_parser.parse_clinical("/Volumes/Costello_reworked/TMZ/Clinical/Complete-LGG-Overview.xlsx", keep_only_astro=True)
        self.clin_parser.parse_hypermutation_on_recurrence_info("/Volumes/Costello_reworked/TMZ/Outcome/which.patients.hm.txt")

        self.clin_dfc = self.clin_parser.get_clinical_dfc()


    def test_predict_mut(self):
        cv = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=sklearn.linear_model.LogisticRegression()
            , dataFrameCollection=self.mut_dfc
            , k=10)
        cv.cross_validate()
        cv.output_results()

    def test_predict_non_dep(self):
        cv = Performance.classification.CrossValidation.CrossValidationNonDeprecated(
            predictorClass=sklearn.linear_model.LogisticRegression()
            , dataFrameCollection=self.mut_dfc
            , k=10)

        cv.cross_validate()
        cv.output_results()

    def test_predict_clin(self):
        cv = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=sklearn.linear_model.LogisticRegression()
            , dataFrameCollection=self.clin_dfc
            , k=10)
        cv.cross_validate()
        cv.output_results()






if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(TimeTests("test_cv_large_N_large_p"))
    runner = unittest.TextTestRunner()
    runner.run(suite)

