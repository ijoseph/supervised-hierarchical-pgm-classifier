from unittest import TestCase

from Performance.classification.CrossValidation import *
import pandas as pd
from pandas.util.testing import assert_frame_equal


class TestDataFrameCollection(TestCase):

    def setUp(self):
        self.dfc = DataFrameCollection()

        self.dfc_with_stuff = DataFrameCollection(independent_data=
                                                  pd.DataFrame(np.random.randn(50, 4), columns=list('ABCD')),
                                                  dependent_data=pd.DataFrame(np.random.binomial(n=1,p=0.5, size=50)),
                                                  boundary_indices=2)

    def test_parseIndependent(self):

        # Make reference DataFrame
        sample_df_1 = pd.DataFrame({"sample_name":["one", "two"],
                                 "value1":[1.1,1.3],
                                  "value2":[1.2,1.4]}).set_index("sample_name")
        sample_df_2 = pd.DataFrame({"sample_name":["one", "two"],
                                   "value1":[2.1,2.3],
                                  "value2":[2.2,2.4]}).set_index("sample_name")
        sample_df = pd.concat([sample_df_1, sample_df_2],
                              axis = 1)

        # Test using files
        (self.dfc.independentDF, self.dfc.indIndices) = self.dfc.parseIndependent([file("test_ind_data.type_1.csv"),
                                                                                   file("test_ind_data.type_2.csv")])
        assert_frame_equal(self.dfc.independentDF, sample_df)

        # Test using filenames
        (self.dfc.independentDF, self.dfc.indIndices) = self.dfc.parseIndependent(
            ["test_ind_data.type_1.csv", "test_ind_data.type_2.csv"])
        assert_frame_equal(self.dfc.independentDF, sample_df)

        # Test using extant data.frames
        (self.dfc.independentDF, self.dfc.indIndices) = self.dfc.parseIndependent(
            [sample_df_1, sample_df_2])
        assert_frame_equal(self.dfc.independentDF, sample_df)

    def test_parseDependent(self):
        sample_df = pd.DataFrame({"class": [0, 1], "sample_name": ["zero_sample", "one_sample"]})
        sample_df = sample_df.set_index("sample_name")

        # Test using a file
        self.dfc.dependentDF = self.dfc.parseDependent(file("test_data.csv"), binarize=True)

        assert_frame_equal(sample_df, self.dfc.dependentDF)

        # Test using a df
        self.dfc.independentDF = self.dfc.parseIndependent(sample_df)
        assert_frame_equal(sample_df, self.dfc.dependentDF)

        # Test using filenames
        self.dfc.dependentDF = self.dfc.parseDependent("test_data.csv", binarize=True)
        assert_frame_equal(sample_df, self.dfc.dependentDF)

        # Test binarizing
        sample_df_float= pd.DataFrame({"class": [-3.2, 0], "sample_name": ["zero_sample", "one_sample"]}).\
            set_index("sample_name")

        self.dfc.dependentDF = self.dfc.parseDependent(sample_df_float, binarize=True)

        sample_df_binarized = pd.DataFrame({"class": [False, True], "sample_name": ["zero_sample", "one_sample"]}).\
            set_index("sample_name")

        assert_frame_equal(sample_df_binarized, self.dfc.dependentDF)

    def test_down_sample(self):
        new_dfc = self.dfc_with_stuff.down_sample(3)

    def test_save_data(self):
        self.dfc_with_stuff.save_data(directory="TestDFCSave")

    def test_create_from_folder(self):
        self.dfc_with_stuff.save_data(directory="TestDFCSave")
        new = DataFrameCollection.from_directory("TestDFCSave")
        self.assertTrue(self.dfc_with_stuff.independentDF.equals(new.independentDF))




















