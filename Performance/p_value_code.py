###
### Exerpt of code for (PCA + CCA) project by Shannon, Brielin, Nick, and Lior
### For Isaac's use
### Feb. 24 2015
###

from __future__ import division

import argparse
import multiprocessing
import sys
from time import time

import numpy
import scipy.linalg.blas


import numpy as np
import pandas as pd
import pandas.io as pio


###first are some functions.
def lrCCA( Ux, Uy, n1, n2, n3 ):
    '''
    Performs low rank CCA with dimensions n1, n2, n3. 
    '''
    UC, sC, VC = np.linalg.svd( Ux[:,0:n1].T.dot( Uy[:,0:n2] ), full_matrices=False ) #No nu,nv
    S1 = Ux[:,0:n1].dot( UC[:,0:n3] )
    S2 = Uy[:,0:n2].dot( VC.T[:,0:n3] ) #V contains RSVs as rows so need transpose
    return( S1, S2, sC[0:n3] )

def CCAVarExp( X, Ux, Uy, n1, n2, n3, nPerm):
    '''
    This does the actual FAA step
    '''
    sys.stderr.write( "Starting FAA step\n")
    tCCA_S1, _, _ = lrCCA( Ux, Uy, n1, n2, n3 )
    XTtCCA_S1= scipy.linalg.blas.dgemm(alpha=1.0, a = X.T, b = tCCA_S1.T, trans_b = True)
    trueVarExp= np.sum( np.multiply(XTtCCA_S1, XTtCCA_S1), axis=1 )/(X.shape[0]-1)
    permSVD = np.copy(Uy)
    tailCounts = np.zeros(X.shape[1])
    permM = np.zeros(X.shape[1])
    permM2 = np.zeros(X.shape[1])
#     permFstat = np.zeros(nPerm)
    t = time()
    for i in range(nPerm):
        if i%1000 == 0: sys.stderr.write( "Permutation:" + str(i) + "\n")
        permSVD = np.random.permutation(Uy)
        pCCA_S1, _, _ = lrCCA(Ux, permSVD, n1, n2, n3)
        XTpCCA_S1= scipy.linalg.blas.dgemm(alpha=1.0, a = X.T, b = pCCA_S1.T, trans_b = True)
        permVarExp =  np.sum( np.multiply(XTpCCA_S1, XTpCCA_S1), axis=1 )/(X.shape[0]-1)
        permM += permVarExp
        permM2 += permVarExp**2
        tailCounts += (permVarExp > trueVarExp)
    sys.stderr.write( "time elapsed (CCAVarExp): " + str(time() - t) + "\n")
    permM = permM/nPerm
    permM2 = permM2/nPerm
    permSD = np.sqrt( permM2 - permM**2 )
    pVal = tailCounts/nPerm
    sys.stderr.write(str( permSD.shape) + "\n")
    ZScore = (trueVarExp - permM)/permSD
    res = pd.DataFrame(np.vstack((trueVarExp,pVal,ZScore,permM,permSD)).T, index = pd.DataFrame(X).columns, columns=('Var Exp','p-Value','Z Score','Perm Mean','Perm SD'))
    return res

def CCAVarExpParallel( (X, Ux, Uy, n1, n2, n3, nPerm)):
    '''
    I suppose this is the same as the above but in parallel?
    '''
    
    tCCA_S1, _, _ = lrCCA( Ux, Uy, n1, n2, n3 )
    XTtCCA_S1= scipy.linalg.blas.dgemm(alpha=1.0, a = X.T, b = tCCA_S1.T, trans_b = True)
    trueVarExp= np.sum( np.multiply(XTtCCA_S1, XTtCCA_S1), axis=1 )/(X.shape[0]-1)
    permSVD = np.copy(Uy)
    tailCounts = np.zeros(X.shape[1])
    permM = np.zeros(X.shape[1])
    permM2 = np.zeros(X.shape[1])
    _ = np.zeros(nPerm)
    t = time()
    for i in range(nPerm):
        if i%1000 == 0: sys.stderr.write( "Permutation:"+str(i) + "\n")
        permSVD = np.random.permutation(Uy)
        pCCA_S1, _, _ = lrCCA(Ux, permSVD, n1, n2, n3)
        XTpCCA_S1= scipy.linalg.blas.dgemm(alpha=1.0, a = X.T, b = pCCA_S1.T, trans_b = True)
        permVarExp =  np.sum( np.multiply(XTpCCA_S1, XTpCCA_S1), axis=1 )/(X.shape[0]-1)
        permM += permVarExp
        permM2 += permVarExp**2
        tailCounts += (permVarExp > trueVarExp)
    sys.stderr.write( str(time() - t) + "\n")
    res = pd.DataFrame(np.vstack((tailCounts,permM,permM2)).T, index = X.columns, columns=('tailcounts','permM','permM2'))
    return res



###here is the code.

def runPermutaiton(X, X_left_singular_vectors, Y_left_singular_vectors, n1, n2, n3, numThreads, nPerms =1000):
    '''
    ijoseph created 
    '''
#     X = None   ###  this is a pandas data frame. rows = samples, columns= gene expression values.  
#     X_left_singular_vectors = None ### left singular vectors of X
#     Y_left_singular_vectors = None  #### left singular vectors of SNPs
#     n1 = None  ### number of X_left_singular_vectors columns to use in CCA
#     n2 = None  ### number of Y_left_singular_vectors columns to use in CCA
#     n3 = None  ###  dimension of CCA
    if numThreads == 1:
        parallel = False
    else:
        parallel = True
    
    ## Find permutation p-values for variance explained by each gene in the CCA projection.
    if not parallel:
        t = time();
        result = CCAVarExp( X, X_left_singular_vectors, Y_left_singular_vectors, n1, n2, n3, nPerm=nPerms)
        sys.stderr.write(str( time() -t) + "\n")
    else:
        ## if ypu need to do it in parallel, you can use this code instead of the line above.
        nPermtot= 10e7
        numThreads=30
        
        expGeneResList=[]
        for i in range(numThreads):
            if i!= numThreads-1:
                expGeneResList.append(( X, X_left_singular_vectors, Y_left_singular_vectors, n1, n2, n3,  int(nPermtot/numThreads)))
            else:
                expGeneResList.append(( X, X_left_singular_vectors, Y_left_singular_vectors, n1, n2, n3, int(nPermtot -int(nPermtot/numThreads)*(numThreads-1))))
        
        t = time();
        pool = multiprocessing.Pool(numThreads)
        results=pool.map_async(CCAVarExpParallel , [i for i in expGeneResList])
        pool.close()
        pool.join()
        expGeneResResults= results.get()
        sys.stderr.write( "time elapsed: "  + str(time() -t) + "\n")    
        
        for i in range(numThreads):
            if i==0:
                expGeneResTot=expGeneResResults[i]
            else:
                expGeneResTot=expGeneResTot+ expGeneResResults[i]
    
            result = CCAVarExp( X, X_left_singular_vectors, Y_left_singular_vectors, n1, n2, n3, nPerm=1) # 
            result['p-Value']= expGeneResTot['tailcounts']/nPermtot
            result['Perm Mean']= expGeneResTot['permM']/nPermtot
            permM2= expGeneResTot['permM2']/nPermtot
            result['Perm SD']= np.sqrt( permM2 - result['Perm Mean']**2 )
            result['Z Score']=(result['Var Exp'] - result['Perm Mean'])/result['Perm SD']
            ###end alternative code.
    
    
    ## Determine significance cutoffs for naive Bonferonni, Benjamini-Hochberg and B-H-Yuketeli  false discovery rates.
    expGenePV = result.sort('p-Value')
    alpha = 0.05
    m = expGenePV.shape[0]
    cm= sum([1/(i+1) for i in range(m)])
    BFC = alpha/m
    for i,p in enumerate(expGenePV['p-Value']):
        if p > ((i+1)*alpha)/m: break
    BHC = (i*alpha)/m # Want the previous one
    for i,p in enumerate(expGenePV['p-Value']):
        if p > ((i+1)*alpha)/(m*cm): break
    BHYC = (i*alpha)/m # Want the previous one
    
    ## Fid significant transcripts for each of the above significance cutoffs
    sigBFC = expGenePV[ expGenePV['p-Value'] < BFC ]
    sigBHC = expGenePV[ expGenePV['p-Value'] < BHC ]
    sigBHYC = expGenePV[ expGenePV['p-Value'] < BHYC ]
    
    return (sigBFC, sigBHC, sigBHYC)

def normalizeData(Xorig,Yorig):
    '''
    Normalize the input matrices.
    '''
    # Scale Xorig matrix-wide
    
    # Leave Y alone. Leave it alone!

def getLeftSingularVectors(inputMatrix):
    '''
    use Scipy (?) to get left singular vectors
    '''
    if type(inputMatrix) == pd.DataFrame:
        inputMatrix = inputMatrix.as_matrix()
    (U, _ , _) = numpy.linalg.svd(inputMatrix)

    return U

def readCSVFile(inputFile):
    '''
    Reads input matrix as a .tsv file and returns a pandas DataFrame that file
    '''
    sys.stderr.write( "Reading file:" + inputFile + "\n")
    return pio.parsers.read_csv(inputFile).as_matrix()

def readHDF5File(inputFile, name):
    sys.stderr.write( "Reading file:" + inputFile + "/" + name + "\n")
    store = pio.pytables.HDFStore(inputFile)
    return pd.DataFrame(store[name])
    

def writeSigXFeatures(dataFrame, output, name):
    output.write(name + "\n" + "-"*100 + "\n")    
    dataFrame.to_csv(output)    
    '''
    Write the data.frames of significance feature names. 
    '''

def main():
    parser = argparse.ArgumentParser(description = """
    Converted code from Shannon McCurddy 2015-03-03. This supposedly does FAA approximation, and also can do
    permutation significance analysis.    
    .""") 
    parser.add_argument("--X", type=str, required=True, help="X matrix in format specified by --fileType")
    parser.add_argument("--Y", type=str, required=True, help="Y matrix in format specified by --fileType")
    parser.add_argument("--fileType", type=str, required=False, default="csv", help="file type (csv or hdf5)")
    parser.add_argument("--n1", type=int, required=False, default=3, help="X PCA dimension")
    parser.add_argument("--n2", type=int, required=False, default=3, help="Y PCA dimension")
    parser.add_argument("--n3", type=int, required=False, default=2, help="CCA dimension")
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "File to which to output information")    
    parser.add_argument('-p', '--numThreads', type=int, default= 1, help = "Number of threads to run [1]")
    parser.add_argument('-p', '--numPermutations', type=int, default= 2, help = "Number of threads to run [1]")
#     mut_parser.add_argument('--tempFolder', default="temp/", help= "temporary folder for files [./temp]")
    namespace = parser.parse_args()
    
    ### Input (read files)
    if namespace.fileType == "csv":
        (X_left_singular_vectors, Y_left_singular_vectors) = (getLeftSingularVectors(readCSVFile(namespace.X)), getLeftSingularVectors(readCSVFile(namespace.Y)))
    elif namespace.fileType == "hdf5":
        X = readHDF5File(namespace.X, "X")
        (X_left_singular_vectors, Y_left_singular_vectors) = (getLeftSingularVectors(X), getLeftSingularVectors(readHDF5File(namespace.Y, "Y")))
    else:
        sys.stderr.write("unrecognized file type. exiting\n")
        sys.exit(1)
        
    
    sys.stderr.write("done reading files\n")
    (sigBFC, sigBHC, sigBHYC) = runPermutaiton(X, X_left_singular_vectors, Y_left_singular_vectors, namespace.n1, namespace.n2, namespace.n3, namespace.numThreads, nPerm = namespace.numPermutations)
    
    # Only write the significant columns of X
    for (sigCutoffResults, name) in zip((sigBFC, sigBHC, sigBHYC), ["BFC", "BHC", "BHYC"]):
        writeSigXFeatures(sigCutoffResults, namespace.output, name)
    namespace.output.close()

if __name__ == '__main__':
    main()
    
