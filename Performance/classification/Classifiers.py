'''
Created on Jul 27, 2015

@author: ijoseph
'''
import sys
import sklearn

import numpy as np
#import rpy2.robjects.numpy2ri
#rpy2.robjects.numpy2ri.activate() 
# import rpy2.robjects as ro
# from rpy2.robjects.packages import importr

class BaseClassifier(sklearn.base.BaseEstimator):
    '''
    A general classifier; has fit() and score() methods 
    '''    
    def __init__(self, indices = None):
        self.indices = indices
    
    def fit(self, xTrain, yTrain):
        '''
        Fits the model by setting the parameters
        '''
        raise NotImplementedError()
    
    def score(self, xTest, yTest):
        raise NotImplementedError()
    
class FactorAssociationAnalysisApproximationClassifier(BaseClassifier):
    '''
    Classifies using lrCCA
    '''
    
    def __init__(self, n1): #
        self.n1 = n1
        self.n2 = 1
        self.n3 = 1
    
    def fit(self, xTrain, yTrain):
        '''
        Fits FAA-approximation by getting weights for canonical variates
        '''    
                   
        # Convert yTrain to float
        yTrain = yTrain.astype(np.float)
        (Xv, Yv, self.canonicalCorrelationCoefficients) = self._lrCCA(xTrain, yTrain, self.n1, self.n2, self.n3)
        
        # Get canonical variates
        self.xWeights = np.dot(xTrain.T, Xv) 
        self.yWeights = np.dot(yTrain.T, Yv)
        
        # Get the target Y for hypermutated and not hypermutated
        assert not min(yTrain) == max(yTrain), "Only one type of output given in training set"
        
        
        self.yHypermutated = max(yTrain)
        self.yNotHypermutated = min(yTrain)        
                
        self.yLatentHypermutated = np.dot(self.yWeights, self.yHypermutated) 
        self.yLatentNotHypermutated = np.dot(self.yWeights, self.yNotHypermutated)
        
        
             
    def score(self, xTest, yTest):
        '''
        Scores based on the weights learned before 
        '''
        yTest = yTest.astype(np.float)
        xLatent = np.dot(xTest, self.xWeights) # project down to 1-dimensional space        
        
        # score by looping through each test outcome and seeing whether it's closer to yLatentHypermutated or yLatentNotHypermutated, then compare with test
        
        resultList = []
        for (index, latentValue) in enumerate(xLatent):
            # Value is closer 
            if abs(latentValue -self.yLatentHypermutated) < abs(latentValue - self.yLatentNotHypermutated): # Predicted closer to hypermutated
                resultList += [1 if yTest[index] == self.yHypermutated else 0]
            else:# predicted closer to not hypermutated
                resultList += [1 if yTest[index] == self.yNotHypermutated else 0]
                
        return float(sum(resultList))/float(len(resultList))
    
    def _lrCCA(self, X, Y, n1, n2, n3 ):
        '''
        Performs low rank CCA with dimensions n1, n2, n3. 
        '''
        (Ux, _, _) =np.linalg.svd(X) 
        (Uy, _, _) =np.linalg.svd(Y) 
        
        if n1 == None:
            n1 = Ux.shape[1]
            sys.stderr.write("Setting PCA dimension of first data type to be number of predictors in data type: {0}".format(n1))
        if n2 == None:
            n2 = Uy.shape[1]
            sys.stderr.write("Setting PCA dimension of second data type to be number of predictors in data type: {0}".format(n2))
        if n3 == None:
            n3= min(n1,n2)
            sys.stderr.write("Setting CCA dimension to me min(n1,n2) = {0}".format(n3))
        
        
        UC, sC, VC = np.linalg.svd( Ux[:,0:n1].T.dot( Uy[:,0:n2] ), full_matrices=False ) #No nu,nv
        S1 = Ux[:,0:n1].dot( UC[:,0:n3] )
        S2 = Uy[:,0:n2].dot( VC.T[:,0:n3] ) #V contains RSVs as rows so need transpose
        return( S1, S2, sC[0:n3] )

# class iClusterClassifier(BaseClassifier):
#     '''
#     Use iCluster to classify by calling R package
#     '''
#     
#     def __init__(self, lambdaVector = None, indices =None):
#         '''
#         '''        
#         if lambdaVector is not None:
#             self.lambdaVector = lambdaVector
#         else:
#             self.lambdaVector = [0]*  2 if indices is None else [0]*(len(indices) +2) 
#         super(iClusterClassifier, self).__init__(indices = indices) # set indices for data division 
#     
#     def fit(self, xTrain, yTrain):
#         '''
#         Fits by getting and setting meanZ and weight matrices
#         '''
#         importr('iClusterPlus', lib_loc = "/Library/Frameworks/R.framework/Versions/3.1/Resources/library/") # Import iClusterPlus library
#         
#         yTrain = self._convertIndependentToNumeric(yTrain)                
#         (xTrainRDataFrames, yTrainRDataFrame) = self._convertDataToRDataFrames(xTrain, yTrain)
#         self._convertToRDataset(xTrainRDataFrames, yTrainRDataFrame)
#         betaList = self._runICluster()
#         
#         # Get the target Y for hypermutated and not hypermutated
#         assert not min(yTrain) == max(yTrain), "Only one type of output given in training set"
#         self.yHypermutated = max(yTrain)
#         self.yNotHypermutated = min(yTrain)
#         
#         #Get parameters I'll need: sample covariance, weight matrices
#         self.trainSampleCovarianceIndep = self._getSampleCovariance(xTrain)  #\Sigma_{independent}^{train}
#         
#         (self.weightMatrixIndep, self.weightMatrixDep) = self._getWeightMatrices(betaList)
# 
#     def score(self, xTest, yTest):
#         '''
#         Scores based on weights learned before
#         '''
#         resultList = []
#         for (i, xTestVal) in enumerate(xTest):
#             if self.classify(xTestVal) == [1 if yTest[i] == self.yHypermutated else 0]:
#                 resultList += [1]
#             else:
#                 resultList +=[0]
#                 
#         return sum(resultList)/ len(resultList)
# 
#     def classify(self, xTest):
#         '''
#         Gets classification for xTest based on learned parameters
#         '''
#         
#         # E [Z | xTest]
#         expZTest = np.dot(np.dot(self.weightMatrixIndep.T, np.linalg.inv(self.trainSampleCovarianceIndep)), xTest)
#         
#         # E [ Y | Z]
#         expYTest = np.dot(self.weightMatrixDep, expZTest)
#         
#         return [1 if abs( self.yHypermutated - expYTest ) < abs(self.yNotHypermutated - expYTest) else 0] 
# 
#     def _convertIndependentToNumeric(self, independentArray):
#         '''
#         Converts the training set from possible strings to numeric entries 
#         '''
#         
#         try:
#             return independentArray.astype(float)
#         except ValueError:
#             raise NotImplementedError("Doesn't yet work with non-numeric targets")
#             
#         
#     
#     def _convertDataToRDataFrames(self, xTrain, yTrain):
#         '''
#         Converts xTrain and yTrain into R dataframes
#         '''
#         # Load xTrain and yTrain into R instance
#         # convert to numpy arrays R dataframes
#         if (self.indices is not None) and (len (self.indices) > 0):
#             xTrainRDataFrames = []
#             # Split up matrix for use in iCluster
#             for (i, dataSplitColumn) in enumerate(self.indices):
#                 lastIndex = self.indices[i-1] if i -1 >=0 else 0 # index from previous split to current 
#                 xTrainRDataFrames +=[rpy2.robjects.numpy2ri.numpy2ri(xTrain[:,lastIndex:dataSplitColumn])] # Add up until the split                
#             # Add data type after last split
#             xTrainRDataFrames += [rpy2.robjects.numpy2ri.numpy2ri(xTrain[:, self.indices[-1]:xTrain.shape[1]])]
#         else:             
#             xTrainRDataFrames = [rpy2.robjects.numpy2ri.numpy2ri(xTrain)]                    
#         yTrainRDataFrame = rpy2.robjects.numpy2ri.numpy2ri(yTrain)
#         
#         return (xTrainRDataFrames, yTrainRDataFrame)
#     
#     def _convertToRDataset(self, xTrainRDataFrames, yTrainRDataFrame):
#         '''
#         Takes xTrain R data frames and yTrain R data frame and imports them into the R instance as 'datsets'
#         '''
#                 # Assign to R objects
#         ro.r('datasets <- list()')
#         
#         # xTrain
#         for (i, xTrainRDataFrame) in enumerate(xTrainRDataFrames):
#             # Assign to temporary r objcts
#             ro.r.assign('temp', xTrainRDataFrame)            
#             ro.r("datasets[[{0}]] <- temp".format(i+1))
#             
#         #yTrain        
#         ro.r.assign('temp', yTrainRDataFrame)
#         ro.r("datasets[[{0}]] <- temp".format(len(xTrainRDataFrames)+1))
#         
#         
#         
#         
#         
#         # run iCluster on the R instance: iCluster(datasets, k=4, lambda=c(0.2, 0.2), max.iter=50, epsilon=1e-3, scalar=FALSE)
#         
#         # extract meanZ and beta, for use in prediction and scoring methods
#     
#     def _runICluster(self):
#         '''
#         Actaully runs iCluster on the datasets, and gets the 'fit' objects
#         '''
#         ro.r("fit <- iCluster(datasets, k=2, lambda=c({0}), max.iter=50, epsilon=1e-3, scalar=FALSE)".format(",".join([str(val) for val in self.lambdaVector])))
#         
#                 
#         # Make list of W matrices (1 for each datatype)
#         betaList = []
#         for matrixIndex in range(len(self.indices)+2):
#             betaList += [np.asarray(ro.r("fit$beta[[{0}]]".format(matrixIndex +1)))]           
#         
#         
#         return betaList
#         
#     def _getSampleCovariance(self, xTrain):        
#         '''
#         Gets sample covariance based on the indices; just does X^T * X    
#         '''
#         return np.dot(xTrain.T,  xTrain)
# 
#     def _getWeightMatrices(self, betaList):
#         '''
#         Get the independent and dependent weight matrices by concatenating beta list   
#         '''        
#         return (reduce(lambda x,y: np.concatenate((x,y), axis=1), betaList[0:len(betaList)-1], betaList[-1]))      