# -*- coding: utf-8 -*-
import matplotlib
matplotlib.use('Agg')
import seaborn as sns
sns.set(font_scale=3)
import Models.LatentFactorModels
import Simulators.Simulator
import Performance.classification.CrossValidation
import numpy as np
import logging
import sklearn




class CVSimulationPerformance(object):
    """
    Runs a full simulation cycle at a range of e-values (:= w/Ψ)
    """

    def __init__(self, N=30, m=2, p_0=3, p_1=5, p_2=5, p_3=1, w_list=(2,2.5,3,20)):
        self.gen_model = None
        self.N=N
        (self.m, self.p_0, self.p_1, self.p_2, self.p_3) = (m, p_0, p_1, p_2, p_3)

        for w_mean in w_list:
            psi_mean = 2
            self.simulate_data(w_mean=w_mean, psi_mean=psi_mean)
            self.gather_data()


            self.logistic_regression(w_mean=w_mean, psi_mean=psi_mean)
            self.mfaa(w_mean=w_mean, psi_mean=psi_mean)



    def simulate_data(self, w_mean, psi_mean):
        logging.info("simulating...")
        self.gen_model = Models.LatentFactorModels.ThreeNodeGaussianModel(self.m, self.p_0, self.p_1, self.p_2, self.p_3)
        self.gen_model.initializeParameters(psiMean = psi_mean, wMean=w_mean, warm_start=False)
        simulator = Simulators.Simulator.Simulator(self.gen_model)

        np.random.seed(seed=150)
        self.gen_model = simulator.simulate(self.N)

    def gather_data(self):
        logging.info("gathering data...")
        self.dfc = Performance.classification.CrossValidation.DataFrameCollection(
            dependent_data=self.gen_model.getY(), independent_data=self.gen_model.getX())

    def logistic_regression(self, w_mean, psi_mean):
        self.cv_log_reg= Performance.classification.CrossValidation.CrossValidation(
            sklearn.linear_model.LogisticRegression()
            , self.dfc, 10)

        self.cv_log_reg.cross_validate()
        confusion_matrix_figure, roc_curve_figure = self.cv_log_reg.output_results(show_plot=False)
        confusion_matrix_figure.savefig("log_reg__confusion__w_{0}_psi_{1}.pdf".format(w_mean, psi_mean), bbox_inches='tight')
        roc_curve_figure.savefig("log_reg__roc__w_{0}_psi_{1}.pdf".format(w_mean, psi_mean), bbox_inches='tight')

    def mfaa(self, w_mean, psi_mean):
        self.cv_mfaa = Performance.classification.CrossValidation.CrossValidation(
            predictorClass=Models.LatentFactorModels.ThreeNodeGaussianModel(
                self.m, self.p_0, self.p_1, self.p_2, self.p_3),
            dataFrameCollection=self.dfc, k=10)

        self.cv_mfaa.cross_validate()
        confusion_matrix_figure, roc_curve_figure  = self.cv_mfaa.output_results(show_plot=False)

        confusion_matrix_figure.savefig("mfaa__confusion__w_{0}_psi_{1}.pdf".format(w_mean, psi_mean), bbox_inches='tight')
        roc_curve_figure.savefig("mfaa__roc__w_{0}_psi_{1}.pdf".format(w_mean, psi_mean), bbox_inches='tight')


if __name__ == '__main__':
    CVSimulationPerformance(N=1000, p_1=25000, p_2=25000)