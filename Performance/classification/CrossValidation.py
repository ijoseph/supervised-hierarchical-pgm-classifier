# -*- coding: utf-8 -*-
"""
Created on Jul 22, 2015

@author: ijoseph
"""
import argparse, sys
import os

import pandas as pd
import sklearn.cross_validation
import sklearn.model_selection
import sklearn.linear_model
import sklearn.metrics
import scipy
import itertools
import scipy.stats

import matplotlib.pyplot as plt
import numpy as np
import pandas.io as pio
import matplotlib.patheffects as path_effects
import logging
logging.basicConfig(stream=sys.stderr, level=50)
import seaborn as sns
import Models.ModelComparer
import multiprocessing

class CrossValidation(object):

    """
    Runs, given a class for prediction, cross validation to estimate expected prediction error with that prediction class for the input data
    """

    def __init__(self, predictorClass, dataFrameCollection, k, auto_tune=False):
        """
        Sets the prediction class and dependent variables; sets indices of classifier
        """
        self.predictorClass = predictorClass        
        self.k = k
        self.independentVariables = dataFrameCollection.independentDF
        self.dependentVariable = dataFrameCollection.dependentDF.iloc[:,0] # get first column only

        self.cv_results = CrossValidationResult(dependent_variable = self.dependentVariable,
                                                model=self.predictorClass)

        self.full_class_proportions = self.dependentVariable.sum()/float(self.dependentVariable.shape[0])

        if auto_tune:
            grid_search_obj_fit = self.grid_search()
            self.predictorClass = grid_search_obj_fit.best_estimator_

    def cross_validate(self, binarize_output=True, stratify=True):
        """
        Runs the cross-validation, given the model.         
        """

        if stratify:
            k_fold_iterator =  sklearn.model_selection.StratifiedKFold(n_splits=self.k, shuffle=True) # use stratified k-fold
        else:
            k_fold_iterator = sklearn.model_selection.KFold(n_splits=self.k,shuffle=True)

        # n_cpu = int(math.floor(multiprocessing.cpu_count()/float(2))) - 1
        # joblib.Parallel(n_jobs=n_cpu, backend="threading", verbose=5)\
        #     (joblib.delayed(self.run_round)(binarize_output, i, train, validate) for (i, train, validate) in
        #      enumerate(k_fold_iterator))



        for (i, (train, validate)) in enumerate(k_fold_iterator.split(X=self.independentVariables, y=self.dependentVariable)):
            self.run_round(binarize_output, i, train, validate)

    def grid_search(self, tuning_grid=({'penalty': ['l1', 'l2'], 'C':[1e-10, 1e-5, 1e-2, 1, 1e2, 1e5, 1e10]})):
        """ Runs a grid search for the best parameters"""

        k_fold_iterator = sklearn.model_selection.StratifiedKFold(n_splits=self.k,
                                                                  shuffle=True)  # use stratified k-fold

        grid_search_obj = sklearn.model_selection.GridSearchCV(estimator=self.predictorClass.__class__(),
                                                    param_grid=tuning_grid,
                                                    n_jobs=int(round(multiprocessing.cpu_count()/2.0+1)),
                                                    cv=k_fold_iterator,
                                                               scoring='roc_auc')


        grid_search_obj_fit = grid_search_obj.fit(X=self.independentVariables,y=self.dependentVariable)

        self.plot_tuning_results(grid_search_obj_fit, tuning_grid)

        return grid_search_obj_fit

    def plot_tuning_results(self, grid_search_obj_fit, tuning_grid):
        plt.figure()
        ax = sns.regplot(x=np.log10(np.array(tuning_grid['C'])),
                         y=grid_search_obj_fit.cv_results_['mean_test_score'][0::2])
        ax.set_title("{0}: log(C) versus AUC".format(tuning_grid['penalty'][0]))
        plt.figure()
        ax = sns.regplot(x=np.log10(np.array(tuning_grid['C'])),
                         y=grid_search_obj_fit.cv_results_['mean_test_score'][1::2])
        ax.set_title("{0}: log(C) versus AUC".format(tuning_grid['penalty'][1]))

    def run_round(self, binarize_output, i, train, validate):
        logging.warning("-" * 30 + "CV fold {0} of {1}".format(i + 1, self.k) + "-" * 30)
        # Fit the model on the training data
        fitted_predictor = self.predictorClass.fit(self.independentVariables.iloc[train],
                                                   self.dependentVariable.iloc[train])
        # what were the validate sample predictions?
        try:
            predictedThisFold = fitted_predictor.predict(self.independentVariables.iloc[validate],
                                                         binarize=binarize_output)
        except TypeError:
            predictedThisFold = fitted_predictor.predict(self.independentVariables.iloc[validate])
        proba = fitted_predictor.decision_function(
            self.independentVariables.iloc[validate])  # decision function on validate set
        self.assess_fold(fitted_predictor, predictedThisFold, proba, validate, train, i, binarize_output, self)
        FP, TN, accuracy_score, precision, recall = self.compile_metrics(predictedThisFold, validate)
        self.cv_results.update_with_fold_results(accuracy_score=accuracy_score,
                                                 precision=precision,
                                                 recall=recall,
                                                 predicted=predictedThisFold.tolist(),
                                                 proba=proba.tolist(),
                                                 specificity=[TN / float(FP + TN)])

    def assess_fold(self, fittedPredictor, predictedThisFold, proba, validate, train, i, binarize_output,
                    cross_validation_object):

        # Σ_z|x
        self.assess_fit(fittedPredictor, i, cross_validation_object)
        # Look at Validate
        self.assess_validate_prediction(cross_validation_object, predictedThisFold, proba, validate, i, fittedPredictor)## Look at Train
        self.assess_train_prediction(binarize_output, cross_validation_object, fittedPredictor, i, train)

    def assess_fit(self, fittedPredictor, i, cross_validation_object):
        """
        Assess fit by looking at properties of parameters fit this fold.
         Used for debugging purposes.
        :param fittedPredictor:
        :return:
        """
        try:
            logging.debug("det(hiddenHiddenJointPrec_Lambda_zz)= \n{0}".format(
                np.linalg.det(fittedPredictor.hiddenHiddenJointPrec_Lambda_zz)))

            if logging.getLogger().getEffectiveLevel() == 10:
                Models.ModelComparer.ModelComparer.\
                    plot_model_parameters(fittedPredictor,
                                          title = "Fold {0} of {1}".format(i + 1, cross_validation_object.k))
        except AttributeError:
            pass

    def assess_validate_prediction(self, cross_validation_object, predictedThisFold, proba, validate, i, fittedPredictor):
        logging.debug("Predicted this fold:\n{0}".format(predictedThisFold))
        logging.debug("Actual this fold:\n{0}".format(self.dependentVariable.iloc[validate]))
        try:
            if (self.dependentVariable.iloc[validate].tolist() != predictedThisFold.tolist()):
                logging.info("Fold {0}: difference between predicted and acutal:"
                             "\n{1}\nvs\n{2}".format(i +1, predictedThisFold,
                                                     self.dependentVariable.iloc[validate]))
        except:
            pass
        logging.debug("Proba this fold: {0}".format(proba))
        # Plots if DEBUG or INFO
        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            plt.figure()
            ax = sns.regplot(x=cross_validation_object.dependentVariable.iloc[validate], y=predictedThisFold,
                             label="Validate")

        # Plot Betas
        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            try:
                plt.figure()
                bp = sns.barplot(x=self.independentVariables.columns, y=fittedPredictor.coef_.T.reshape(fittedPredictor.coef_.shape[1], ))
                bp.set_title("Betas CV round {0}".format(i+1))
                for p in bp.patches:
                    b = p.get_bbox()
                    bp.annotate("{:+.2f}".format(b.y1 + b.y0), ((b.x0 + b.x1) / 2 - 0.03, b.y1 + 0.02))
            except AttributeError:
                pass

        # Output gamma
        this_validate_frac = self.dependentVariable.iloc[validate].sum() / float(self.dependentVariable.iloc[validate].shape[0])
        gamma = this_validate_frac / self.full_class_proportions
        logging.info("Gamma this fold: {0:.3f}".format(gamma))

    def assess_train_prediction(self, binarize_output, cross_validation_object, fittedPredictor, i, train):
        try:
            train_prediction = fittedPredictor.predict(cross_validation_object.independentVariables.iloc[train],
                                                       binarize=binarize_output)
        except TypeError:
            train_prediction = fittedPredictor.predict(cross_validation_object.independentVariables.iloc[train])
        if logging.getLogger().getEffectiveLevel() <= logging.INFO:
            plt.figure()
            ax = sns.regplot(x=cross_validation_object.dependentVariable.iloc[train],
                             y=train_prediction,
                             label="Train")

            CrossValidation.add_identity(ax)

            ax.set(xlabel="$y$", ylabel="$\hat{y}$")

            ax.set_title("Fold {0} of {1}".format(i + 1, cross_validation_object.k))
            ax.legend()

    @staticmethod
    def add_identity(axes, *line_args, **line_kwargs):
        identity, = axes.plot([], [], 'k--', linewidth=0.5, *line_args, **line_kwargs)

        def callback(axes):
            low_x, high_x = axes.get_xlim()
            low_y, high_y = axes.get_ylim()
            low = max(low_x, low_y)
            high = min(high_x, high_y)
            identity.set_data([low, high], [low, high])

        callback(axes)
        axes.callbacks.connect('xlim_changed', callback)
        axes.callbacks.connect('ylim_changed', callback)
        return axes

    @staticmethod
    def auc_and_n_1_and_n_2_to_p(AUC, n_1, n_2):
        U = AUC * n_1 * n_2
        print ("U = {0:.1f}".format(U))
        z_u = (np.abs(U - (n_1 * n_2) / 2.0)) / np.sqrt((n_1 * n_2 * (n_1 + n_2 + 1)) / (12))
        p_val = scipy.stats.norm.sf(abs(z_u))
        return p_val

    def compile_metrics(self, predictedThisFold, validate):
        try:
            confus = sklearn.metrics.confusion_matrix(self.dependentVariable.iloc[validate], predictedThisFold)
        except ValueError:
            confus = np.array([[0, 0], [0, 0]])
        logging.debug("Confus: {0}".format(confus))
        if confus.size == 4:
            TN, FP = confus[0, 0], confus[0, 1]
        else:
            TN, FP = 0, 1

        try:
            accuracy_score = [sklearn.metrics.accuracy_score(self.dependentVariable.iloc[
                                                                 validate], predictedThisFold)]
            precision = [sklearn.metrics.precision_score(self.dependentVariable.iloc[
                                                             validate], predictedThisFold)]
            recall = [sklearn.metrics.recall_score(self.dependentVariable.iloc[
                                                       validate], predictedThisFold)]
        except ValueError:
            accuracy_score = [np.nan]
            precision = [np.nan]
            recall = [np.nan]

        return FP, TN, accuracy_score, precision, recall

    def output_results(self, show_plot=True):
        return self.cv_results.display(show_plot=show_plot)

class CrossValidationNonDeprecated(CrossValidation):
    def cross_validate(self, binarize_output=True):
        # cv = sklearn.model_selection.StratifiedKFold(n_splits=self.k)
        cv = sklearn.model_selection.StratifiedShuffleSplit(n_splits=self.k)


        colors = itertools.cycle(['cyan', 'indigo', 'seagreen', 'yellow', 'blue', 'darkorange'])
        lw = 2

        mean_tpr = 0.0
        mean_fpr = np.linspace(0, 1, 100)
        i = 0
        for (train, test), color in zip(cv.split(self.independentVariables, self.dependentVariable), colors):
            probas_ = self.predictorClass.fit(self.independentVariables.iloc[train],
                                              self.dependentVariable.iloc[train]).\
                predict_proba(self.independentVariables.iloc[test])

            fpr, tpr, thresholds = sklearn.metrics.roc_curve(self.dependentVariable.iloc[test], probas_[:, 1])
            mean_tpr += scipy.interp(mean_fpr, fpr, tpr)

            mean_tpr[0] = 0.0
            roc_auc = sklearn.metrics.auc(fpr, tpr)

            plt.plot(fpr, tpr, lw=lw, color=color,
                     label='ROC fold %d (area = %0.2f)' % (i, roc_auc))

            i+=1
        plt.plot([0, 1], [0, 1], linestyle='--', lw=lw, color='k',
                 label='Luck')

        mean_tpr /= cv.get_n_splits(self.independentVariables, self.dependentVariable)
        mean_tpr[-1] = 1.0
        mean_auc = sklearn.metrics.auc(mean_fpr, mean_tpr)
        plt.plot(mean_fpr, mean_tpr, color='g', linestyle='--',
                 label='Mean ROC (area = %0.2f)' % mean_auc, lw=lw)

        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic retry')
        plt.legend(loc="lower right")

    def output_results(self, show_plot=True):
        plt.show()

class DataFrameCollection:
    """
    Takes in independent and dependent data and returns lists of numpy.arrays of both 
    """
    
    def __init__(self, independent_data = None, dependent_data = None, binarize_dependent = True,
                 boundary_indices = None):

        self.indIndices = None
        self.independentDF = None
        
        if (independent_data is not None) and (dependent_data is not None):

            self.dependentDF = self.parseDependent(dependent_data=dependent_data, binarize=binarize_dependent)
            (self.independentDF, self.indIndices) = self.parseIndependent(independent_data)
        
            self.n = self.independentDF.shape[0]

        # set boundary indices
        if boundary_indices:
            self.indIndices = boundary_indices


    @classmethod
    def from_directory(cls, directory):
        """
        Initialize from directory whether feather data saved
        """
        import feather

        ind_ind_df_file_name, dep_df_file_name, ind_df_file_name = None, None, None

        for file_name in os.listdir(directory):
            if file_name.endswith("independentDF.feather"):
                ind_df_file_name = file_name

            elif file_name.endswith("dependentDF.feather"):
                dep_df_file_name = file_name
            elif file_name.endswith("Indices.feather"):
                ind_ind_df_file_name = file_name


        if (dep_df_file_name is None) or (ind_df_file_name is None):
            raise IOError("Can't find files in this dir")

        if ind_ind_df_file_name is not None:
            boundary_indices = list(feather.read_dataframe(os.path.join(directory, ind_ind_df_file_name)).iloc[:, 0])
        else:
            boundary_indices = None



        return cls(independent_data=feather.read_dataframe(os.path.join(directory, ind_df_file_name)),
                   dependent_data=feather.read_dataframe(os.path.join(directory,dep_df_file_name)),
                   boundary_indices=boundary_indices)
        
    
    def parseIndependent(self, independent_data_list):
        """
        Parse the independent data
        """
        
        if not isinstance(independent_data_list, list):
            independent_data_list = [independent_data_list]

        outputList = []
        for input_data in independent_data_list:

            df = self.parse_input_data(input_data)

            outputList += [df]
            
        return (pd.concat(outputList, axis=1), [piece.shape[1] for piece in outputList[0:len(outputList)-1]])

    def parse_input_data(self, input_data):
        """
        Helper method to parse input data into a pd DataFrame,
        using the string-typed column as the index, if present.
        :param input_data:
        :return:
        """
        if isinstance(input_data, str) or isinstance(input_data, file):
            logging.info("Reading independent file:" + str(input_data) + "\n")
            df = pio.parsers.read_csv(input_data, sep=",", na_values=["NA", "NaN", " "])

            try:
                df = df.set_index(df.select_dtypes(include=['object']).columns.values.tolist()[0])
            except IndexError:  # No string-type columns.
                pass

        elif isinstance(input_data, pd.DataFrame):
            df = input_data

        else:
            raise NotImplementedError("Input data of type {0}".format(type(input_data)))

        # Set the string-type column as the index

        return df

    def parseDependent(self, dependent_data, binarize):

        dependent_df = self.parse_input_data(dependent_data)

        if binarize:
            self.binarize(dependent_df)

        return dependent_df


    def binarize(self, dependent_df):
        # If columns are not boolean, convert them to be as such
        non_bool_cols = [col for col in dependent_df if not dependent_df[[col]].dropna().isin([0, 1]).all().values]

        for col in non_bool_cols:
            logging.warn("Found non-boolean columns in dependent data; converting based on threshold of mean\n")
            self.binarize_column(dependent_df, col)
            # logging.info("{0}".format(dependent_df))

    def binarize_column(self, data_frame, column_name):
        """
        Updates DataSeries by converting to boolean values
        :param data_frame_series:
        :return:
        """
        data_frame[column_name] = data_frame[column_name] > data_frame[column_name].mean()

    def num_missing_values(self):
        """

        :return:
        """
        return self.independentDF.isnull().values.sum() +  self.dependentDF.isnull().values.sum()

    def num_columns_of_low_variance(self, thresh=0):
        # Should be no zero-variance columns
        return sum(self.independentDF.var() < thresh) +  sum(self.dependentDF.var() < thresh)


    def dropRows(self, rowsToDrop):
        """
        Drops rows, then returns them as a new DataFrameCollection object
        """
        
        # Construct new
        new = DataFrameCollection()        
        new.independentDF = self.independentDF.iloc[rowsToDrop]
        new.dependentDF = self.dependentDF.iloc[rowsToDrop]
        new.indIndices = self.indIndices
        new.n= new.independentDF.shape[0]
        
        # Drop rows
        
        self.independentDF.drop(self.independentDF.index[rowsToDrop], inplace=True)
        self.dependentDF.drop(self.dependentDF.index[rowsToDrop], inplace=True)
        self.n=self.independentDF.shape[0]
        
        return new


    def down_sample(self, num_kept_rows_and_cols, random_state=42):
        """ Down-sample independent and dependent data by keeping only the specified number of rows and columns"""



        new_ind = self.independentDF.sample(num_kept_rows_and_cols, random_state=random_state)  # keep rows
        new_ind = new_ind.sample(num_kept_rows_and_cols, axis=1, random_state=random_state) # keep columns
        new_dep = self.dependentDF.loc[new_ind.index] # keep these rows from dependent as well

        new_dfc = DataFrameCollection(independent_data=new_ind,
                                      dependent_data=new_dep, boundary_indices=self.indIndices)

        return new_dfc

    def save_data(self, directory):
        """

        :return:
        """
        import feather

        if not os.path.exists(directory):
            os.makedirs(directory)


        prefix = "n_{0}_p_{1}".format(self.independentDF.shape[0], self.independentDF.shape[1])

        feather.write_dataframe(self.independentDF.copy(),
                                os.path.join(directory, prefix + "independentDF.feather"))

        feather.write_dataframe(self.dependentDF.copy(),
                                os.path.join(directory,  prefix + "dependentDF.feather"))

        feather.write_dataframe(pd.DataFrame([self.indIndices]).copy(),
                                os.path.join(directory,  prefix + "indIndices.feather"))

        return True

    def head(self, n=10):
        return DataFrameCollection(independent_data=self.independentDF.head(n),
                                   dependent_data=self.dependentDF.head(n), boundary_indices=self.indIndices)



    def __str__(self):
        return  "Independent Data: \n {0} \n IndIndices {1}\n\n Dependent Data:\n{2}"\
            .format(self.independentDF, self.indIndices, self.dependentDF)

class CrossValidationResult(object):
    def __init__(self, dependent_variable, model):
        self.accuracy_score_list= []
        self.precision_list = []
        self.recall_list = []
        self.predicted_list = []
        self.proba_list = []
        self.specificity_list = []
        self.dependent_variable = dependent_variable
        self.model = model

    def update_with_fold_results(self, accuracy_score = [], precision = [], recall = [], predicted = [], proba=[],
                                 specificity = []):
        self.accuracy_score_list += accuracy_score
        self.precision_list += precision
        self.recall_list += recall
        self.predicted_list += predicted
        self.proba_list += proba
        self.specificity_list += specificity


    def display(self, show_plot=True):
        """
        Formats and writes an output table with scores
        """
        confusion_matrix_figure, roc_curve_figure = None, None
        self.finalize_cv()

        for (score_list, name) in zip(
                (self.proba_list, self.accuracy_score_list, self.recall_list, self.specificity_list, self.precision_list),
                ("proba", "accuracy", "recall", "specificity", "precision")):
            self.display_mean_and_se(score_list, name)

        # plot confusion matrix

        confusion_matrix_figure = self.plot_confusion_matrix_and_format(confusion_matrix_figure, show_plot)
        # plot ROC curve
        roc_curve_figure = self.plot_roc_curve_and_format(roc_curve_figure, show_plot)

        score_space_figure = self.score_space_plot(show_plot)

        return confusion_matrix_figure, roc_curve_figure, score_space_figure

    def score_space_plot(self, show_plot=False):
        sns.plt.figure()
        indexed_score_space = pd.DataFrame({"score": self.proba_list}).set_index(
            self.dependent_variable.index).join(self.dependent_variable).rename(columns={0:"HM"})
        ax = sns.boxplot(data=indexed_score_space, x="HM", y="score")
        sns.swarmplot(data=indexed_score_space, x="HM", y="score", color="black")

        ax.set_ylabel("prediction score")
        sns.plt.setp(ax.artists, alpha=.1)
        for patch in ax.artists:
            r, g, b, a = patch.get_facecolor()
            patch.set_facecolor((r, g, b, .3))
        figure = sns.plt.gcf()
        if show_plot:
            sns.plt.show()
        return figure

    def display_mean_and_se(self, lst, list_name):
        print ("mean {0} score {1:.2f}, standard error {2:.2f}".format(
            list_name, np.mean(lst), np.std(lst)))
        logging.info("All {0} scores: {1}".format(list_name, lst))

    def plot_confusion_matrix_and_format(self, confusion_matrix_figure, show_plot):
        try:
            confusion_matrix_figure = self._plot_confusion_matrix(
                self.cm, title="Confusion Matrix ({0})".format(self.model), show_title=show_plot)

        except TypeError:
            pass

        return confusion_matrix_figure

    def plot_roc_curve_and_format(self, roc_curve_figure, show_plot):
        try:
            roc_curve_figure = self._plot_roc_curve(ttl="ROC ({0})".format(self.model), show_title=show_plot)
        except AttributeError:
            pass
        except TypeError:
            pass
        if show_plot:
            plt.show()
        return roc_curve_figure

    def finalize_cv(self):
        if len(self.proba_list):
            try:
                self.rocCurve = sklearn.metrics.roc_curve(self.dependent_variable, self.proba_list)
            except ValueError:
                self.rocCurve = None
        try:
            self.cm = sklearn.metrics.confusion_matrix(self.dependent_variable, self.predicted_list)
        except ValueError:
            self.cm = None
        self.meanScore = np.mean(self.accuracy_score_list)
        self.stdScore = np.std(self.accuracy_score_list)

    def _plot_confusion_matrix(self, cm, numTargets=2, title='Confusion matrix', show_title=True):
        fig = plt.figure()
        plt.imshow(cm, interpolation='nearest')
        # CrossValidationResult.make_matrix_heatmap(data=cm)
        if show_title:
            plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(numTargets)
        plt.xticks(tick_marks, ["false", "true"])
        plt.yticks(tick_marks, ["false", "true"])
        plt.ylabel('Actual label')
        plt.xlabel('Predicted label')

        if cm.shape[0] * cm.shape[1] <= 25:   # add text if 25 cells or smaller
            for y in range(cm.shape[0]):
                for x in range(cm.shape[1]):
                    # print ("(x = {0}, y={1}, data={2}".format(x,y, data))
                    txt = plt.text(x , y , '%.1f' % cm[y, x],
                             horizontalalignment='center',
                             verticalalignment='center')
                    txt.set_path_effects([path_effects.Stroke(linewidth=3, foreground='white'),
                                           path_effects.Normal()])


        return fig


    def get_auc(self):
        try:
            (fpr, tpr, _) = self.rocCurve
        except AttributeError:
            self.finalize_cv()
            (fpr, tpr, _) = self.rocCurve

        rocAuc = sklearn.metrics.auc(fpr, tpr)
        return rocAuc

    def show_score_space_plot(self):
        """
        Show plot of samples in score space, colored by outcome
        :return:
        """





    def _plot_roc_curve(self, ttl = "ROC", show_title=True):
        fig = plt.figure()
        (fpr, tpr, _) = self.rocCurve
        rocAuc = sklearn.metrics.auc(fpr, tpr)
        plt.plot(fpr, tpr, label="ROC curve (AUC = %.3f)" % rocAuc)
        plt.plot([0,1],[0,1], 'k--')
        plt.xlim([0.0,1.05])
        plt.ylim([0.0,1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        if show_title:
            plt.title(ttl)
        plt.legend(loc="lower right")
        return fig
        # plt.savefig(plotDir + os.sep+ 'roc-curve.pdf')






def main():    
    parser = argparse.ArgumentParser(description = "Runs, given a class for prediction, "
                                                   "cross validation to estimate expected prediction error "
                                                   "with that prediction class for the input data")
    parser.add_argument("--independentData", required = True, type=argparse.FileType('r'), nargs='+',
                        help="one or more input matrices from which to predict. .csv file.")
    parser.add_argument("--dependentData", required=True, type=file,
                        help="file where each line specifies the class of the independent data (same order). .csv file")
    parser.add_argument("-k", type=int, help="k for k-fold CV", default=10)
    parser.add_argument('-o', '--output', type=argparse.FileType('w'),
                        default = sys.stdout, help= "File to which to output output" )
    parser.add_argument(
        '-v', '--verbose',
        help="Be verbose",
        action="store_const", dest="loglevel", const=logging.INFO)


    
    namespace = parser.parse_args()
    logging.basicConfig(level=namespace.loglevel)
    
    # Parse independent    
    dfc = DataFrameCollection(namespace.independentData, namespace.dependentData) # parse input files and store as pd DataFrames
    cv = CrossValidation(sklearn.linear_model.LogisticRegression(), dfc , namespace.k) # logistic regression
#    cv_logistic_regression = CrossValidation(sklearn.discriminant_analysis.LinearDiscriminantAnalysis(), fp , namespace.k) # linear discriminant analysis (LDA)
#     cv_logistic_regression = CrossValidation(FactorAssociationAnalysisApproximationClassifier(n1=20), fp , namespace.k) # factor association analsyis approx (should be similar to LDA)
#     cv_logistic_regression = CrossValidation(iClusterClassifier(indices=fp.indIndices), fp , namespace.k) # iCluster-based calssification (not implememted)
    cv.cross_validate()
    cv.output_results()
    
        
if __name__ == '__main__':
    main()