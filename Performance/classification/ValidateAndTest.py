'''
Created on Feb 5, 2016

@author: ijoseph

Includes validation/ test runs for a variety of models
'''

import os

from ggplot import *
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn import metrics
from CrossValidation import *


class ValidateAndTest:
    
    def __init__(self, fp, cVals, k, output):
        self.validateMatrixData = fp
        self.cVals = cVals
        self.k = k
        self.output = output
        
        self.cVals.sort()
        
        
    def validateModel(self, model):
        '''
        Does CV for logistic regression to select C (L1 penaltiy)
        '''
        meanScores = []
        stdScores = []
        for c in self.cVals: # different penalties
            model.set_params(C=c)
            modelTuneParameterSet = model
            
            cv = CrossValidation(modelTuneParameterSet, self.validateMatrixData , self.k)
            cv.cross_validate()
            meanScores += [cv.meanScore]
            stdScores += [cv.stdScore]
        self.meanOfCVScores = meanScores
        self.stdOfCVScores= stdScores
        
    
    
    def splitMatrixData(self, testPercent):
        '''
        Splits CrossValidation.dataFrameCollection class into testPercent% and (100 - testPercent)%
        CrossValidation.dataFrameCollection pieces randomly
        '''    
        np.random.seed(seed=42)
        
        rowsToDrop = np.random.choice(self.validateMatrixData.n-1,
                                      size=int(np.floor(.01 * testPercent * self.validateMatrixData.n )), replace=False)
        
        self.testData  = self.validateMatrixData.dropRows(rowsToDrop)        
        
    
    
    def plotValidationResults(self, cVals, plotDir):
        '''
        Plots results for different cValues
        '''
        meanSDCVDF = pandas.DataFrame(data={"meanScores": self.meanOfCVScores, "stdScores": self.stdOfCVScores, "cVals":cVals})    
            
        plt = self.makePlotWithErrorBars(meanSDCVDF, "cVals", "meanScores", "stdScores")
        plt += geom_line() + xlab("L1 penalty") + ylab("Mean cross validation-estimated prediction accuracy")
        # manually add error bars
        ggsave(plot=plt, filename=plotDir + os.sep + "plt.pdf")
        plt
        
        
    def makePlotWithErrorBars(self, dataFrame, xValName, yValName, errorName):
        '''
        Add error bars to a plot object based based on yvalue, error bar name; uses the data.frame 
        '''
        
        dataFrame['lower'] = dataFrame[yValName] - dataFrame[errorName]
        dataFrame['upper'] = dataFrame[yValName] + dataFrame[errorName]
        plt = ggplot(aes(x=xValName, y=yValName), data = dataFrame)    
        plt += geom_point()
        plt += geom_point(aes(y="lower"), shape =1, size=100, colour="#CC0000")
        plt += geom_point(aes(y="upper"), shape =1, size=100, colour="#CC0000")
        plt+= scale_x_log10()
        return plt
    
    def chooseBestTuningParam(self):
        '''
        Looks at the range of c values; chooses the best based on that is 1 sd-from-max rule
        '''
        
        for (i, score) in enumerate( self.meanOfCVScores):
            if score + self.stdOfCVScores[i] >= max(self.meanOfCVScores):
                self.output.write("Best tuning parameter value (based on 1 sd rule): {0:.2f}".format(self.cVals[i]))
                return self.cVals[i]

    def testAccuracy(self, cVal, model, plotDir =""):
        '''
        Tests accuracy of model trained on the training set with a given cVal at predicting for test set
        '''
        model.set_params(C=cVal, random_state = 42)
        modelBestTuned= model
        fittedModel = modelBestTuned.fit(self.validateMatrixData.independentDF, self.validateMatrixData.dependentDF) # fit on all validation data
        overallScore = fittedModel.score(self.testData.independentDF, self.testData.dependentDF) # score on all test data
        probas = list(fittedModel.decision_function(self.testData.independentDF))
        rocCurve = metrics.roc_curve(self.testData.dependentDF, probas, pos_label='1') 
        CrossValidation._plot_roc_curve(rocCurve, plotDir = plotDir)
        self.output.write("Overall score: {0:.2f}".format(overallScore))
        return overallScore
        
       
        
        
def main():
    
    '''
    Class to take a direcotry as an input argument
    '''
    class readable_dir(argparse.Action):
        '''
        Class to take a direcotry as an input argument
        '''
        def __call__(self,parser, namespace, values, option_string=None):
            prospective_dir=values
            if not os.path.isdir(prospective_dir):
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
            if os.access(prospective_dir, os.R_OK):
                setattr(namespace,self.dest,prospective_dir)
            else:
                raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))
    
    parser = argparse.ArgumentParser(description = "Runs, given a class for prediction, cross validation to estimate expected prediction error with that prediction class for the input data") 
    parser.add_argument("--independentData", required = True, type=argparse.FileType('r'), nargs='+', help="one or more input matrices from which to predict [True]") 
    parser.add_argument("--dependentData", required=True, type=file, help="file where each line specifies the class of the independent data (same order) [True]")
    parser.add_argument('--testPercent', default=25, help="How much of data to withhold for testing. [25]")
    parser.add_argument('--cVals', nargs="*", type=float, default=[10**-2, 10**-1, 10**0, 10**1, 10**2, 10**3] + [(10**2) * i for i in range(1,10)], help="How much of data to withhold for testing. [25]")
    parser.add_argument("-k", type=int, help="k for k-fold CV. [10]", default=10)
    parser.add_argument('-o', '--output', type=argparse.FileType('w'), default = sys.stdout, help= "File to which to output output" )
    parser.add_argument('-p', '--plotDir', action=readable_dir, default="")
    
    namespace = parser.parse_args()
    fp = DataFrameCollection(namespace.independentData, namespace.dependentData) # read in file    
    vat = ValidateAndTest(fp, namespace.cVals, namespace.k, namespace.output)    
    vat.splitMatrixData(namespace.testPercent) # create validate/ test split
    
#     # L1-penalized Logistic Regression model
    model = LogisticRegression()
    model.set_params(penalty="l1")
    #
#     model= SVC()    
    
    
    # Get the mean scores across all folds for different C values
    vat.validateModel(model)
    vat.plotValidationResults(namespace.cVals, namespace.plotDir)
    
    bestC = vat.chooseBestTuningParam()
#     vat.testAccuracy(bestC, model, namespace.plotDir)
    vat.testAccuracy(10**1, model, namespace.plotDir)
    

if __name__ == '__main__':
    main()