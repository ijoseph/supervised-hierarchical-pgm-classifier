'''
Created on Jan 15, 2016

@author: ijoseph
'''

import functools
import logging
import math
import multiprocessing
import operator
import sys
import copy

import numpy as np
import joblib


class Simulator(object):
    '''
    Works with a hierarchical Model with specified parameters to simulate data from the model
    '''

    def __init__(self, model, output =sys.stdout):
        '''
        Sets seed and parameters; runs simulation
        '''
        self.model = model
        

    def simulate(self, N=50, parallel = True):
        '''
        Traverse tree and generate N samples
        '''
        try:
            self.model.initialized
        except AttributeError:
            raise ValueError("Model parameters not initialized before simulating")

        self.model.clear_data()  # clear data before simulating new data

        # Simulate each data point separately
        n_cpu = int(math.floor(multiprocessing.cpu_count()/float(2))) - 1
        logging.info("Simulating {0} points with {1} processors".format(N, n_cpu))
        # for each data point, simulate.
        def chunkify(lst, n):
            return [lst[i::n] for i in range(n)]

        ns = map(len, chunkify([0] * int(N), n_cpu)) # list of sizes per CPU
        sizes, counts = np.unique(ns, return_counts=True)

        try:
            logging.info("{0} cpus do {1}, {2} cpus do {3}. Prepping models..."
                     .format(counts[0], sizes[0], counts[1], sizes[1]))
        except IndexError:
            logging.info("{0} cpus do {1}. Prepping models..."
                     .format(counts[0], sizes[0]))
        model_list_pre_simulate = [copy.deepcopy(self.model) for _ in ns]

        logging.info("Done prepping models. Mapping...")
        if parallel:
            model_list = joblib.Parallel(n_jobs=n_cpu, backend="threading", verbose=5)\
                (joblib.delayed(bfs_simulate)(mod, n) for (mod, n) in zip (model_list_pre_simulate, ns))
        else:
            model_list = [bfs_simulate(mod, n) for (mod,n) in zip(model_list_pre_simulate, ns)]

        updated_model = functools.reduce(operator.add, model_list)

        return updated_model


def bfs_simulate(model, N):

    np.random.seed() # make a new seed.
    for _ in range(N):
        queue = [model.dataTree]
        while queue:
            vertex = queue.pop(0)
            if vertex is None:
                continue
                # Generate data for this node
            vertex.paramData.simulate(vertex.parent)
            queue.extend([vertex.left, vertex.right])  # add left node, right node
    return model

                
                
    
        
        
        
        
    
        
        